﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace SGIDemo
{
    public class DiagramEntity : SGI.SGIEntity
    {
        private Image _diagramImage;
        private Rectangle _selectArea;

        public Image DiagramImage
        {
            get { return _diagramImage; }
            set { _diagramImage = value; }
        }

        public DiagramEntity()
            : base()
        {
            this.Border = false;
            this.BorderWidth = 1;
            this.DrawBackground = false;
            this.OwnerDrawText = false;
            this.ToggleBackcolor = true;
            this.SelectedBackColor = Color.Khaki;
            this.HoverBackColor = Color.LightSteelBlue;
        }

        protected override Rectangle GetSelectionArea()
        {
            _selectArea.Offset(1, 1);
            _selectArea.Width -= 3;
            _selectArea.Height -= 3;
            return _selectArea;
        }

        protected override void Draw(System.Drawing.Graphics graphics)
        {
            base.Draw(graphics);

            if (_diagramImage != null)
            {
                #region Draw Image
                Rectangle rectangleImage = this.ClientRectangle;

                rectangleImage.X = (rectangleImage.Width - this._diagramImage.Width) / 2;
                rectangleImage.Y = 2;
                rectangleImage.Size = this._diagramImage.Size;

                graphics.DrawImage(_diagramImage, rectangleImage);
                #endregion

                #region Draw Text
                // render text
                using (Font font = new Font(this.FontName, this.FontSize))
                {
                    Rectangle rectangleText = this.ClientRectangle;

                    rectangleText.X = 0;
                    rectangleText.Y = rectangleImage.Height + 5;
                    rectangleText.Width = this.Width - 1;
                    rectangleText.Height = this.Height - rectangleImage.Height - 5;

                    using (SGI.TextPainter painter = new SGI.TextPainter())
                        painter.DrawString(graphics, this.Text, font, rectangleText.X, rectangleText.Y, rectangleText.Width - 2, rectangleText.Height, Color.Black, true);

                    using (Pen p = new Pen(Color.LightGray))
                    {
                        //graphics.DrawRectangle(p, rectangleText);
                        graphics.DrawPath(p, SGI.DrawHelper.BuildRndRect(rectangleText));
                        _selectArea = rectangleText;
                    }
                }
                #endregion
            }

            
        }
    }
}
