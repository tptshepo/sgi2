﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace SGIDemo
{
  public partial class Form1 : Form
  {
    public Form1()
    {
      InitializeComponent();
    }

    private void Form1_Load(object sender, EventArgs e)
    {
      GetCanvasImage();
    }

    private void GetCanvasImage()
    {
      canvas1.HidePathBar();

      string icon = @"E:\Projects\NEDBANK\SuperBoss Version Control\SuperBoss_PROD_V4\SourceCode\SuperBossSolution\YFlow\SourceCode\YFlow.ConfigurationTool\bin\Debug\images\location3.png";

      DiagramEntity ent = canvas1.CreateEntity<DiagramEntity>("Location 1", true, false, true);
      ent.DiagramImage = Image.FromFile(icon);
      ent.Location = new Point(0, 0);
      ent.Size = new Size(107, 100);

      DiagramEntity ent2 = canvas1.CreateEntity<DiagramEntity>("Location 2", true, false, true);
      ent2.DiagramImage = Image.FromFile(icon);
      ent2.Location = new Point(600, 300);
      ent2.Size = new Size(107, 100);

      var conn = canvas1.CreateConnection(ent, ent2, false);
      conn.CanDelete = false;

      //canvas1.CanMultiDelete = false;

    }
  }
}
