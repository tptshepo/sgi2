# Spotty Graphics Interface #

SGI is a GDI+ graphics library for rendering 2D objects on a windows form that can be dragged around and customised. 
I started the project because I wanted to build a visual flow chart like diagram to control code execution for a workflow. 

### Technologies used ###

* Microsoft .Net Framework - C#
