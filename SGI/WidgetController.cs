using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace SGI
{
  /// <summary>
  /// Basic shape types that a widget can be
  /// </summary>
  public enum ObjectTypes
  {
    /// <summary>
    /// Rectangular shape
    /// </summary>
    Rectangle,
    /// <summary>
    /// Line shape
    /// </summary>
    Line
  };

  /// <summary>
  /// This class implements sizing and moving functions for
  ///	the GComponent
  /// </summary>
  [Serializable]
  public class WidgetController : IDisposable
  {
    #region Fields
    private bool _movable = true;
    private bool _selected = false;
    private bool _resizable = true;

    private Color BOX_COLOR = Color.White;
    private Widget _control;
    private int startl;
    private int startt;
    private int startw;
    private int starth;
    private int startx;
    private int starty;
    private bool dragging;

    private Cursor[] arrArrow = new Cursor[] {Cursors.SizeNWSE, Cursors.SizeNS,
			Cursors.SizeNESW, Cursors.SizeWE, Cursors.SizeNWSE, Cursors.SizeNS,
			Cursors.SizeNESW, Cursors.SizeWE};


    private int _preferedSize = 25;
    private int _minConnectorSize = 6;

    private ObjectTypes _objectType = ObjectTypes.Rectangle;
    private Point _linePointStart;
    private Point _linePointEnd;
    #endregion

    #region Properties
    /// <summary>
    /// Gets or sets if the Widget if resizerble
    /// </summary>
    public bool Resizable
    {
      get { return _resizable; }
      set { _resizable = value; }
    }
    /// <summary>
    /// Gets or sets the object type
    /// </summary>
    public ObjectTypes ObjectType
    {
      get { return _objectType; }
      set { _objectType = value; }
    }
    /// <summary>
    /// Gets or sets the start point
    /// </summary>
    public Point LinePointStart
    {
      get { return _linePointStart; }
      set { _linePointStart = value; }
    }
    /// <summary>
    /// Gets or sets the end point
    /// </summary>
    public Point LinePointEnd
    {
      get { return _linePointEnd; }
      set { _linePointEnd = value; }
    }
    /// <summary>
    /// Gets or sets the minimum size of the Connector
    /// </summary>
    public int MinConnectorSize
    {
      get { return _minConnectorSize; }
      set { _minConnectorSize = value; }
    }
    /// <summary>
    /// Gets or sets the Prefered size of the Widget
    /// </summary>
    public int PreferedSize
    {
      get { return _preferedSize; }
      set { _preferedSize = value; }
    }
    /// <summary>
    /// Gets or sets if the control can be moved
    /// </summary>
    public bool Movable
    {
      get { return _movable; }
      set { _movable = value; }
    }

    #endregion

    #region Constructor
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="ctl">The widget to apply motion support to</param>
    public WidgetController(Widget ctl)
    {
      WireControl(ctl);
    }

    #endregion

    #region Methods

    /// <summary>
    /// Wires a Click event handler to the passed Control
    /// </summary>
    /// <param name="ctl"></param>
    public void WireControl(Widget ctl)
    {
      ctl.Click += new EventHandler(this.SelectControl);

      _control = (Widget)ctl;

      //Add event handlers for moving the selected control around
      ctl.MouseDown += new MouseEventHandler(this.ctl_MouseDown);
      ctl.MouseMove += new MouseEventHandler(this.ctl_MouseMove);
      ctl.MouseUp += new MouseEventHandler(this.ctl_MouseUp);
    }

    private void SelectControl(object sender, EventArgs e)
    {
    }

    #endregion

    #region Control Event Handlers

    // Get mouse pointer starting position on mouse down and hide sizing handles
    private void ctl_MouseDown(object sender, MouseEventArgs e)
    {
      if (!_movable)
        return;

      dragging = true;
      startx = e.X;
      starty = e.Y;
      System.Diagnostics.Debug.WriteLine(string.Format("===============Start Drag: {0}x{1}", startx, starty));
    }

    // Reposition the dragged control
    private void ctl_MouseMove(object sender, MouseEventArgs e)
    {
      if (dragging)
      {
        int currentX = _control.X;
        int currentY = _control.Y;

        int newLeft = _control.X + e.X - startx;
        System.Diagnostics.Debug.WriteLine(string.Format("newLeft: {0}", newLeft));

        int newTop = _control.Y + e.Y - starty;
        System.Diagnostics.Debug.WriteLine(string.Format("newTop: {0}", newTop));

        //keep the control within bounds of the container 
        newLeft = Math.Max(newLeft, 0);
        newTop = Math.Max(newTop, 0);

        if (_control.Parent.Control.MultipleSelected)
        {
          _control.Parent.Control.UpdateDragOverlayPosition();

          int overlayLeft = _control.Parent.Control.DragOverlayLeft + (newLeft - currentX);
          System.Diagnostics.Debug.WriteLine(string.Format("overlayLeft: {0}", overlayLeft));

          int overlayTop = _control.Parent.Control.DragOverlayTop + (newTop - currentY);
          System.Diagnostics.Debug.WriteLine(string.Format("overlayTop: {0}", overlayTop));

          if (overlayLeft <= 0)
          {
            newLeft = currentX;// +Math.Abs(newLeft - currentX);
          }
          if (overlayTop <= 0)
          {
            newTop = currentY;// +Math.Abs(newTop - currentY);
          }
        }
        else
        {
          /*don't snap to grid if multiple widgets are being moved*/

          //scale to units 
          double ux = newLeft / 20.0;
          double uy = newTop / 20.0;

          //find the closest unit and scale back - snap to grid
          newLeft = (int)Math.Round(ux) * 20;
          newTop = (int)Math.Round(uy) * 20;
        }

        _control.Location = new Point(newLeft, newTop);
      }
    }

    //Display sizing handles around picked control once dragging has completed
    private void ctl_MouseUp(object sender, MouseEventArgs e)
    {
      dragging = false;
      System.Diagnostics.Debug.WriteLine(string.Format("===============End Drag"));
    }

    #endregion

    #region IDisposable Members

    /// <summary>
    /// Release resources
    /// </summary>
    public void Dispose()
    {
      //Remove event any pre-existing event handlers appended by this class
      _control.MouseDown -= new MouseEventHandler(this.ctl_MouseDown);
      _control.MouseMove -= new MouseEventHandler(this.ctl_MouseMove);
      _control.MouseUp -= new MouseEventHandler(this.ctl_MouseUp);

      _control.Click -= new EventHandler(this.SelectControl);

      //_connectors = null;
      _control = null;
    }

    #endregion
  }
}
