﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace SGI
{
    /// <summary>
    /// This interface is used to animate widgets
    /// </summary>
    interface IAmination
    {
        /// <summary>
        /// start the amination
        /// </summary>
        void Start();

        /// <summary>
        /// Stop the animation
        /// </summary>
        void Stop();

        /// <summary>
        /// Gets or sets the speed of the animation
        /// </summary>
        int Speed { get; set; }

        /// <summary>
        /// Gets or sets the animation path
        /// </summary>
        List<Point> Path { get; set; }

    }

}
