using System;
using System.Data;
using System.Text;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace SGI
{
    internal class WindowAPI
    {
        #region WIN32 API TYPES
        public enum Messages
        {

            LVM_FIRST = 0x1000,
            LVM_GETITEMCOUNT = LVM_FIRST + 4,

            LB_ADDSTRING = 0x0180,
            LB_INSERTSTRING = 0x0181,
            LB_DELETESTRING = 0x0182,
            LB_SETSEL = 0x0185,
            LB_SETCURSEL = 0x0186,
            LB_GETSEL = 0x0187,
            LB_GETCURSEL = 0x188,
            LB_GETTEXT = 0x189,
            LB_GETTEXTLEN = 0x18a,
            LB_GETCOUNT = 0x18b,
            LB_SELECTSTRING = 0x18c,
            LB_DIR = 0x18d,
            LB_FINDSTRING = 0x18f,
            LB_GETSELCOUNT = 0x190,
            LB_GETSELITEMS = 0x191,
            LB_GETITEMDATA = 0x199,
            LB_SETITEMDATA = 0x19a,
            LBN_SELCHANGE = 1,
            LBN_DBLCLK = 2,


            CB_GETEDITSEL = 0x0140,
            CB_LIMITTEXT = 0x0141,
            CB_SETEDITSEL = 0x0142,
            CB_ADDSTRING = 0x0143,
            CB_DELETESTRING = 0x0144,
            CB_DIR = 0x0145,
            CB_GETCOUNT = 0x0146,
            CB_GETCURSEL = 0x0147,
            CB_GETLBTEXT = 0x0148,
            CB_GETLBTEXTLEN = 0x0149,
            CB_INSERTSTRING = 0x014A,
            CB_RESETCONTENT = 0x014B,
            CB_FINDSTRING = 0x014C,
            CB_SELECTSTRING = 0x014D,
            CB_SETCURSEL = 0x014E,
            CB_SHOWDROPDOWN = 0x014F,
            CB_GETITEMDATA = 0x0150,
            CB_SETITEMDATA = 0x0151,
            CB_GETDROPPEDCONTROLRECT = 0x0152,
            CB_SETITEMHEIGHT = 0x0153,
            CB_GETITEMHEIGHT = 0x0154,
            CB_SETEXTENDEDUI = 0x0155,
            CB_GETEXTENDEDUI = 0x0156,
            CB_GETDROPPEDSTATE = 0x0157,
            CB_FINDSTRINGEXACT = 0x0158,
            CB_SETLOCALE = 0x0159,
            CB_GETLOCALE = 0x015A,
            CB_GETTOPINDEX = 0x015B,
            CB_SETTOPINDEX = 0x015C,
            CB_GETHORIZONTALEXTENT = 0x015D,
            CB_SETHORIZONTALEXTENT = 0x015E,
            CB_GETDROPPEDWIDTH = 0x015F,
            CB_SETDROPPEDWIDTH = 0x0160,
            CB_INITSTORAGE = 0x0161,

            TCM_FIRST = 0x1300,
            TCM_GETIMAGELIST = (TCM_FIRST + 2),
            TCM_SETIMAGELIST = (TCM_FIRST + 3),
            TCM_GETITEMCOUNT = (TCM_FIRST + 4),
            TCM_GETITEMA = (TCM_FIRST + 5),
            TCM_GETITEMW = (TCM_FIRST + 60),
            TCM_SETITEMA = (TCM_FIRST + 6),
            TCM_SETITEMW = (TCM_FIRST + 61),
            TCM_INSERTITEMA = (TCM_FIRST + 7),
            TCM_INSERTITEMW = (TCM_FIRST + 62),
            TCM_DELETEITEM = (TCM_FIRST + 8),
            TCM_DELETEALLITEMS = (TCM_FIRST + 9),
            TCM_GETITEMRECT = (TCM_FIRST + 10),
            TCM_GETCURSEL = (TCM_FIRST + 11),
            TCM_SETCURSEL = (TCM_FIRST + 12),
            TCM_HITTEST = (TCM_FIRST + 13),
            TCM_SETITEMEXTRA = (TCM_FIRST + 14),
            TCM_ADJUSTRECT = (TCM_FIRST + 40),
            TCM_SETITEMSIZE = (TCM_FIRST + 41),
            TCM_REMOVEIMAGE = (TCM_FIRST + 42),
            TCM_SETPADDING = (TCM_FIRST + 43),
            TCM_GETROWCOUNT = (TCM_FIRST + 44),
            TCM_GETCURFOCUS = (TCM_FIRST + 47),
            TCM_SETCURFOCUS = (TCM_FIRST + 48),
            TCM_SETMINTABWIDTH = (TCM_FIRST + 49),
            TCM_DESELECTALL = (TCM_FIRST + 50),
            TCM_HIGHLIGHTITEM = (TCM_FIRST + 51),
            TCM_SETEXTENDEDSTYLE = (TCM_FIRST + 52), // optional wParam == mask
            TCM_GETEXTENDEDSTYLE = (TCM_FIRST + 53),

            TCHT_NOWHERE = 0x0001,
            TCHT_ONITEMICON = 0x0002,
            TCHT_ONITEMLABEL = 0x0004,
            TCHT_ONITEM = TCHT_ONITEMICON | TCHT_ONITEMLABEL,

            MOUSEEVENTF_LEFTDOWN = 0x0002,
            MOUSEEVENTF_LEFTUP = 0x0004,
            MOUSEEVENTF_RIGHTDOWN = 0x0008,
            MOUSEEVENTF_RIGHTUP = 0x0010,
            MK_LBUTTON = 0x0001,
            MK_RBUTTON = 0x0002,
            MK_SHIFT = 0x0004,
            MK_CONTROL = 0x0008,
            MK_MBUTTON = 0x0010,
            MK_ALT = 0x1000,
            WM_UNKNOWN = 0,
            WM_NULL = 0x0000,
            WM_CREATE = 0x0001,
            WM_DESTROY = 0x0002,
            WM_MOVE = 0x0003,
            WM_SIZE = 0x0005,
            WM_ACTIVATE = 0x0006,
            WM_SETFOCUS = 0x0007,
            WM_KILLFOCUS = 0x0008,
            WM_ENABLE = 0x000A,
            WM_SETREDRAW = 0x000B,
            WM_SETTEXT = 0x000C,
            WM_GETTEXT = 0x000D,
            WM_GETTEXTLENGTH = 0x000E,
            WM_PAINT = 0x000F,
            WM_CLOSE = 0x0010,
            WM_QUERYENDSESSION = 0x0011,
            WM_QUIT = 0x0012,
            WM_QUERYOPEN = 0x0013,
            WM_ERASEBKGND = 0x0014,
            WM_SYSCOLORCHANGE = 0x0015,
            WM_ENDSESSION = 0x0016,
            WM_SHOWWINDOW = 0x0018,
            WM_CTLCOLOR = 0x0019,
            WM_WININICHANGE = 0x001A,
            WM_SETTINGCHANGE = 0x001A,
            WM_DEVMODECHANGE = 0x001B,
            WM_ACTIVATEAPP = 0x001C,
            WM_FONTCHANGE = 0x001D,
            WM_TIMECHANGE = 0x001E,
            WM_CANCELMODE = 0x001F,
            WM_SETCURSOR = 0x0020,
            WM_MOUSEACTIVATE = 0x0021,
            WM_CHILDACTIVATE = 0x0022,
            WM_QUEUESYNC = 0x0023,
            WM_GETMINMAXINFO = 0x0024,
            WM_PAINTICON = 0x0026,
            WM_ICONERASEBKGND = 0x0027,
            WM_NEXTDLGCTL = 0x0028,
            WM_SPOOLERSTATUS = 0x002A,
            WM_DRAWITEM = 0x002B,
            WM_MEASUREITEM = 0x002C,
            WM_DELETEITEM = 0x002D,
            WM_VKEYTOITEM = 0x002E,
            WM_CHARTOITEM = 0x002F,
            WM_SETFONT = 0x0030,
            WM_GETFONT = 0x0031,
            WM_SETHOTKEY = 0x0032,
            WM_GETHOTKEY = 0x0033,
            WM_QUERYDRAGICON = 0x0037,
            WM_COMPAREITEM = 0x0039,
            WM_GETOBJECT = 0x003D,
            WM_COMPACTING = 0x0041,
            WM_COMMNOTIFY = 0x0044,
            WM_WINDOWPOSCHANGING = 0x0046,
            WM_WINDOWPOSCHANGED = 0x0047,
            WM_POWER = 0x0048,
            WM_COPYDATA = 0x004A,
            WM_CANCELJOURNAL = 0x004B,
            WM_NOTIFY = 0x004E,
            WM_INPUTLANGCHANGEREQUEST = 0x0050,
            WM_INPUTLANGCHANGE = 0x0051,
            WM_TCARD = 0x0052,
            WM_HELP = 0x0053,
            WM_USERCHANGED = 0x0054,
            WM_NOTIFYFORMAT = 0x0055,
            WM_CONTEXTMENU = 0x007B,
            WM_STYLECHANGING = 0x007C,
            WM_STYLECHANGED = 0x007D,
            WM_DISPLAYCHANGE = 0x007E,
            WM_GETICON = 0x007F,
            WM_SETICON = 0x0080,
            WM_NCCREATE = 0x0081,
            WM_NCDESTROY = 0x0082,
            WM_NCCALCSIZE = 0x0083,
            WM_NCHITTEST = 0x0084,
            WM_NCPAINT = 0x0085,
            WM_NCACTIVATE = 0x0086,
            WM_GETDLGCODE = 0x0087,
            WM_SYNCPAINT = 0x0088,
            WM_NCMOUSEMOVE = 0x00A0,
            WM_NCLBUTTONDOWN = 0x00A1,
            WM_NCLBUTTONUP = 0x00A2,
            WM_NCLBUTTONDBLCLK = 0x00A3,
            WM_NCRBUTTONDOWN = 0x00A4,
            WM_NCRBUTTONUP = 0x00A5,
            WM_NCRBUTTONDBLCLK = 0x00A6,
            WM_NCMBUTTONDOWN = 0x00A7,
            WM_NCMBUTTONUP = 0x00A8,
            WM_NCMBUTTONDBLCLK = 0x00A9,
            WM_KEYDOWN = 0x0100,
            WM_KEYUP = 0x0101,
            WM_CHAR = 0x0102,
            WM_DEADCHAR = 0x0103,
            WM_SYSKEYDOWN = 0x0104,
            WM_SYSKEYUP = 0x0105,
            WM_SYSCHAR = 0x0106,
            WM_SYSDEADCHAR = 0x0107,
            WM_KEYLAST = 0x0108,
            WM_IME_STARTCOMPOSITION = 0x010D,
            WM_IME_ENDCOMPOSITION = 0x010E,
            WM_IME_COMPOSITION = 0x010F,
            WM_IME_KEYLAST = 0x010F,
            WM_INITDIALOG = 0x0110,
            WM_COMMAND = 0x0111,
            WM_SYSCOMMAND = 0x0112,
            WM_TIMER = 0x0113,
            WM_HSCROLL = 0x0114,
            WM_VSCROLL = 0x0115,
            WM_INITMENU = 0x0116,
            WM_INITMENUPOPUP = 0x0117,
            WM_MENUSELECT = 0x011F,
            WM_MENUCHAR = 0x0120,
            WM_ENTERIDLE = 0x0121,
            WM_MENURBUTTONUP = 0x0122,
            WM_MENUDRAG = 0x0123,
            WM_MENUGETOBJECT = 0x0124,
            WM_UNINITMENUPOPUP = 0x0125,
            WM_MENUCOMMAND = 0x0126,
            WM_CTLCOLORMSGBOX = 0x0132,
            WM_CTLCOLOREDIT = 0x0133,
            WM_CTLCOLORLISTBOX = 0x0134,
            WM_CTLCOLORBTN = 0x0135,
            WM_CTLCOLORDLG = 0x0136,
            WM_CTLCOLORSCROLLBAR = 0x0137,
            WM_CTLCOLORSTATIC = 0x0138,
            WM_MOUSEMOVE = 0x0200,
            WM_LBUTTONDOWN = 0x0201,
            WM_LBUTTONUP = 0x0202,
            WM_LBUTTONDBLCLK = 0x0203,
            WM_RBUTTONDOWN = 0x0204,
            WM_RBUTTONUP = 0x0205,
            WM_RBUTTONDBLCLK = 0x0206,
            WM_MBUTTONDOWN = 0x0207,
            WM_MBUTTONUP = 0x0208,
            WM_MBUTTONDBLCLK = 0x0209,
            WM_MOUSEWHEEL = 0x020A,
            WM_PARENTNOTIFY = 0x0210,
            WM_ENTERMENULOOP = 0x0211,
            WM_EXITMENULOOP = 0x0212,
            WM_NEXTMENU = 0x0213,
            WM_SIZING = 0x0214,
            WM_CAPTURECHANGED = 0x0215,
            WM_MOVING = 0x0216,
            WM_DEVICECHANGE = 0x0219,
            WM_MDICREATE = 0x0220,
            WM_MDIDESTROY = 0x0221,
            WM_MDIACTIVATE = 0x0222,
            WM_MDIRESTORE = 0x0223,
            WM_MDINEXT = 0x0224,
            WM_MDIMAXIMIZE = 0x0225,
            WM_MDITILE = 0x0226,
            WM_MDICASCADE = 0x0227,
            WM_MDIICONARRANGE = 0x0228,
            WM_MDIGETACTIVE = 0x0229,
            WM_MDISETMENU = 0x0230,
            WM_ENTERSIZEMOVE = 0x0231,
            WM_EXITSIZEMOVE = 0x0232,
            WM_DROPFILES = 0x0233,
            WM_MDIREFRESHMENU = 0x0234,
            WM_IME_SETCONTEXT = 0x0281,
            WM_IME_NOTIFY = 0x0282,
            WM_IME_CONTROL = 0x0283,
            WM_IME_COMPOSITIONFULL = 0x0284,
            WM_IME_SELECT = 0x0285,
            WM_IME_CHAR = 0x0286,
            WM_IME_REQUEST = 0x0288,
            WM_IME_KEYDOWN = 0x0290,
            WM_IME_KEYUP = 0x0291,
            WM_MOUSEHOVER = 0x02A1,
            WM_MOUSELEAVE = 0x02A3,
            WM_CUT = 0x0300,
            WM_COPY = 0x0301,
            WM_PASTE = 0x0302,
            WM_CLEAR = 0x0303,
            WM_UNDO = 0x0304,
            WM_RENDERFORMAT = 0x0305,
            WM_RENDERALLFORMATS = 0x0306,
            WM_DESTROYCLIPBOARD = 0x0307,
            WM_DRAWCLIPBOARD = 0x0308,
            WM_PAINTCLIPBOARD = 0x0309,
            WM_VSCROLLCLIPBOARD = 0x030A,
            WM_SIZECLIPBOARD = 0x030B,
            WM_ASKCBFORMATNAME = 0x030C,
            WM_CHANGECBCHAIN = 0x030D,
            WM_HSCROLLCLIPBOARD = 0x030E,
            WM_QUERYNEWPALETTE = 0x030F,
            WM_PALETTEISCHANGING = 0x0310,
            WM_PALETTECHANGED = 0x0311,
            WM_HOTKEY = 0x0312,
            WM_PRINT = 0x0317,
            WM_PRINTCLIENT = 0x0318,
            WM_HANDHELDFIRST = 0x0358,
            WM_HANDHELDLAST = 0x035F,
            WM_AFXFIRST = 0x0360,
            WM_AFXLAST = 0x037F,
            WM_PENWINFIRST = 0x0380,
            WM_PENWINLAST = 0x038F,
            WM_APP = 0x8000,
            WM_USER = 0x0400,
            WM_REFLECT = WM_USER + 0x1c00,

            KEYEVENTF_KEYUP = 2,

            VK_LBUTTON = 1,
            VK_RBUTTON = 2,
            VK_CANCEL = 3,          //Used for control-break processing.
            VK_MBUTTON = 4,         //Middle mouse button (3-button mouse).
            VK_BACK = 8,            //spaceback
            VK_TAB = 9,
            VK_CLEAR = 12,
            VK_PR = 42,
            VK_RETURN = 13,         //enter          
            VK_SHIFT = 0x10,
            VK_CONTROL = 17,
            VK_SCROLL = 145,
            VK_MENU = 18,
            VK_PAUSE = 19,
            VK_CAPITAL = 0x14,
            VK_ESCAPE = 27,
            VK_STARTKEY = 91,        // Start Menu key
            VK_SPACE = 32,
            VK_PRIOR = 33,
            VK_NEXT = 34,
            VK_END = 35,
            VK_HOME = 36,
            VK_LEFT = 37,
            VK_UP = 38,
            VK_RIGHT = 39,
            VK_DOWN = 40,
            VK_SELECT = 41,
            VK_INSERT = 45,
            VK_DELETE = 46,
            VK_HELP = 47,
            VK_OEM_COMMA = 188,
            VK_NUMLOCK = 144,
            KEYEVENTF_EXTENDEDKEY = 1,

            VK_0 = 48,
            VK_1 = 49,
            VK_2 = 50,
            VK_3 = 51,
            VK_4 = 52,
            VK_5 = 53,
            VK_6 = 54,
            VK_7 = 55,
            VK_8 = 56,
            VK_9 = 57,

            VK_A = 65,
            VK_B = 66,
            VK_C = 67,
            VK_D = 68,
            VK_E = 69,
            VK_F = 70,
            VK_G = 71,
            VK_H = 72,
            VK_I = 73,
            VK_J = 74,
            VK_K = 75,
            VK_L = 76,
            VK_M = 77,
            VK_N = 78,
            VK_O = 79,
            VK_P = 80,
            VK_Q = 81,
            VK_R = 82,
            VK_S = 83,
            VK_T = 84,
            VK_U = 85,
            VK_V = 86,
            VK_W = 87,
            VK_X = 88,
            VK_Y = 89,
            VK_Z = 90,

            VK_a = 0x061,
            VK_b = 0x062,
            VK_c = 0x063,
            VK_d = 0x064,
            VK_e = 0x065,
            VK_f = 0x066,
            VK_g = 0x067,
            VK_h = 0x068,
            VK_i = 0x069,
            VK_j = 0x06A,
            VK_k = 0x06B,
            VK_l = 0x06C,
            VK_m = 0x06D,
            VK_n = 0x06E,
            VK_o = 0x06F,
            VK_p = 0x070,
            VK_q = 0x071,
            VK_r = 0x072,
            VK_s = 0x073,
            VK_t = 0x074,
            VK_u = 0x075,
            VK_v = 0x076,
            VK_w = 0x077,
            VK_x = 0x078,
            VK_y = 0x079,
            VK_z = 0x07A,

            VK_NUMPAD0 = 96,
            VK_NUMPAD1 = 97,
            VK_NUMPAD2 = 98,
            VK_NUMPAD3 = 99,
            VK_NUMPAD4 = 100,
            VK_NUMPAD5 = 101,
            VK_NUMPAD6 = 102,
            VK_NUMPAD7 = 103,
            VK_NUMPAD8 = 104,
            VK_NUMPAD9 = 105,
            VK_MULTIPLY = 106,

            VK_ADD = 107,
            VK_SEPARATER = 108,
            VK_SEPARATOR = 108,
            VK_SUBTRACT = 109,
            VK_DECIMAL = 110,
            VK_DIVIDE = 111,
            VK_OEM_2 = 191,
            VK_OEM_MINUS = 0xBD,        // -
            VK_UNDERSCORE = 0x05F,      // _
            VK_OEM_PERIOD = 190,        // .
            VK_EQUAL = 0x03D,            // =
            VK_HYPHEN = 0xBD,           // -

            VK_F1 = 112,
            VK_F2 = 113,
            VK_F3 = 114,
            VK_F4 = 115,
            VK_F5 = 116,
            VK_F6 = 117,
            VK_F7 = 118,
            VK_F8 = 119,
            VK_F9 = 120,
            VK_F10 = 121,
            VK_F11 = 122,
            VK_F12 = 123,
            VK_F13 = 124,
            VK_F14 = 125,
            VK_F15 = 126,
            VK_F16 = 127,
            VK_F17 = 128,
            VK_F18 = 129,
            VK_F19 = 130,
            VK_F20 = 131,
            VK_F21 = 132,
            VK_F22 = 133,
            VK_F23 = 134,
            VK_F24 = 135,

            VK_CRSEL = 247,
            VK_EREOF = 249,
            VK_EXECUTE = 43,
            VK_EXSEL = 248,
            VK_NONAME = 252,
            VK_OEM_CLEAR = 254,
            VK_PA1 = 253,
            VK_PROCESSKEY = 229,

            CAPSLOCK_ON = 128,
            BN_CLICKED = 245,
        };

        [Flags]
        public enum MouseEventFlags
        {
            LEFTDOWN = 0x00000002,
            LEFTUP = 0x00000004,
            MIDDLEDOWN = 0x00000020,
            MIDDLEUP = 0x00000040,
            MOVE = 0x00000001,
            ABSOLUTE = 0x00008000,
            RIGHTDOWN = 0x00000008,
            RIGHTUP = 0x00000010
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MENUITEMINFO
        {
            public uint cbSize;
            public uint fMask;
            public uint fType;
            public uint fState;
            public int wID;
            public int hSubMenu;
            public int hbmpChecked;
            public int hbmpUnchecked;
            public int dwItemData;
            public string dwTypeData;
            public uint cch;
            public int hbmpItem;
        }

        // Values for the fMask parameter
        public const UInt32 MIM_MAXHEIGHT = 0x00000001;
        public const UInt32 MIM_BACKGROUND = 0x00000002;
        public const UInt32 MIM_HELPID = 0x00000004;
        public const UInt32 MIM_MENUDATA = 0x00000008;
        public const UInt32 MIM_STYLE = 0x00000010;
        public const UInt32 MIM_APPLYTOSUBMENUS = 0x80000000;

        //menu types
        public const uint WM_COMMAND = 273;
        public const uint WM_MENUSELECT = 0x011F;

        public enum MENUFLAG : uint
        {
            MF_INSERT = 0x00000000,
            MF_CHANGE = 0x00000080,
            MF_APPEND = 0x00000100,
            MF_DELETE = 0x00000200,
            MF_REMOVE = 0x00001000,

            MF_BYCOMMAND = 0x00000000,
            MF_BYPOSITION = 0x00000400,

            MF_SEPARATOR = 0x00000800,

            MF_ENABLED = 0x00000000,
            MF_GRAYED = 0x00000001,
            MF_DISABLED = 0x00000002,

            MF_UNCHECKED = 0x00000000,
            MF_CHECKED = 0x00000008,
            MF_USECHECKBITMAPS = 0x00000200,

            MF_STRING = 0x00000000,
            MF_BITMAP = 0x00000004,
            MF_OWNERDRAW = 0x00000100,

            MF_POPUP = 0x00000010,
            MF_MENUBARBREAK = 0x00000020,
            MF_MENUBREAK = 0x00000040,

            MF_UNHILITE = 0x00000000,
            MF_HILITE = 0x00000080,

            MF_DEFAULT = 0x00001000,
            MF_SYSMENU = 0x00002000,
            MF_HELP = 0x00004000,
            MF_RIGHTJUSTIFY = 0x00004000,

            MF_MOUSESELECT = 0x00008000,

            MFT_STRING = MF_STRING,
            MFT_BITMAP = MF_BITMAP,
            MFT_MENUBARBREAK = MF_MENUBARBREAK,
            MFT_MENUBREAK = MF_MENUBREAK,
            MFT_OWNERDRAW = MF_OWNERDRAW,
            MFT_RADIOCHECK = 0x00000200,
            MFT_SEPARATOR = MF_SEPARATOR,
            MFT_RIGHTORDER = 0x00002000,
            MFT_RIGHTJUSTIFY = MF_RIGHTJUSTIFY,

            MFS_GRAYED = 0x00000003,
            MFS_DISABLED = MFS_GRAYED,
            MFS_CHECKED = MF_CHECKED,
            MFS_HILITE = MF_HILITE,
            MFS_ENABLED = MF_ENABLED,
            MFS_UNCHECKED = MF_UNCHECKED,
            MFS_UNHILITE = MF_UNHILITE,
            MFS_DEFAULT = MF_DEFAULT,
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MENUINFO
        {
            public Int32 cbSize;
            public Int32 fMask;
            public Int32 dwStyle;
            public Int32 cyMax;
            public IntPtr hbrBack;
            public Int32 dwContextHelpID;
            public Int32 dwMenuData;
        }

        [StructLayout(LayoutKind.Sequential)]
        public class TCHITTESTINFO
        {
            public Point pt;
            public int flags = 0;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        public enum fnStyle
        {
            HS_BDIAGONAL = 3,               //'  /////
            HS_CROSS = 4,                   //'  +++++
            HS_DIAGCROSS = 5,               //'  xxxxx
            HS_FDIAGONAL = 2,               //'  \\\\\
            HS_HORIZONTAL = 0,              //'  -----
            HS_VERTICAL = 1                 //'  |||||

        };

        public enum RedrawWindowflags
        {
            RDW_INVALIDATE = 0x0001,
            RDW_INTERNALPAINT = 0x0002,
            RDW_ERASE = 0x0004,

            RDW_VALIDATE = 0x0008,
            RDW_NOINTERNALPAINT = 0x0010,
            RDW_NOERASE = 0x0020,

            RDW_NOCHILDREN = 0x0040,
            RDW_ALLCHILDREN = 0x0080,

            RDW_UPDATENOW = 0x0100,
            RDW_ERASENOW = 0x0200,

            RDW_FRAME = 0x0400,
            RDW_NOFRAME = 0x0800
        };

        public enum VirtualKeyStates : int
        {
            VK_LBUTTON = 0x01,
            VK_RBUTTON = 0x02,
            VK_CANCEL = 0x03,
            VK_MBUTTON = 0x04,
            //
            VK_XBUTTON1 = 0x05,
            VK_XBUTTON2 = 0x06,
            //
            VK_BACK = 0x08,
            VK_TAB = 0x09,
            //
            VK_CLEAR = 0x0C,
            VK_RETURN = 0x0D,
            //
            VK_SHIFT = 0x10,
            VK_CONTROL = 0x11,
            VK_MENU = 0x12,
            VK_PAUSE = 0x13,
            VK_CAPITAL = 0x14,
            //
            VK_KANA = 0x15,
            VK_HANGEUL = 0x15,  /* old name - should be here for compatibility */
            VK_HANGUL = 0x15,
            VK_JUNJA = 0x17,
            VK_FINAL = 0x18,
            VK_HANJA = 0x19,
            VK_KANJI = 0x19,
            //
            VK_ESCAPE = 0x1B,
            //
            VK_CONVERT = 0x1C,
            VK_NONCONVERT = 0x1D,
            VK_ACCEPT = 0x1E,
            VK_MODECHANGE = 0x1F,
            //
            VK_SPACE = 0x20,
            VK_PRIOR = 0x21,
            VK_NEXT = 0x22,
            VK_END = 0x23,
            VK_HOME = 0x24,
            VK_LEFT = 0x25,
            VK_UP = 0x26,
            VK_RIGHT = 0x27,
            VK_DOWN = 0x28,
            VK_SELECT = 0x29,
            VK_PRINT = 0x2A,
            VK_EXECUTE = 0x2B,
            VK_SNAPSHOT = 0x2C,
            VK_INSERT = 0x2D,
            VK_DELETE = 0x2E,
            VK_HELP = 0x2F,
            //
            VK_LWIN = 0x5B,
            VK_RWIN = 0x5C,
            VK_APPS = 0x5D,
            //
            VK_SLEEP = 0x5F,
            //
            VK_NUMPAD0 = 0x60,
            VK_NUMPAD1 = 0x61,
            VK_NUMPAD2 = 0x62,
            VK_NUMPAD3 = 0x63,
            VK_NUMPAD4 = 0x64,
            VK_NUMPAD5 = 0x65,
            VK_NUMPAD6 = 0x66,
            VK_NUMPAD7 = 0x67,
            VK_NUMPAD8 = 0x68,
            VK_NUMPAD9 = 0x69,
            VK_MULTIPLY = 0x6A,
            VK_ADD = 0x6B,
            VK_SEPARATOR = 0x6C,
            VK_SUBTRACT = 0x6D,
            VK_DECIMAL = 0x6E,
            VK_DIVIDE = 0x6F,
            VK_F1 = 0x70,
            VK_F2 = 0x71,
            VK_F3 = 0x72,
            VK_F4 = 0x73,
            VK_F5 = 0x74,
            VK_F6 = 0x75,
            VK_F7 = 0x76,
            VK_F8 = 0x77,
            VK_F9 = 0x78,
            VK_F10 = 0x79,
            VK_F11 = 0x7A,
            VK_F12 = 0x7B,
            VK_F13 = 0x7C,
            VK_F14 = 0x7D,
            VK_F15 = 0x7E,
            VK_F16 = 0x7F,
            VK_F17 = 0x80,
            VK_F18 = 0x81,
            VK_F19 = 0x82,
            VK_F20 = 0x83,
            VK_F21 = 0x84,
            VK_F22 = 0x85,
            VK_F23 = 0x86,
            VK_F24 = 0x87,
            //
            VK_NUMLOCK = 0x90,
            VK_SCROLL = 0x91,
            //
            VK_OEM_NEC_EQUAL = 0x92,   // '=' key on numpad
            //
            VK_OEM_FJ_JISHO = 0x92,   // 'Dictionary' key
            VK_OEM_FJ_MASSHOU = 0x93,   // 'Unregister word' key
            VK_OEM_FJ_TOUROKU = 0x94,   // 'Register word' key
            VK_OEM_FJ_LOYA = 0x95,   // 'Left OYAYUBI' key
            VK_OEM_FJ_ROYA = 0x96,   // 'Right OYAYUBI' key
            //
            VK_LSHIFT = 0xA0,
            VK_RSHIFT = 0xA1,
            VK_LCONTROL = 0xA2,
            VK_RCONTROL = 0xA3,
            VK_LMENU = 0xA4,
            VK_RMENU = 0xA5,
            //
            VK_BROWSER_BACK = 0xA6,
            VK_BROWSER_FORWARD = 0xA7,
            VK_BROWSER_REFRESH = 0xA8,
            VK_BROWSER_STOP = 0xA9,
            VK_BROWSER_SEARCH = 0xAA,
            VK_BROWSER_FAVORITES = 0xAB,
            VK_BROWSER_HOME = 0xAC,
            //
            VK_VOLUME_MUTE = 0xAD,
            VK_VOLUME_DOWN = 0xAE,
            VK_VOLUME_UP = 0xAF,
            VK_MEDIA_NEXT_TRACK = 0xB0,
            VK_MEDIA_PREV_TRACK = 0xB1,
            VK_MEDIA_STOP = 0xB2,
            VK_MEDIA_PLAY_PAUSE = 0xB3,
            VK_LAUNCH_MAIL = 0xB4,
            VK_LAUNCH_MEDIA_SELECT = 0xB5,
            VK_LAUNCH_APP1 = 0xB6,
            VK_LAUNCH_APP2 = 0xB7,
            //
            VK_OEM_1 = 0xBA,   // ';:' for US
            VK_OEM_PLUS = 0xBB,   // '+' any country
            VK_OEM_COMMA = 0xBC,   // ',' any country
            VK_OEM_MINUS = 0xBD,   // '-' any country
            VK_OEM_PERIOD = 0xBE,   // '.' any country
            VK_OEM_2 = 0xBF,   // '/?' for US
            VK_OEM_3 = 0xC0,   // '`~' for US
            //
            VK_OEM_4 = 0xDB,  //  '[{' for US
            VK_OEM_5 = 0xDC,  //  '\|' for US
            VK_OEM_6 = 0xDD,  //  ']}' for US
            VK_OEM_7 = 0xDE,  //  ''"' for US
            VK_OEM_8 = 0xDF,
            //
            VK_OEM_AX = 0xE1,  //  'AX' key on Japanese AX kbd
            VK_OEM_102 = 0xE2,  //  "<>" or "\|" on RT 102-key kbd.
            VK_ICO_HELP = 0xE3,  //  Help key on ICO
            VK_ICO_00 = 0xE4,  //  00 key on ICO
            //
            VK_PROCESSKEY = 0xE5,
            //
            VK_ICO_CLEAR = 0xE6,
            //
            VK_PACKET = 0xE7,
            //
            VK_OEM_RESET = 0xE9,
            VK_OEM_JUMP = 0xEA,
            VK_OEM_PA1 = 0xEB,
            VK_OEM_PA2 = 0xEC,
            VK_OEM_PA3 = 0xED,
            VK_OEM_WSCTRL = 0xEE,
            VK_OEM_CUSEL = 0xEF,
            VK_OEM_ATTN = 0xF0,
            VK_OEM_FINISH = 0xF1,
            VK_OEM_COPY = 0xF2,
            VK_OEM_AUTO = 0xF3,
            VK_OEM_ENLW = 0xF4,
            VK_OEM_BACKTAB = 0xF5,
            //
            VK_ATTN = 0xF6,
            VK_CRSEL = 0xF7,
            VK_EXSEL = 0xF8,
            VK_EREOF = 0xF9,
            VK_PLAY = 0xFA,
            VK_ZOOM = 0xFB,
            VK_NONAME = 0xFC,
            VK_PA1 = 0xFD,
            VK_OEM_CLEAR = 0xFE
        }

        [StructLayout(LayoutKind.Explicit)]
        public struct INPUT
        {
            [FieldOffset(0)]
            public int type;
            [FieldOffset(4)]
            public KEYBDINPUT ki;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct KEYBDINPUT
        {
            public ushort wVk;
            public ushort wScan;
            public uint dwFlags;
            public uint time;
            public IntPtr dwExtraInfo;
        }

        public enum WindowFlags
        {
            SWP_NOSIZE = 0x0001,
            SWP_NOMOVE = 0x0002,
            SWP_NOZORDER = 0x0004,
            SWP_NOREDRAW = 0x0008,
            SWP_NOACTIVATE = 0x0010,
            SWP_FRAMECHANGED = 0x0020,  /* The frame changed: send WM_NCCALCSIZE */
            SWP_SHOWWINDOW = 0x0040,
            SWP_HIDEWINDOW = 0x0080,
            SWP_NOCOPYBITS = 0x0100,
            SWP_NOOWNERZORDER = 0x0200,  /* Don't do owner Z ordering */
            SWP_NOSENDCHANGING = 0x0400,  /* Don't send WM_WINDOWPOSCHANGING */

            HWND_TOPMOST = -1,
            HWND_NOTOPMOST = -2,
            HWND_TOP = 0,
            HWND_BOTTOM = 1
        };

        public enum WordIndex
        {
            GWW_HINSTANCE = -6,
            GWW_ID = -12,
            GWL_STYLE = -16
        };

        public enum GetWindow_Cmd : uint
        {
            GW_HWNDFIRST = 0,
            GW_HWNDLAST = 1,
            GW_HWNDNEXT = 2,
            GW_HWNDPREV = 3,
            GW_OWNER = 4,
            GW_CHILD = 5,
            GW_ENABLEDPOPUP = 6
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int x;
            public int y;
        }
        public enum GWL : int
        {
            ExStyle = -20,
        }

        public enum WS_EX : int
        {
            Transparent = 32,
            Layered = 524288,
        }

        public enum LWA : int
        {
            ColorKey = 1,
            Alpha = 2,
        }

        #endregion

        #region WIN32 API FUNCTIONS

        #region Accessible
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr FindWindowEx(
            int parent /*HWND*/,
            int next /*HWND*/,
            string lpszClass,
            int sWindowTitle  /*HWND*/);

        public enum NativeMsg : int
        {
            CHILDID_SELF = 0,
            CHILDID_1 = 1,
            OBJID_CLIENT = -4, //0xFFFFFFC
        }
        #endregion

        [DllImport("user32.dll", SetLastError = false)]
        public static extern IntPtr GetDesktopWindow();

        [DllImport("kernel32.dll", EntryPoint = "LoadLibraryA")]
        public static extern IntPtr LoadLibrary(string path);

        [DllImport("kernel32.dll")]
        public static extern IntPtr GetProcAddress(IntPtr library, string procName);

        [DllImport("kernel32.dll")]
        public static extern bool FreeLibrary(IntPtr library);

        [DllImport("uxtheme.dll")]
        public static extern bool IsThemeActive();

        [DllImport("uxtheme.dll")]
        public static extern bool IsAppThemed();

        [DllImport("user32", EntryPoint = "GetWindowLong")]
        public static extern int GetWindowLong(IntPtr hWnd, GWL nIndex);

        [DllImport("user32", EntryPoint = "SetWindowLong")]
        public static extern int SetWindowLong(IntPtr hWnd, GWL nIndex, WS_EX dsNewLong);

        [DllImport("user32.dll", EntryPoint = "SetLayeredWindowAttributes")]
        public static extern bool SetLayeredWindowAttributes(IntPtr hWnd, int crKey, byte alpha, LWA dwFlags);

        // Registers a hot key with Windows.
        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);

        // Unregisters the hot key with Windows.
        [DllImport("user32.dll")]
        public static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        [DllImport("user32.dll")]
        public static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth,
           int nHeight, bool bRepaint);

        [DllImport("user32.dll")]
        public static extern IntPtr GetActiveWindow();

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetWindow(IntPtr hWnd, uint uCmd);

        [DllImport("user32.dll")]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X,
           int Y, int cx, int cy, uint uFlags);

        [DllImport("User32.dll")]
        public static extern short GetAsyncKeyState(System.Windows.Forms.Keys vKey); // Keys enumeration
        [DllImport("User32.dll")]
        public static extern short GetAsyncKeyState(System.Int32 vKey);
        [DllImport("User32.dll")]
        public static extern int GetForegroundWindow();

        [DllImport("user32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumChildWindows(IntPtr window, EnumWindowProc callback, IntPtr i);

        /// <summary>
        /// Returns a list of child windows
        /// </summary>
        /// <param name="parent">Parent of the windows to return</param>
        /// <returns>List of child windows</returns>
        public static List<IntPtr> GetChildWindows(IntPtr parent)
        {
            List<IntPtr> result = new List<IntPtr>();
            GCHandle listHandle = GCHandle.Alloc(result);
            try
            {
                EnumWindowProc childProc = new EnumWindowProc(EnumWindow);
                EnumChildWindows(parent, childProc, GCHandle.ToIntPtr(listHandle));
            }
            finally
            {
                if (listHandle.IsAllocated)
                    listHandle.Free();
            }
            return result;
        }

        /// <summary>
        /// Callback method to be used when enumerating windows.
        /// </summary>
        /// <param name="handle">Handle of the next window</param>
        /// <param name="pointer">Pointer to a GCHandle that holds a reference to the list to fill</param>
        /// <returns>True to continue the enumeration, false to bail</returns>
        private static bool EnumWindow(IntPtr handle, IntPtr pointer)
        {
            GCHandle gch = GCHandle.FromIntPtr(pointer);
            List<IntPtr> list = gch.Target as List<IntPtr>;
            if (list == null)
            {
                throw new InvalidCastException("GCHandle Target could not be cast as List<IntPtr>");
            }
            list.Add(handle);
            //  You can modify this to check to see if you want to cancel the operation, then return a null here
            return true;
        }

        /// <summary>
        /// Delegate for the EnumChildWindows method
        /// </summary>
        /// <param name="hWnd">Window handle</param>
        /// <param name="parameter">Caller-defined variable; we use it for a pointer to our list</param>
        /// <returns>True to continue enumerating, false to bail.</returns>
        public delegate bool EnumWindowProc(IntPtr hWnd, IntPtr parameter);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern System.UInt32 GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        public static extern IntPtr GetParent(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport("user32")]
        public static extern bool GetClientRect(IntPtr hWnd, out RECT lpRect);

        [DllImport("user32")]
        public static extern bool GetCursorPos(out POINT lpPoint);

        [DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);

        [DllImport("gdi32.dll")]
        public static extern IntPtr CreateHatchBrush(int fnStyle, uint clrref);

        /// <summary>
        /// SetCursorPos sets the position of the mouse cursor
        /// </summary>
        /// <param name="x">The x coordinate to move the cursor to</param>
        /// <param name="y">The y coordinate to move the cursor to</param>
        /// <returns></returns>
        [DllImport("user32")]
        public static extern int SetCursorPos(int x, int y);

        [DllImport("user32")]
        static extern bool RedrawWindow(IntPtr hWnd, IntPtr lprcUpdate, IntPtr hrgnUpdate, uint flags);

        [DllImport("user32")]
        public static extern IntPtr GetDC(IntPtr hwnd);

        [DllImport("user32")]
        public static extern int ReleaseDC(IntPtr hWnd, IntPtr hdc);

        [DllImport("gdi32.dll")]
        public static extern uint GetPixel(IntPtr hdc, int nXPos, int nYPos);

        /// <summary>
        /// The WindowFromPoint function retrieves a handle to the window that contains the specified point.
        /// </summary>
        /// <param name="Point">Specifies a POINT structure that defines the point to be checked.</param>
        /// <returns>The return value is a handle to the window that contains the point. 
        /// If no window exists at the given point, the return value is NULL. If the point is over a static text control, 
        /// the return value is a handle to the window under the static text control.</returns>
        [DllImport("user32")]
        public static extern IntPtr WindowFromPoint(POINT Point);

        [DllImport("user32")]
        public static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

        /// <summary>
        /// The GetWindowRect function retrieves the dimensions of the bounding rectangle of the specified window. 
        /// The dimensions are given in screen coordinates that are relative to the upper-left corner of the screen.
        /// </summary>
        /// <param name="hWnd">Handle to the window.</param>
        /// <param name="lpRect">Pointer to a structure that receives the screen coordinates of the upper-left and lower-right corners of the window.</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetWindowRect(int hWnd, out RECT lpRect);

        /// <summary>
        /// The GetDlgItem function retrieves a handle to a control in the specified dialog box
        /// </summary>
        /// <param name="hDlg">Handle to the dialog box that contains the control.</param>
        /// <param name="nIDDlgItem">Specifies the identifier of the control to be retrieved.</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern IntPtr GetDlgItem(IntPtr hDlg, int nIDDlgItem);

        /// <summary>
        /// he keybd_event function synthesizes a keystroke
        /// </summary>
        /// <param name="bVk">Specifies a virtual-key code. The code must be a value in the range 1 to 254</param>
        /// <param name="bScan">Specifies a hardware scan code for the key.</param>
        /// <param name="dwFlags">Specifies various aspects of function operation</param>
        /// <param name="dwExtraInfo">Specifies an additional value associated with the key stroke</param>
        [DllImport("user32.dll")]
        public static extern void keybd_event(int bVk, int bScan, int dwFlags, IntPtr dwExtraInfo);
        [DllImport("user32.dll")]
        public static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, UIntPtr dwExtraInfo);

        [DllImport("user32.dll")]
        public static extern short GetKeyState(VirtualKeyStates nVirtKey);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint SendInput(uint nInputs, ref INPUT pInputs, int cbSize);
        public const int INPUT_KEYBOARD = 1;

        [DllImport("user32.dll")]
        public static extern IntPtr GetMessageExtraInfo();

        [DllImport("user32.dll")]
        static extern short VkKeyScan(char ch);

        /// <summary>
        /// GetClassName retrieves the name of the window class to which a window belongs. 
        /// The name of the class is placed into the string passed as lpClassName.
        /// </summary>
        /// <param name="hWnd">handle to the required window</param>
        /// <param name="lpClassName">Text of the class name</param>
        /// <param name="size">size of the text buffer</param>
        /// <returns></returns>
        [DllImport("user32.dll", SetLastError = true)]
        public static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int size);

        /// <summary>
        /// The SetForegroundWindow function puts the thread that created the 
        /// specified window into the foreground and activates the 
        /// window.Keyboard input is directed to the window, and various 
        /// visual cues are changed for the user.
        /// </summary>
        /// <param name="hWnd">handle to the required window</param>
        /// <returns></returns>
        [DllImport("User32.dll")]
        public static extern Int32 SetForegroundWindow(int hWnd);

        /// <summary>
        /// The GetWindowText function copies the text of the specified 
        /// window's title bar (if it has one) into a buffer. 
        /// If the specified window is a control, the text of the control is copied.
        /// </summary>
        /// <param name="hwnd">handle to the required window</param>
        /// <param name="text">Text of the window title</param>
        /// <param name="size">size of the text buffer</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern int GetWindowText(IntPtr hwnd, StringBuilder text, int size);

        /// <summary>
        /// The FindWindow API
        /// </summary>
        /// <param name="lpClassName">the class name for the window to search for</param>
        /// <param name="lpWindowName">the name of the window to search for</param>
        /// <returns></returns>
        [DllImport("User32.dll")]
        public static extern Int32 FindWindow(String lpClassName, String lpWindowName);

        /// <summary>
        /// The SendMessage API
        /// </summary>
        /// <param name="hWnd">handle to the required window</param>
        /// <param name="msg">the system/Custom message to send</param>
        /// <param name="wParam">first message parameter</param>
        /// <param name="lParam">second message parameter</param>
        /// <returns></returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendNotifyMessage(int hWnd, int msg, int wParam, IntPtr lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(int hWnd, int msg, int wParam, int lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(int hWnd, int msg, int wParam, IntPtr lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        public static extern IntPtr SendNotifyMessage(int hWnd, int msg, int wParam, StringBuilder lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        public static extern IntPtr SendMessage(int hWnd, int msg, int wParam, StringBuilder lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        public static extern IntPtr SendMessage(int hWnd, int msg, int wParam, String lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        public static extern IntPtr SendMessage(int hWnd, int msg, int wParam, ref RECT lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        public static extern IntPtr SendMessage(int hWnd, int msg, int wParam, ref POINT lParam);

        [DllImport("user32.dll")]
        public static extern int FrameRect(IntPtr hdc, [In] ref RECT lprc, IntPtr hbr);

        /// <summary>
        /// The FindWindowEx API
        /// </summary>
        /// <param name="parentHandle">a handle to the parent window </param>
        /// <param name="childAfter">a handle to the child window to start search after</param>
        /// <param name="className">the class name for the window to search for</param>
        /// <param name="windowTitle">the name of the window to search for</param>
        /// <returns></returns>
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, string windowTitle);

        [DllImport("gdi32.dll", EntryPoint = "CreateSolidBrush", SetLastError = true)]
        public static extern IntPtr CreateSolidBrush(int crColor);

        #region Menu API

        [DllImport("user32.dll")]
        public static extern IntPtr GetMenu(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern int GetMenuString(IntPtr hMenu, uint uIDItem,
           [Out, MarshalAs(UnmanagedType.LPStr)] StringBuilder lpString, int nMaxCount, uint uFlag);

        [DllImport("user32.dll")]
        public static extern int GetMenuItemID(IntPtr hMenu, int nPos);

        [DllImport("user32.dll")]
        public static extern bool EnableMenuItem(IntPtr hMenu, uint uIDEnableItem,
           uint uEnable);

        [DllImport("user32.dll")]
        public static extern uint GetMenuState(IntPtr hMenu, uint uId, uint uFlags);

        [DllImport("user32.dll")]
        public static extern IntPtr GetSubMenu(IntPtr hMenu, int nPos);

        [DllImport("user32.dll")]
        public static extern bool GetMenuInfo(IntPtr hmenu, out MENUINFO lpcmi);

        [DllImport("user32.dll")]
        public static extern bool GetMenuItemInfo(IntPtr hMenu, uint uItem, bool fByPosition, ref MENUITEMINFO lpmii);

        [DllImport("user32.dll")]
        public static extern int GetMenuItemCount(IntPtr hMenu);


        #endregion

        #endregion

        #region WRAPPER CLASSES
       
        /// <summary>
        /// Roght mouse click
        /// </summary>
        public static void MouseRightClick()
        {
            //Mouse Right Down and Mouse Right Up
            mouse_event((int)MouseEventFlags.RIGHTDOWN, 0, 0, 0, 0);
            System.Threading.Thread.Sleep(100);
            mouse_event((int)MouseEventFlags.RIGHTUP, 0, 0, 0, 0);
        }

        /// <summary>
        /// Left mouse click
        /// </summary>
        public static void MouseLeftClick()
        {
            //Mouse Right Down and Mouse Right Up
            mouse_event((int)MouseEventFlags.LEFTDOWN, 0, 0, 0, 0);
            System.Threading.Thread.Sleep(100);
            mouse_event((int)MouseEventFlags.LEFTUP, 0, 0, 0, 0);
        }

        public static void SetFormToTransparent(IntPtr Handler)
        {
            //  make form transparent to the mouse.  
            int m_InitialStyle = GetWindowLong(Handler, GWL.ExStyle);
            SetWindowLong(Handler, GWL.ExStyle, (WS_EX.Layered | WS_EX.Transparent));
            //  for visual effect!
            SetLayeredWindowAttributes(Handler, 0, 200, LWA.Alpha);
        }
        public static void OutlineWindow(IntPtr hwnd)
        {
            IntPtr hHBr;
            RECT r = new RECT();
            IntPtr hdc;

            if (hwnd == IntPtr.Zero)
                return;

            GetClientRect(hwnd, out r);
            hHBr = CreateSolidBrush(Color.Red.R);

            hdc = GetDC(hwnd);
            FrameRect(hdc, ref r, hHBr);

            ReleaseDC(hwnd, hdc);
            DeleteObject(hHBr);

        }
        public static void RemoveRectOutline(IntPtr hwnd)
        {
            RedrawWindow(hwnd, IntPtr.Zero, IntPtr.Zero, Convert.ToUInt32(RedrawWindowflags.RDW_INVALIDATE));
        }
        public static string GetWinText(IntPtr hwnd)
        {
            StringBuilder text = new StringBuilder(256);
            GetWindowText(hwnd, text, text.Capacity);
            return text.ToString().Trim();
        }
        public static string GetWinClassName(IntPtr hwnd)
        {
            StringBuilder text = new StringBuilder(256);
            GetClassName(hwnd, text, text.Capacity);
            return text.ToString().Trim();
        }
        public static void MoveMouseToWindow(IntPtr hwnd)
        {
            SetForegroundWindow((int)hwnd);
            RECT rect = new RECT();
            GetWindowRect((int)hwnd, out rect);
            SetCursorPos(rect.Left + 5, rect.Top + 5);
        }
        public static Color GetPixelColor(IntPtr hwnd)
        {
            POINT point;
            IntPtr hdc;
            uint pixel;

            GetCursorPos(out point);
            hdc = GetDC(IntPtr.Zero);
            pixel = GetPixel(hdc, point.x, point.y);
            Color color = Color.FromArgb((int)pixel);
            ReleaseDC(IntPtr.Zero, hdc);

            return color;
        }
        public static Color GetPixelColor(int x, int y)
        {
            IntPtr hdc;
            uint pixel;

            hdc = GetDC(IntPtr.Zero);
            pixel = GetPixel(hdc, x, y);
            Color color = Color.FromArgb((int)pixel);
            ReleaseDC(IntPtr.Zero, hdc);

            return color;
        }
        public static string GetControlId(IntPtr hwnd)
        {
            //get parent window
            IntPtr parentHwnd = GetParent(hwnd);
            if (parentHwnd != IntPtr.Zero)
            {
                //get Control Text
                UInt32 ctrlId = GetWindowLong(hwnd, Convert.ToInt32(WordIndex.GWW_ID));
                return ctrlId.ToString("X");
            }
            else
            {
                return "";
            }
        }
        public static string GetActiveWin()
        {
            int hwnd = GetForegroundWindow();
            return GetWinText(new IntPtr(hwnd));
        }
        public static void SetWindowPos(int hwnd, POINT pt)
        {
            SetWindowPos(new IntPtr(hwnd),
                 IntPtr.Zero,
                 pt.x,
                 pt.y,
                 0,
                 0,
                 Convert.ToUInt32(WindowFlags.SWP_NOSIZE));

            //MoveWindow(new IntPtr(hwnd), rect.Left, rect.Top, rect.Right, rect.Bottom, true);
        }
        public static RECT GetWinRect(int hwnd)
        {
            RECT r = new RECT();
            GetWindowRect(hwnd, out r);

            r.Right = r.Right - r.Left;
            r.Bottom = r.Bottom - r.Top;

            return r;
        }
        public static POINT GetMousePoint()
        {
            POINT pt = new POINT();
            GetCursorPos(out pt);
            return pt;
        }
        public static IntPtr GetWin(IntPtr hwnd, GetWindow_Cmd cmd)
        {
            IntPtr handle = GetWindow(hwnd, Convert.ToUInt32(cmd));
            return handle;
        }
        #endregion

        #region CUSTOM EVENT MESSAGING FUNTIONS
        public static void TabItemClick(IntPtr hwnd, Point pt)
        {

            TCHITTESTINFO testinfo = new TCHITTESTINFO();
            testinfo.pt = pt;
            testinfo.flags = Convert.ToInt32(Messages.TCHT_ONITEMLABEL);
            IntPtr p = new IntPtr();
            SendNotifyMessage((int)hwnd,
                              Convert.ToInt32(Messages.TCM_HITTEST),
                              0,
                              p);
        }
        #endregion
    }

    internal static class UnsafeNativeMethods
    {
        private const string User32 = "User32.dll";

        #region Constructors

        #endregion

        #region Destructor

        #endregion

        #region Fields

        #endregion

        #region Events

        #endregion

        #region Operators

        #endregion

        #region Properties

        #endregion

        #region Methods

        /// <summary>
        /// retrieves the status of the specified virtual key
        /// </summary>
        /// <param name="KeyCode">Specifies a virtual key</param>
        /// <returns>
        /// The return value specifies the status of the specified virtual key, as follows: 
        ///  If the high-order bit is 1, the key is down; otherwise, it is up.
        ///  If the low-order bit is 1, the key is toggled. A key, such as the CAPS LOCK key, 
        ///  is toggled if it is turned on. The key is off and untoggled if the low-order bit is 0.
        ///  A toggle key's indicator light (if any) on the keyboard will be on when the key is
        ///  toggled, and off when the key is untoggled.
        /// </returns>
        [DllImport(User32, CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern short GetKeyState(int KeyCode);

        /// <summary>
        ///  retrieves a handle to the top-level window whose class name and window name match the specified strings
        /// </summary>
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr FindWindow(string className, string windowName);

        /// <summary>
        /// determines which pop-up window owned by the specified window was most recently active
        /// </summary>
        [DllImport("user32.dll")]
        public static extern IntPtr GetLastActivePopup(IntPtr hWnd);

        /// <summary>
        /// puts the thread that created the specified window into the foreground and activates the window. 
        /// </summary>
        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);


        #endregion
    }
}
