using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Microsoft.Win32;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing.Text;
using System.Collections.Generic;

namespace SGI
{
    /// <summary>
    /// Represents a base light component that can be used to design
    /// custom control without the overhead of using a 
    /// System.Windows.Forms.Control class control.
    /// </summary>
    [Serializable]
    public class Widget : WidgetBase
    {
        #region Fields
        private DrawHelper _drawHelper;

        private Rectangle _imageRectangle;

        //Custom
        Shape3D _image3DStyle = Shape3D.Flat;
        System.Drawing.Drawing2D.LinearGradientMode _imageGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
        SmoothingMode _imageQuality = SmoothingMode.HighQuality;
        Shapes _imageShape = Shapes.Square;
        HatchStyle _imageHatchStyle = HatchStyle.DashedHorizontal;
        ArrowDirection _imageDirection = ArrowDirection.Top;
        DashStyle _imageLnStyle = DashStyle.Solid;

        bool _imageHatched = false;
        bool _transparent = false;
        bool _border = true;
        bool _objIsCirc = false;

        int _borderWidth = 1;

        //Colors
        Color _backColor = Color.White;
        Color _borderColor = Color.Black;
        Color _colAlternate = Color.LightGray;
        private List<Color> _colGradColors = new List<Color>();
        private List<float> _colGradPositions = new List<float>();
        double _ImageRadius = 0.1;

        //size
        //private Region _region;

        //text
        private string _text = string.Empty;
        private ContentAlignment _textAlign = ContentAlignment.MiddleCenter;
        private bool _useTextLocation = false;
        private Point _textLocation;

        //tag
        private object _tag = new object();

        private bool _ownerDrawText = true;
        private bool _showText = true;

        private Image _widgetImage;
        private Point _imageLocation = new Point(1, 1);


        [DllImport("kernel32.dll", SetLastError = true)]
        static extern int SetProcessWorkingSetSize(int hProcess, int dwMinimumWorkingSetSize, int dwMaximumWorkingSetSize);

        //font
        //private Font _font;
        private Color _foreColor = Color.Black;

        //Index
        private int _tabIndex;

        private string _name = string.Empty;

        private PictureBoxSizeMode _sizeMode = PictureBoxSizeMode.Normal;
        private bool _drawBackground = false;
        //private bool _overrideImageSize = false;
        private Size _imageSize = new Size();

        private bool _canDelete = true;
        #endregion

        #region Events

        /// <summary>
        /// Occurs when the control is clicked.
        /// </summary>
        public event EventHandler Click;
        /// <summary>
        /// Fires the doudle click event
        /// </summary>
        public event EventHandler DoubleClick;
        /// <summary>
        /// Fires the Mouse down event
        /// </summary>
        public event MouseEventHandler MouseDown;
        /// <summary>
        /// Fires the mouse up event
        /// </summary>
        public event MouseEventHandler MouseUp;
        /// <summary>
        /// Fires the mouse move event
        /// </summary>
        public event MouseEventHandler MouseMove;
        /// <summary>
        /// Fires the mouse enter event
        /// </summary>
        public event EventHandler MouseEnter;
        /// <summary>
        /// Fires the mouse leave event
        /// </summary>
        public event EventHandler MouseLeave;
        /// <summary>
        /// Fires when the widget is selected
        /// </summary>
        public event EventHandler OnSelected;
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets if the widget can be deleted from the canvas
        /// with the delete button on the keyboard.
        /// </summary>
        public bool CanDelete
        {
            get { return _canDelete; }
            set { _canDelete = value; }
        }

        public Size ImageSize
        {
            get { return _imageSize; }
            set { _imageSize = value; }
        }

        public bool DrawBackground
        {
            get { return _drawBackground; }
            set { _drawBackground = value; }
        }
        public PictureBoxSizeMode SizeMode
        {
            get { return _sizeMode; }
            set { _sizeMode = value; }
        }

        /// <summary>
        /// Gets or sets if the text is drawn on the screen
        /// </summary>
        public bool ShowText
        {
            get { return _showText; }
            set { _showText = value; }
        }
        /// <summary>
        /// Gets or sets if location of the text is explicitly set
        /// </summary>
        public bool UseTextLocation
        {
            get { return _useTextLocation; }
            set { _useTextLocation = value; }
        }

        /// <summary>
        /// Gets or sets the location of the text
        /// </summary>
        public Point TextLocation
        {
            get { return _textLocation; }
            set { _textLocation = value; }
        }
        /// <summary>
        /// Gets or sets the location of the widget image
        /// </summary>
        public Point ImageLocation
        {
            get { return _imageLocation; }
            set { _imageLocation = value; }
        }

        /// <summary>
        /// Gets or sets the image of the widget
        /// </summary>
        public Image Image
        {
            get { return _widgetImage; }
            set { _widgetImage = value; }
        }

        /// <summary>
        /// Gets or sets the name of the component
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets if the drawing of the text is done by the <see cref="Widget"/> or the inheriting object
        /// </summary>
        public bool OwnerDrawText
        {
            get { return _ownerDrawText; }
            set { _ownerDrawText = value; }
        }

        /// <summary>
        /// Gets or sets the tab index of the widget
        /// </summary>
        public int TabIndex
        {
            get { return _tabIndex; }
            set { _tabIndex = value; }
        }

        /// <summary>
        /// Gets or sets the tag object
        /// </summary>
        [Browsable(false)]
        public virtual object Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

        /// <summary>
        /// Gets or sets the text associated with this Widget.
        /// </summary>
        /// <value>
        /// The text associated with this Widget.
        /// </value>
        public string Text
        {
            get
            {
                return _text;
            }

            set
            {
                if (_text != value)
                {
                    _text = value;
                    Invalidate();
                }
            }
        }

        /// <summary>
        /// Gets or sets the alignment of text in the label.
        /// </summary>
        /// <value>
        /// One of the <see cref="ContentAlignment"/> values. The default is 
        /// <see cref="ContentAlignment.TopLeft"/>.
        /// </value>
        public ContentAlignment TextAlign
        {
            get
            {
                return _textAlign;
            }

            set
            {
                if (_textAlign != value)
                {
                    _textAlign = value;
                    Invalidate();
                }
            }
        }

        /// <summary>
        /// Gets or sets the fore color
        /// </summary>
        [Description("Gets or sets the text color."), Category("Appearence")]
        public virtual Color ForeColor
        {
            get { return _foreColor; }
            set
            {
                if (_foreColor != value)
                {
                    _foreColor = value;
                    Invalidate();
                }
            }
        }

        /// <summary>
        /// Gets or sets the fill color
        /// </summary>
        [Description("Gets or sets the fill Color."), Category("Appearence")]
        public Color BackColor
        {
            get { return _backColor; }
            set
            {
                if (_backColor != value)
                {
                    _backColor = value;
                    BuildBrush();
                    Invalidate();
                }
            }
        }

        /// <summary>
        /// Gets or sets the outline color
        /// </summary>
        [Description("Sets the the outline Color the thermometer is drawn in."), Category("Appearence")]
        public virtual Color BorderColor
        {
            get { return _borderColor; }
            set
            {
                if (_borderColor != value)
                {
                    _borderColor = value;
                    Invalidate();

                }
            }
        }

        /// <summary>
        /// Gets or sets the alternate color
        /// </summary>
        [Description("Sets the Alternate shading color."), Category("Appearence")]
        public Color AlternateColor
        {
            get { return _colAlternate; }
            set
            {
                if (_colAlternate != value)
                {
                    _colAlternate = value;
                    BuildBrush();
                    Invalidate();
                }
            }
        }

        /// <summary>
        ///Remember in the properties, you only want to re-build/redraw if the new value
        ///is different from the existing value. Saves unnecessary computing
        /// </summary>
        [Description("Sets the HatchStyle for the object when in Flat Shading mode."), Category("Drawing")]
        public HatchStyle HatchFillStyle
        {
            get { return _imageHatchStyle; }
            set
            {
                if (_imageHatchStyle != value)
                {
                    _imageHatchStyle = value;
                    if (_imageHatched == true & _image3DStyle == Shape3D.Flat)
                    {
                        BuildBrush();
                        Invalidate();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the object to be filled with Hatches when in Flat Shading mode.
        /// </summary>
        [Description("Sets the object to be filled with Hatches when in Flat Shading mode."), Category("Drawing")]
        public bool HatchFill
        {
            get { return _imageHatched; }
            set
            {
                if (_imageHatched != value)
                {
                    _imageHatched = value;
                    BuildBrush();
                    Invalidate();
                }
            }
        }

        /// <summary>
        ///Gets or sets the Shape drawn for the object.
        /// </summary>
        [Description("Sets the Shape drawn for the object."), Category("Drawing")]
        public Shapes Shape
        {
            get { return _imageShape; }
            set
            {
                if (_imageShape != value)
                {
                    _imageShape = value;
                    Invalidate();
                }
            }
        }

        /// <summary>
        ///Gets or sets the Gradient mode for the object.
        /// </summary>
        [Description("Sets the Gradient mode for the object."), Category("Drawing")]
        public System.Drawing.Drawing2D.LinearGradientMode ShapeGradientMode
        {
            get
            {
                return _imageGradientMode;
            }
            set
            {
                if (_imageGradientMode != value)
                {
                    _imageGradientMode = value;
                    BuildBrush();
                    Invalidate();
                }
            }
        }

        /// <summary>
        /// Gets or sets the level of Anti-Aliasing for the object.
        /// </summary>
        [Description("Sets the level of Anti-Aliasing for the object."), Category("Drawing")]
        public Quality ShapeQuality
        {
            get
            {
                switch (_imageQuality)
                {
                    case SmoothingMode.AntiAlias:
                        return Quality.AntiAlias;
                    case SmoothingMode.HighQuality:
                        return Quality.HighQuality;
                    case SmoothingMode.HighSpeed:
                        return Quality.HighSpeed;
                    case SmoothingMode.Default:
                        return Quality.DefaultQuality;
                    case SmoothingMode.None:
                        return Quality.None;
                    default:
                        return Quality.None;
                }
            }
            set
            {
                switch (value)
                {
                    case Quality.AntiAlias:
                        _imageQuality = SmoothingMode.AntiAlias;
                        break;
                    case Quality.HighQuality:
                        _imageQuality = SmoothingMode.HighQuality;
                        break;
                    case Quality.HighSpeed:
                        _imageQuality = SmoothingMode.HighSpeed;
                        break;
                    case Quality.DefaultQuality:
                        _imageQuality = SmoothingMode.Default;
                        break;
                    case Quality.None:
                        _imageQuality = SmoothingMode.None;
                        break;
                    default:
                        _imageQuality = SmoothingMode.None;
                        break;
                }
                Invalidate();
            }
        }

        /// <summary>
        /// Get or sets the Shading mode for the object.
        /// </summary>
        [Description("Sets the Shading mode for the object."), Category("Drawing")]
        public Shape3D ShapeShading
        {
            get
            {
                return _image3DStyle;
            }
            set
            {
                if (_image3DStyle != value)
                {
                    _image3DStyle = value;
                    BuildBrush();
                    Invalidate();
                }
            }
        }

        /// <summary>
        /// Get or sets the pen width for the object.
        /// </summary>
        [Description("Sets the pen width for the object."), Category("Drawing")]
        public int BorderWidth
        {
            get { return _borderWidth; }
            set
            {
                if (_borderWidth != value)
                {
                    _borderWidth = value;
                    Invalidate();
                }
            }
        }

        /// <summary>
        /// Gets or sets the Color array used in the 3D Shading Mode.
        /// </summary>
        [Browsable(false)]
        [Description("Sets the Color array used in the 3D Shading Mode."), Category("Drawing")]
        public List<Color> Gradients
        {
            get { return _colGradColors; }
        }

        /// <summary>
        /// Gets or sets the positions of each color in the gradient color array when in 3D shading mode.
        /// </summary>
        [Browsable(false)]
        [Description("Sets the positions of each color in the gradient color array when in 3D shading mode."), Category("Drawing")]
        public List<float> GradientPositions
        {
            get { return _colGradPositions; }
        }

        /// <summary>
        /// Gets or sets the radius of the rounded corners when applicable.
        /// </summary>
        [Description("Sets the radius of the rounded corners when applicable."), Category("Drawing")]
        public double CornerRadius
        {
            get { return _ImageRadius; }
            set
            {
                if (value >= 0.1 & value < 1)
                {
                    if (_ImageRadius != value)
                    {
                        _ImageRadius = value;
                        if (_imageShape == Shapes.RoundedRectangle ||
                            _imageShape == Shapes.RoundedSquare)
                        {
                            Invalidate();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the Direction of the object when applicable
        /// </summary>
        [Description("Sets the Direction of the object when applicable"), Category("Drawing")]
        public ArrowDirection Direction
        {
            get { return _imageDirection; }
            set
            {
                if (_imageDirection != value)
                {
                    _imageDirection = value;
                    Invalidate();
                }
            }
        }

        /// <summary>
        /// Gets or sets the background opacity.
        /// </summary>
        [Description("Sets the background opacity."), Category("Drawing")]
        public bool Transparent
        {
            get { return _transparent; }
            set
            {
                if (_transparent != value)
                {
                    _transparent = value;
                    Invalidate();
                }
            }
        }

        /// <summary>
        /// Gets or sets the style of line to draw the object.
        /// </summary>
        [Description("Sets/Gets the style of line to draw the object."), Category("Drawing")]
        public DashStyle LineStyle
        {
            get { return _imageLnStyle; }
            set
            {
                if (_imageLnStyle != value)
                {
                    _imageLnStyle = value;
                    Invalidate();
                }
            }
        }

        /// <summary>
        /// Gets or sets whether or not to draw an outline around the object.
        /// </summary>
        [Description("Sets/Gets whether or not to draw an outline around the object."), Category("Drawing")]
        public bool Border
        {
            get { return _border; }
            set
            {
                if (_border != value)
                {
                    _border = value;
                    Invalidate();
                }
            }
        }

        /// <summary>
        /// Gets or sets the client area of the Widget
        /// </summary>
        [Browsable(false)]
        public virtual Rectangle ClientRectangle
        {
            get
            {
                return new Rectangle(this.Location.X,
                this.Location.Y, this.Size.Width,
                this.Size.Height);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public virtual Rectangle ClientImageRectangle
        {
            get
            {
                if (_sizeMode == PictureBoxSizeMode.CenterImage)
                {
                    Rectangle rectangle = this.ClientRectangle;
                    rectangle.X = (rectangle.Width - this._imageSize.Width) / 2;
                    rectangle.Size = _imageSize;
                    rectangle.Y = 10;

                    return rectangle;
                }
                else
                    return new Rectangle();
            }
        }

        #endregion

        #region Build Brushes/Pen

        /// <summary>
        /// Builds the Pen object used to draw the shape
        /// </summary>
        /// <returns></returns>
        public virtual Pen GetPen()
        {
            return GetPen(_borderColor, _borderWidth);
        }
        public virtual Pen GetPen(Color c, int width)
        {
            //Here you set the pen linestyle,color and thickness
            Pen t_Pen = new Pen(c, width);
            t_Pen.DashStyle = _imageLnStyle;
            return t_Pen;
        }

        //Builds a 2 Color LinearGradientBrush
        private LinearGradientBrush BuildBrushes()
        {
            //Build the Brush and set it's Paint Area, The Alternate Color
            //The Primary Color, and the Gradient Mode
            return new LinearGradientBrush(this.ClientRectangle, _colAlternate, _backColor, _imageGradientMode);
        }

        //Builds a 1 Color LinearGradientBrush
        private System.Drawing.Drawing2D.LinearGradientBrush BuildFlatBrush()
        {
            //Build the Brush and set it's Paint Area, The Alternate Color=Primary Color
            //The Primary Color, and the Gradient Mode is defaulted, because there is not 
            //Visible gradient
            return new System.Drawing.Drawing2D.LinearGradientBrush(this.ClientRectangle,
                _backColor, _backColor, System.Drawing.Drawing2D.LinearGradientMode.Vertical);
        }


        /// <summary>
        /// Adds gradient colors
        /// </summary>
        /// <param name="position"></param>
        /// <param name="color"></param>
        public void AddGradientColor(float position, Color color)
        {
            if (position < 0.0)
                throw new OverflowException();

            if (position > 1.0)
                throw new OverflowException();

            //add colors    
            _colGradPositions.Add(position);
            _colGradColors.Add(color);

            BuildBrush();
            Invalidate();
        }

        //Builds a User-Defined LinearGradientBrush
        private LinearGradientBrush Build3DBrush()
        {
            //Make sure there are colors in the Color Array
            if (_colGradColors == null || _colGradColors.Count <= 0)
            {
                throw new OverflowException("No Colors Assigned to color Array!");
            }

            //Make sure there are positions to set the colors in the color array properly
            //into the brush gradient
            if (_colGradPositions == null || _colGradPositions.Count <= 0)
            {
                throw new OverflowException("No Color positions assigned to positions array!");
            }

            //Make sure that the color array has the same number of elements as 
            //the Color position array or it will bomb. 
            if (_colGradPositions.Count != _colGradPositions.Count)
            {
                throw new OverflowException("Different amount of color from positions!");
            }


            //Create an new standard LinearGradientBrush as you would any other
            LinearGradientBrush lBrush = new LinearGradientBrush(new Rectangle(0, 0, this.Width, this.Height),
                _colAlternate, _backColor, _imageGradientMode);

            try
            {
                //Create a Color Blend object. The ColorBlend brings together the Colors Selected
                //And the positions the user wants them put at.
                ColorBlend cBlend = new ColorBlend((int)(_colGradColors.Count - 1));

                //Set the user-defined Colors
                cBlend.Colors = _colGradColors.ToArray();

                //Set the user-defined Positions for the Colors
                cBlend.Positions = _colGradPositions.ToArray();

                //Set the LinearGradientBrush's Interpolation Colors. Layman- Tell the brush
                //to paint each color to a certain percent of the fillarea and then begin the next
                //color accordingly
                lBrush.InterpolationColors = cBlend;

                //Return your newly created 3D brush;
                return lBrush;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                string Message = "There was an error, Check your Gradient Positions.\r\n";
                Message += "Make Sure that the First Position is 0.0 And the Last Position is 1.0. \r\n";
                Message += "Make Sure that Number of Colors matches the number of positions.";
                throw new OverflowException(Message);
            }

        }

        //Builds a HatchBrush
        private HatchBrush BuildHatchBrush()
        {
            //Very Simple Brush
            //Here you set the brush HatchStyle,AlternateColr and PrimaryColor
            return new HatchBrush(_imageHatchStyle, _colAlternate, _backColor);
        }

        //Builds a Brush that is defiend for a GraphicsPath-created earlier
        private PathGradientBrush BuildPathBrush(GraphicsPath pth)
        {
            //First Create a generic Path Brush, using the graphicsPath(shape)
            //Passed in.
            PathGradientBrush brsh = new PathGradientBrush(pth);
            //Set the CenterColor for the brush, since this brush is being used for circular
            //shapes
            brsh.CenterColor = _colAlternate;
            //Set the Surround Color tot he promary color for the brush, since this brush 
            //is being used for circular shapes
            brsh.SurroundColors = new Color[] { _backColor };
            return brsh;
        }
        #endregion

        #region Build Objects
        private GraphicsPath BuildRect()
        {
            //Create a new Graphics Path
            GraphicsPath grfxPath = new GraphicsPath();
            //Create a new Rectangle for Calculations
            Rectangle grfxRect = new Rectangle();
            //Set the New Rectangle bound = to the control bounds
            grfxRect = new Rectangle(0, 0, (int)(this.Width - 1), (int)(this.Height - 1));
            try
            {
                //Start the Figure
                grfxPath.StartFigure();
                //Add the shape to the Path
                grfxPath.AddRectangle(grfxRect);
                //Close the Path, this ensures that all line will form 
                //one Continuous shape outline
                grfxPath.CloseFigure();
                //Return the Path to be drawn and filled
                return grfxPath;
            }
            catch (IOException ioe)
            {
                string msg = ioe.Message;
                msg = null;
                return null;
            }
        }
        private GraphicsPath BuildSquare()
        {
            //Create a new Graphics Path
            GraphicsPath grfxPath = new GraphicsPath();
            //Create a new Rectangle for Calculations
            Rectangle grfxRect = new Rectangle();
            int m_Diff = 0;
            if (this.Height > this.Width)
            {
                m_Diff = (int)((this.Height - this.Width) * .5);
                grfxRect = new Rectangle(0, (int)(m_Diff - 1), (int)(this.Width - 1), (int)(this.Height - (m_Diff * 2)));
            }
            else
            {
                m_Diff = (int)((this.Width - this.Height) * .5);
                grfxRect = new Rectangle((int)(m_Diff - 1), 0, (int)(this.Width - (m_Diff * 2)), (int)(this.Height - 1));
            }

            try
            {
                grfxPath.StartFigure();
                grfxPath.AddRectangle(grfxRect);
                grfxPath.CloseFigure();
                return grfxPath;
            }
            catch (IOException ioe)
            {
                string msg = ioe.Message;
                msg = null;
                return null;
            }
        }
        private GraphicsPath BuildOval()
        {
            //Create a new Graphics Path
            GraphicsPath grfxPath = new GraphicsPath();
            //Create a new Rectangle for Calculations
            Rectangle grfxRect = new Rectangle();
            //Set the New Rectangle bound = to the control bounds
            grfxRect = new Rectangle(0, 0, (int)(this.Width - 1), (int)(this.Height - 1));
            try
            {
                grfxPath.StartFigure();
                grfxPath.AddEllipse(grfxRect);
                grfxPath.CloseFigure();
                return grfxPath;
            }
            catch (IOException ioe)
            {
                string msg = ioe.Message;
                msg = null;
                return null;
            }
        }
        private GraphicsPath BuildCircle()
        {
            //Create a new Graphics Path
            GraphicsPath grfxPath = new GraphicsPath();
            //Create a new Rectangle for Calculations
            Rectangle grfxRect = new Rectangle();
            int m_Diff = 0;
            if (this.Height > this.Width)
            {
                m_Diff = (int)((this.Height - this.Width) * .5);
                grfxRect = new Rectangle(0, (int)(m_Diff - 1), (int)(this.Width - 1), (int)(this.Height - (m_Diff * 2)));
            }
            else
            {
                m_Diff = (int)((this.Width - this.Height) * .5);
                grfxRect = new Rectangle((int)(m_Diff - 1), 0, (int)(this.Width - (m_Diff * 2)), (int)(this.Height - 1));
            }

            try
            {
                grfxPath.StartFigure();
                grfxPath.AddEllipse(grfxRect);
                grfxPath.CloseFigure();
                return grfxPath;
            }
            catch (IOException ioe)
            {
                string msg = ioe.Message;
                msg = null;
                return null;
            }
        }
        private GraphicsPath BuildRndRect()
        {
            //Create a new Graphics Path
            GraphicsPath grfxPath = new GraphicsPath();
            //Create a new Rectangle for Calculations
            Rectangle grfxRect = new Rectangle();
            //Set the New Rectangle bound = to the control bounds
            grfxRect = new Rectangle(0, 0, (int)(this.Width - 1), (int)(this.Height - 1));
            //The Radius of the rounded edges
            float radius = (int)(grfxRect.Height * _ImageRadius);
            //Width of the rectangle
            float width = (int)(grfxRect.Width);
            //Height of the rectangle
            float height = (int)(grfxRect.Height);
            //Following two lines are simply the Location of the rectangle
            float x = grfxRect.Left;
            float y = grfxRect.Top;
            //Make sure the radius is a valid value
            if (radius < 1) { radius = 1; }
            try
            {
                grfxPath.StartFigure();
                grfxPath.AddLine(x + radius, y, x + width - (radius * 2), y);
                grfxPath.AddArc(x + width - (radius * 2), y, radius * 2, radius * 2, 270, 90);
                grfxPath.AddLine(x + width, y + radius, x + width, y + height - (radius * 2));
                grfxPath.AddArc(x + width - (radius * 2), y + height - (radius * 2), radius * 2, radius * 2, 0, 90);
                grfxPath.AddLine(x + width - (radius * 2), y + height, x + radius, y + height);
                grfxPath.AddArc(x, y + height - (radius * 2), radius * 2, radius * 2, 90, 90);
                grfxPath.AddLine(x, y + height - (radius * 2), x, y + radius);
                grfxPath.AddArc(x, y, radius * 2, radius * 2, 180, 90);
                grfxPath.CloseFigure();
                return grfxPath;
            }
            catch (IOException ioe)
            {
                string msg = ioe.Message;
                msg = null;
                return null;
            }
        }
        private GraphicsPath BuildRndSquare()
        {
            //Create a new Graphics Path
            GraphicsPath grfxPath = new GraphicsPath();
            //Create a new Rectangle for Calculations
            Rectangle grfxRect = new Rectangle();
            int m_Diff = 0;
            if (this.Height > this.Width)
            {
                m_Diff = (int)((this.Height - this.Width) * .5);
                grfxRect = new Rectangle(0, (int)(m_Diff - 1), (int)(this.Width - 1), (int)(this.Height - (m_Diff * 2)));
            }
            else
            {
                m_Diff = (int)((this.Width - this.Height) * .5);
                grfxRect = new Rectangle((int)(m_Diff - 1), 0, (int)(this.Width - (m_Diff * 2)), (int)(this.Height - 1));
            }
            float radius = (int)(grfxRect.Width * _ImageRadius);
            float width = (int)(grfxRect.Width);
            float height = (int)(grfxRect.Height);
            float X = grfxRect.Left;
            float Y = grfxRect.Top;

            try
            {
                grfxPath.StartFigure();
                grfxPath.AddLine(X + radius, Y, X + width - (radius * 2), Y);
                grfxPath.AddArc(X + width - (radius * 2), Y, radius * 2, radius * 2, 270, 90);
                grfxPath.AddLine(X + width, Y + radius, X + width, Y + height - (radius * 2));
                grfxPath.AddArc(X + width - (radius * 2), Y + height - (radius * 2), radius * 2, radius * 2, 0, 90);
                grfxPath.AddLine(X + width - (radius * 2), Y + height, X + radius, Y + height);
                grfxPath.AddArc(X, Y + height - (radius * 2), radius * 2, radius * 2, 90, 90);
                grfxPath.AddLine(X, Y + height - (radius * 2), X, Y + radius);
                grfxPath.AddArc(X, Y, radius * 2, radius * 2, 180, 90);

                grfxPath.CloseFigure();
                return grfxPath;
            }
            catch (IOException ioe)
            {
                string msg = ioe.Message;
                msg = null;
                return null;
            }
        }
        private GraphicsPath BuildTriangle()
        {
            GraphicsPath grfxPath = new GraphicsPath();
            Rectangle grfxRect = new Rectangle();
            int m_Diff = 0;
            if (this.Height > this.Width)
            {
                m_Diff = (int)((this.Height - this.Width) * .5);
                grfxRect = new Rectangle(0, (int)(m_Diff - 1), (int)(this.Width - 1), (int)(this.Height - (m_Diff * 2)));
            }
            else
            {
                m_Diff = (int)((this.Width - this.Height) * .5);
                grfxRect = new Rectangle((int)(m_Diff - 1), 0, (int)(this.Width - (m_Diff * 2)), (int)(this.Height - 1));
            }
            m_Diff = grfxRect.Left;
            try
            {
                grfxPath.StartFigure();
                switch (_imageDirection)
                {
                    default:
                    case ArrowDirection.Top:
                        grfxPath.AddLine((int)(this.Width * .5), 0, 0, (int)(this.Height - 1));
                        grfxPath.AddLine(0, (int)(this.Height - 1), this.Width, (int)(this.Height - 1));
                        grfxPath.AddLine(this.Width, (int)(this.Height - 1), (int)(this.Width * .5), 0);
                        break;
                    case ArrowDirection.Right:
                        grfxPath.AddLine(0, 0, 0, (int)(this.Height - 1));
                        grfxPath.AddLine(0, (int)(this.Height - 1), (int)(this.Width - 1), (int)(this.Height * .5));
                        grfxPath.AddLine((int)(this.Width - 1), (int)(this.Height * .5), 0, 0);
                        break;
                    case ArrowDirection.Bottom:
                        grfxPath.AddLine(0, 0, (int)(this.Width * .5), (int)(this.Height - 1));
                        grfxPath.AddLine((int)(this.Width * .5), (int)(this.Height - 1), this.Width, 0);
                        grfxPath.AddLine(this.Width, 0, 0, 0);
                        break;
                    case ArrowDirection.Left:
                        grfxPath.AddLine(0, (int)(this.Height * .5), (int)(this.Width - 1), (int)(this.Height - 1));
                        grfxPath.AddLine((int)(this.Width - 1), (int)(this.Height - 1), (int)(this.Width - 1), 0);
                        grfxPath.AddLine((int)(this.Width - 1), 0, 0, (int)(this.Height * .5));
                        break;
                }
                grfxPath.CloseFigure();
                return grfxPath;
            }
            catch (IOException ioe)
            {
                string msg = ioe.Message;
                msg = null;
                return null;
            }
        }
        private GraphicsPath BuildDice()
        {
            GraphicsPath grfxPath = new GraphicsPath();
            Rectangle grfxRect = new Rectangle();
            int m_Diff = 0;
            if (this.Height > this.Width)
            {
                m_Diff = (int)((this.Height - this.Width) * .5);
                grfxRect = new Rectangle(0, (int)(m_Diff - 1), (int)(this.Width - 1), (int)(this.Height - (m_Diff * 2)));
            }
            else
            {
                m_Diff = (int)((this.Width - this.Height) * .5);
                grfxRect = new Rectangle((int)(m_Diff - 1), 0, (int)(this.Width - (m_Diff * 2)), (int)(this.Height - 1));
            }
            m_Diff = grfxRect.Left;
            try
            {
                grfxPath.StartFigure();

                grfxPath.AddLine((int)(this.Width * .5), 0, 0, (int)(this.Height * .5));
                grfxPath.AddLine(0, (int)(this.Height * .5), (int)(this.Width * .5), (int)(this.Height - 1));
                grfxPath.AddLine((int)(this.Width * .5), (int)(this.Height - 1), this.Width, (int)(this.Height * .5));
                grfxPath.AddLine(this.Width, (int)(this.Height * .5), (int)(this.Width * .5), 0);

                grfxPath.CloseFigure();
                return grfxPath;
            }
            catch (IOException ioe)
            {
                string msg = ioe.Message;
                msg = null;
                return null;
            }
        }
        private GraphicsPath BuildArrow()
        {
            GraphicsPath grfxPath = new GraphicsPath();
            Rectangle grfxRect = new Rectangle();
            Rectangle lineRect;

            int m_Diff = 0;
            if (this.Height > this.Width)
            {
                m_Diff = (int)((this.Height - this.Width) * .5);
                grfxRect = new Rectangle(0, (int)(m_Diff - 1), (int)(this.Width - 1), (int)(this.Height - (m_Diff * 2)));
            }
            else
            {
                m_Diff = (int)((this.Width - this.Height) * .5);
                grfxRect = new Rectangle((int)(m_Diff - 1), 0, (int)(this.Width - (m_Diff * 2)), (int)(this.Height - 1));
            }
            m_Diff = grfxRect.Left;
            try
            {
                grfxPath.StartFigure();
                switch (_imageDirection)
                {
                    default:
                    case ArrowDirection.Top:
                        //draw line cap
                        grfxPath.AddLine((int)(this.Width * .5), 0, 0, 13);
                        grfxPath.AddLine(0, 13, (int)(this.Width), 13);
                        grfxPath.AddLine((int)(this.Width * .5), 0, this.Width, 13);

                        //Draw the line
                        lineRect = new Rectangle((int)(this.Width * .5) - (int)(5 * .5), 13, 5, (int)(this.Height - 1));

                        grfxPath.AddRectangle(lineRect);

                        break;
                    case ArrowDirection.Right:
                        //draw line cap
                        grfxPath.AddLine((int)(this.Width - 13), 0, (int)(this.Width), (int)(this.Height * .5));
                        grfxPath.AddLine((int)(this.Width), (int)(this.Height * .5), (int)(this.Width - 13), (int)(this.Height - 1));
                        grfxPath.AddLine((int)(this.Width - 13), 0, (int)(this.Height - 1), (int)(this.Width - 13));

                        //Draw the line
                        lineRect = new Rectangle(0, (int)(this.Height * .5) - (int)(5 * .5), (int)(this.Width - 13), 5);

                        grfxPath.AddRectangle(lineRect);

                        break;
                    case ArrowDirection.Bottom:
                        //draw line cap
                        grfxPath.AddLine(0, (int)(this.Height - 13), (int)(this.Width * .5), (int)(this.Height - 1));
                        grfxPath.AddLine((int)(this.Width * .5), (int)(this.Height - 1), this.Width, (int)(this.Height - 13));
                        grfxPath.AddLine(0, (int)(this.Height - 13), this.Width, (int)(this.Height - 13));

                        //Draw the line
                        lineRect = new Rectangle((int)(this.Width * .5) - (int)(5 * .5), 0, 5, (int)(this.Height - 13));

                        grfxPath.AddRectangle(lineRect);

                        break;
                    case ArrowDirection.Left:
                        //draw line cap
                        grfxPath.AddLine(0, (int)(this.Height * .5), 13, (int)(this.Height - 1));
                        grfxPath.AddLine(13, (int)(this.Height - 1), 13, 0);
                        grfxPath.AddLine(13, 0, 0, (int)(this.Height * .5));

                        //Draw the line
                        lineRect = new Rectangle(13, (int)(this.Height * .5) - (int)(5 * .5), (int)(this.Width), 5);

                        grfxPath.AddRectangle(lineRect);

                        break;
                }
                grfxPath.CloseFigure();
                return grfxPath;
            }
            catch (IOException ioe)
            {
                string msg = ioe.Message;
                msg = null;
                return null;
            }
        }

        #endregion

        #region Build and Draw Routines

        /// <summary>
        /// The BuildObject Routine determines the object selected as the shape and builds it accordingly
        /// </summary>
        /// <returns></returns>
        private GraphicsPath GetShapePath()
        {
            _objIsCirc = false;

            switch (_imageShape)
            {
                case Shapes.Square:
                    return this.BuildSquare();
                case Shapes.RoundedSquare:
                    return this.BuildRndSquare();
                case Shapes.Rectangle:
                    return this.BuildRect();
                case Shapes.RoundedRectangle:
                    return this.BuildRndRect();
                case Shapes.Circle:
                    _objIsCirc = true;
                    return this.BuildCircle();
                case Shapes.Oval:
                    _objIsCirc = true;
                    return this.BuildOval();
                case Shapes.Triangle:
                    return BuildTriangle();
                case Shapes.Arrow:
                    return BuildArrow();
                case Shapes.Dice:
                    return BuildDice();
                default:
                    return null;
            }
        }

        /// <summary>
        /// The BuildBrush Routine determines what type of fill was selected for the shape and Builds a brush to accordingly
        /// </summary>
        internal Brush BuildBrush()
        {
            Brush brush = null;
            switch (_image3DStyle)
            {
                default:
                case Shape3D.Flat:
                    if (_imageHatched == true)
                    {
                        brush = this.BuildHatchBrush();
                    }
                    else
                    {
                        brush = this.BuildFlatBrush();
                    }
                    break;
                case Shape3D.Shaded:
                    brush = this.BuildBrushes();
                    break;
                case Shape3D.Shaded3D:
                    brush = this.Build3DBrush();
                    break;
            }
            return brush;
        }

        //The Rebuild() Routine re-Builds all elements on the screen
        private void ReBuild()
        {
            //Draw The Object to the screen
            Invalidate();
        }

        /// <summary>
        /// Paints the control in the proper state.
        /// </summary>
        /// <param name="graphics">An instance of the <see cref="Graphics"/>
        /// object to paint on.</param>
        protected override void Draw(Graphics graphics)
        {
            if (Width <= 0 || Height <= 0)
                return;

            graphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
            graphics.SmoothingMode = SmoothingMode.AntiAlias;

            if (_border == true) // check if we need to draw the border
            {
                GraphicsPath grfxObject = GetShapePath();
                Pen p = GetPen();
                graphics.DrawPath(p, grfxObject);
                p.Dispose();
                grfxObject.Dispose();
                grfxObject = null;
                p = null;
            }

        }

        /// <summary>
        /// Get the size and location of the image
        /// </summary>
        public Rectangle ImageRectangle
        {
            get { return _imageRectangle; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="Widget"/>.
        /// </summary>
        /// <param name="x">The horizontal coordinate of the Widget relative to
        /// it's parent object.</param>
        /// <param name="y">The vertical coordinate of the Widget relative to
        /// it's parent object.</param>
        /// <param name="width">The width of the Widget.</param>
        /// <param name="height">The height of the Widget.</param>
        /// 
        public Widget(int x, int y, int width, int height)
            :
            base(x, y, width, height)
        {
            _drawHelper = new DrawHelper();

            _textAlign = ContentAlignment.MiddleCenter;
        }
        /// <summary>
        /// 
        /// </summary>
        public Widget()
            :
            this(0, 0, 1, 1)
        {
        }
        #endregion

        #region Methods

        public virtual Color GetCurrentForeColor()
        {
            return this.ForeColor;
        }
        public virtual Color GetCurrentBackColor()
        {
            return this.BackColor;
        }

        /// <summary>
        /// Sets the location and size of the widget
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void SetBounds(int x, int y, int width, int height)
        {
            this.X = x;
            this.Y = y;
            this.Width = width;
            this.Height = height;
        }

        #region Preferred Size
        /*
            The preferred size is used to override the size and location of the orginal shape.
         * This functionality was required for the Connection Class.
         */
        /// <summary>
        /// Get x
        /// </summary>
        /// <returns></returns>
        public virtual int GetPreferredX()
        {
            return this.X;
        }
        /// <summary>
        /// Get y
        /// </summary>
        /// <returns></returns>
        public virtual int GetPreferredY()
        {
            return this.Y;
        }
        /// <summary>
        /// Get height
        /// </summary>
        /// <returns></returns>
        public virtual int GetPreferredHeight()
        {
            return this.Height;
        }
        /// <summary>
        /// Get Width
        /// </summary>
        /// <returns></returns>
        public virtual int GetPreferredWidth()
        {
            return this.Width;
        }
        #endregion

        /// <summary>
        /// Get the x offset
        /// </summary>
        /// <returns></returns>
        public virtual int GetXCenter()
        {
            return this.GetPreferredX() + (int)Math.Round(this.GetPreferredWidth() * .5);
        }
        /// <summary>
        /// Get the y offset
        /// </summary>
        /// <returns></returns>
        public virtual int GetYCenter()
        {
            return this.GetPreferredY() + (int)Math.Round(this.GetPreferredHeight() * .5);
        }
        /// <summary>
        /// Moves the Widget up in the zorder
        /// </summary>
        public virtual void BringToFront()
        {
            ZOrder = true;
        }
        /// <summary>
        /// Moves the widget down in the zorder
        /// </summary>
        public virtual void SendToBack()
        {
            ZOrder = false;
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Raises the <see cref="Click"/> event.
        /// </summary>
        /// <param name="e">
        /// An <see cref="EventArgs"/> that contains the event data.
        /// </param>
        protected virtual void OnClick(EventArgs e)
        {
            EventHandler handler = Click;
            if (null != handler)
                handler(this, e);
        }

        /// <summary>
        /// Handles mouse when it enters the button and changes the Widget's state
        /// appropriately.
        /// </summary>
        /// <param name="posX">The x-coordinate of the mouse.</param>
        /// <param name="posY">The y-coordinate of the mouse.</param>
        protected override void OnMouseEnter(int posX, int posY)
        {
            if (MouseEnter != null)
                MouseEnter(this, EventArgs.Empty);
        }

        /// <summary>
        /// Handles mouse when it hovers over the control and changes the Widget's state
        /// appropriately.
        /// </summary>
        /// <param name="posX">The x-coordinate of the mouse.</param>
        /// <param name="posY">The y-coordinate of the mouse.</param>
        protected override void OnMouseHover(int posX, int posY)
        {
            if (MouseMove != null)
                MouseMove(this, new MouseEventArgs(MouseButtons.None, 1, posX, posY, 0));
        }

        /// <summary>
        /// Handles mouse when it leaves the button and changes the Widget's state
        /// appropriately.
        /// </summary>
        /// <param name="posX">The x-coordinate of the mouse.</param>
        /// <param name="posY">The y-coordinate of the mouse.</param>
        protected override void OnMouseLeave(int posX, int posY)
        {
            if (MouseLeave != null)
                MouseLeave(this, EventArgs.Empty);
        }

        /// <summary>
        /// Handles mouse down.
        /// </summary>
        /// <param name="posX">
        /// The horizontal position of the mouse.
        /// </param>
        /// <param name="posY">
        /// The vertical position of the mouse.
        /// </param>
        /// <param name="buttons">
        /// A <see cref="MouseButtons"/> that describes the mouse buttons
        /// pressed.
        /// </param>
        protected override void OnMouseDown(int posX, int posY, MouseButtons buttons)
        {
            if (OnSelected != null)
                OnSelected(this, EventArgs.Empty);

            if (buttons == MouseButtons.Left)
            {
                Dragging = true;
            }

            if (MouseDown != null)
                MouseDown(this, new MouseEventArgs(buttons, 1, posX, posY, 0));
        }

        /// <summary>
        /// Fire the double click event
        /// </summary>
        protected override void OnDoubleClick()
        {
            if (DoubleClick != null)
                DoubleClick(this, EventArgs.Empty);
        }

        /// <summary>
        /// Handles mouse up and, optionally, raises <see cref="Click"/> event.
        /// </summary>
        /// <param name="posX">
        /// The horizontal position of the mouse.
        /// </param>
        /// <param name="posY">
        /// The vertical position of the mouse.
        /// </param>
        /// <param name="buttons">
        /// A <see cref="MouseButtons"/> that describes the mouse buttons
        /// pressed.
        /// </param>
        /// <event cref="Click">Can be raised by this method.</event>
        protected override void OnMouseUp(int posX, int posY, MouseButtons buttons)
        {
            Dragging = false;

            if (MouseUp != null)
                MouseUp(this, new MouseEventArgs(buttons, 1, posX, posY, 0));

            this.BringToFront();

            if (Click != null)
                Click(this, EventArgs.Empty);
        }

        /// <summary>
        /// Remove widget from container
        /// </summary>
        /// <param name="container"></param>
        protected override void OnRemoveFromContainer(WidgetCanvas container)
        {
            DrawHelper.Instance.Cleanup();

            _tag = null;

            base.OnRemoveFromContainer(container);
        }

        #endregion

        /// <summary>
        /// Free memory
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();
            _drawHelper = null;
        }
    }

    #region Enumations

    /// <summary>
    /// Image quality
    /// </summary>
    public enum Quality
    {
        /// <summary>
        /// HighQuality
        /// </summary>
        HighQuality = 0,
        /// <summary>
        /// AntiAlias
        /// </summary>
        AntiAlias = 1,
        /// <summary>
        /// HighSpeed
        /// </summary>
        HighSpeed = 2,
        /// <summary>
        /// DefaultQuality
        /// </summary>
        DefaultQuality = 3,
        /// <summary>
        /// None
        /// </summary>
        None = 4
    }

    /// <summary>
    /// Widget basic shapes
    /// </summary>
    public enum Shapes
    {
        /// <summary>
        /// Rectangle
        /// </summary>
        Rectangle = 0,
        /// <summary>
        /// Square
        /// </summary>
        Square = 1,
        /// <summary>
        /// Oval
        /// </summary>
        Oval = 2,
        /// <summary>
        /// Circle
        /// </summary>
        Circle = 3,
        /// <summary>
        /// RoundedRectangle
        /// </summary>
        RoundedRectangle = 4,
        /// <summary>
        /// RoundedSquare
        /// </summary>
        RoundedSquare = 5,
        /// <summary>
        /// Triangle
        /// </summary>
        Triangle = 6,
        /// <summary>
        /// Arrow
        /// </summary>
        Arrow = 7,
        /// <summary>
        /// Dice
        /// </summary>
        Dice = 8,
        /// <summary>
        /// Custom
        /// </summary>
        Custom = 10

    }

    /// <summary>
    /// Shape's 3D look
    /// </summary>
    public enum Shape3D
    {
        /// <summary>
        /// Flat
        /// </summary>
        Flat = 0,
        /// <summary>
        /// Shaded
        /// </summary>
        Shaded = 1,
        /// <summary>
        /// Shaded3D
        /// </summary>
        Shaded3D = 2
    }
    /// <summary>
    /// Direction of the arrow
    /// </summary>
    public enum ArrowDirection
    {
        /// <summary>
        /// Top
        /// </summary>
        Top = 0,
        /// <summary>
        /// Right
        /// </summary>
        Right = 1,
        /// <summary>
        /// Bottom
        /// </summary>
        Bottom = 2,
        /// <summary>
        /// Left
        /// </summary>
        Left = 3
    }
    #endregion
}
