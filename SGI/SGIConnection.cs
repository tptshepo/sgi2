using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace SGI
{
    /// <summary>
    /// Connects two widget objects
    /// </summary>
    [Serializable]
    public class SGIConnection : InteractiveWidget
    {
        #region Fields

        private Point[] _points4 = new Point[4];
        private bool _reverse = false;
        private Point _linePointStart;
        private Point _linePointEnd;

        private Widget _fromWidget;
        private Widget _toWidget;

        #endregion

        /// <summary>
        /// Gets or sets the Widget to  connect to
        /// </summary>
        public Widget ToWidget
        {
            get { return _toWidget; }
            set { _toWidget = value; }
        }

        /// <summary>
        /// Gets or sets the widget to connect from
        /// </summary>
        public Widget FromWidget
        {
            get { return _fromWidget; }
            set { _fromWidget = value; }
        }

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public SGIConnection()
            : base(0, 0, 1, 1)
        {
            this.ZOrder = false;
            this.Controller.Movable = false;
            this.Controller.Resizable = false;
            this.Controller.ObjectType = ObjectTypes.Line;
            this.Shape = Shapes.Custom;

            this.ToggleBackcolor = true;
            this.ToggleForeColor = true;

            this.BorderWidth = 2;

            this.BackColor = Color.Black;

            this.HoverBackColor = Color.FromArgb(42, 170, 255);
            this.SelectedBackColor = Color.FromArgb(255, 212, 85);

            this.ForeColor = Color.White;
            this.SelectedForeColor = Color.Black;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public SGIConnection(Widget from, Widget to)
            : this()
        {
            _fromWidget = from;
            _toWidget = to;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Add widget to container
        /// </summary>
        /// <param name="container"></param>
        protected override void OnAddToContainer(WidgetCanvas container)
        {
            base.OnAddToContainer(container);

            if (this.Connector != null)
                container.Widgets.Remove(this.Connector);
        }

        /// <summary>
        /// Draws the connecting line between the two widgets
        /// </summary>
        /// <param name="graphics"></param>
        protected override void Draw(Graphics graphics)
        {
            _reverse = false;
            ComputePath(_fromWidget, _toWidget);
            this.Controller.LinePointStart = _linePointStart;
            this.Controller.LinePointEnd = _linePointEnd;

            Rectangle rectangleText = new Rectangle();

            Pen startPen = GetPen(GetCurrentBackColor(), this.BorderWidth);
            Pen endPen = GetPen(GetCurrentBackColor(), this.BorderWidth);

            PenCapProvider penCap = new PenCapProvider();

            endPen.CustomEndCap = penCap.GetSingleRepresentation();

            try
            {
                if (!_reverse)
                {
                    graphics.DrawLine(startPen, this._points4[0], this._points4[1]);
                    graphics.DrawLine(startPen, this._points4[1], this._points4[2]);
                    graphics.DrawLine(endPen, this._points4[2], this._points4[3]);
                    endPen.CustomEndCap.Dispose();

                    #region Draw Text
                    rectangleText.Location = this._points4[1];
                    rectangleText.Size = new Size(200, 30);
                    rectangleText.X += 10;
                    rectangleText.Y -= rectangleText.Size.Height;

                    using (Font font = new Font(this.FontName, 8))
                    {
                        using (SGI.TextPainter painter = new SGI.TextPainter())
                        {
                            painter.DrawString(graphics, this.Text, font, rectangleText.X, rectangleText.Y,
                                rectangleText.Width, rectangleText.Height, Color.Black, false);
                        }
                    }
                    //using (Pen p = new Pen(Color.Red))
                    //    graphics.DrawRectangle(p, rectangleText); 
                    #endregion

                    return;
                }
                graphics.DrawLine(endPen, this._points4[1], this._points4[0]);
                graphics.DrawLine(startPen, this._points4[1], this._points4[2]);
                graphics.DrawLine(startPen, this._points4[3], this._points4[2]);
                endPen.CustomEndCap.Dispose();

                #region Draw Text

                if (ToWidget.X < FromWidget.X)
                {
                    //is on the top left
                    rectangleText.Location = this._points4[1];
                }
                else if (ToWidget.X > FromWidget.X)
                {
                    //is on the top right
                    rectangleText.Location = this._points4[1];
                }
                else
                {
                    //is on the top middle
                    rectangleText.Location = this._points4[1];
                }

                rectangleText.Size = new Size(200, 30);
                rectangleText.X += 10;
                rectangleText.Y -= rectangleText.Size.Height;
                using (Font font = new Font(this.FontName, 8))
                {
                    using (SGI.TextPainter painter = new SGI.TextPainter())
                    {
                        painter.DrawString(graphics, this.Text, font, rectangleText.X, rectangleText.Y,
                            rectangleText.Width, rectangleText.Height, Color.Black, false);
                    }
                }
                //using (Pen p = new Pen(Color.Red))
                //    graphics.DrawRectangle(p, rectangleText);

                #endregion

                return;
            }
            finally
            {
                penCap.Dispose();
                startPen.Dispose();
                endPen.Dispose();
            }
        }

        /// <summary>
        /// Checks if the point is in the line's path
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public virtual bool Contains(Point p)
        {
            //make the collision detection slightly inaccurate,
            //or else the user has to click with single pixel accuracy 
            //allow some fuzzy , and detect the point even if it is clicked
            //2 or 3 pixels around the line.
            int fuzzy = 3;
            // first check if the click was made on the relation text, 
            // since it is natural to the user to click on the relation text
            // rather than on the lines.
            //if (this.reltext != String.Empty && this.relTextRectF_.Contains(p))
            // return true;

            GraphicsPath path1 = new GraphicsPath();
            GraphicsPath path2 = new GraphicsPath();
            GraphicsPath path3 = new GraphicsPath();
            path1.AddLines(new Point[]{
								  new Point( this._points4[0].X-fuzzy,this._points4[0].Y-fuzzy),
								  new Point( this._points4[0].X+fuzzy,this._points4[0].Y+fuzzy),
								  new Point( this._points4[1].X+fuzzy,this._points4[1].Y+fuzzy),
								  new Point( this._points4[1].X-fuzzy,this._points4[1].Y-fuzzy),
								  
				});
            path2.AddLines(new Point[]{
								  new Point( this._points4[1].X-fuzzy,this._points4[1].Y-fuzzy),
								  new Point( this._points4[1].X+fuzzy,this._points4[1].Y+fuzzy),
								  new Point( this._points4[2].X+fuzzy,this._points4[2].Y+fuzzy),
								  new Point( this._points4[2].X-fuzzy,this._points4[2].Y-fuzzy),
								  
				});
            path3.AddLines(new Point[]{
								  new Point( this._points4[2].X-fuzzy,this._points4[2].Y-fuzzy),
								  new Point( this._points4[2].X+fuzzy,this._points4[2].Y+fuzzy),
								  new Point( this._points4[3].X+fuzzy,this._points4[3].Y+fuzzy),
								  new Point( this._points4[3].X-fuzzy,this._points4[3].Y-fuzzy),
								  
				});
            return path1.IsVisible(p) || path2.IsVisible(p) || path3.IsVisible(p);


        }

        /// <summary>
        /// Checks if the point is in the line's path
        /// </summary>
        /// <param name="posX"></param>
        /// <param name="posY"></param>
        /// <returns></returns>
        protected override bool HitTest(int posX, int posY)
        {
            return Contains(new Point(posX, posY));
        }

        /// <summary>
        /// Get half the width
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        internal virtual int Half(int value)
        {
            return (int)Math.Round(value * .5);
        }

        /// <summary>
        /// Calculate where to put the lines
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        public virtual void ComputePath(Widget r1, Widget r2)
        {
            int x1 = r1.X,
                y1 = r1.Y,
                x2 = r2.X,
                y2 = r2.Y,
                w1 = r1.Width, h1 = r1.Height,
                w2 = r2.Width, h2 = r2.Height;

            int index = 0;
            int minConnectorSize = 6; //connector size;

            int xoffset1, xoffset2, yoffset1, yoffset2;

            _points4 = new Point[4];

            xoffset1 = r1.GetXCenter();
            xoffset2 = r2.GetXCenter();
            yoffset1 = r1.GetYCenter();
            yoffset2 = r2.GetYCenter();


            if (x1 + w1 < x2)//left of the rectangle.
            {
                /* [r1] 
                 *		[r2] */

                if (y1 + h1 < y2)
                {
                    /*
                     * [r1]
                     *  | 
                     *  -------------->[r2]
                     * */
                    this._points4[0].X = xoffset1;
                    this._points4[0].Y = y1 + h1;

                    this._points4[1].X = xoffset1;
                    //this._points4[1].Y = (y1 + h1) + Half((y2 - (y1 + h1)));  //(y1 + h1 + y2) / 2 + index * 5;
                    this._points4[1].Y = y2 + Half(h2);

                    this._points4[2].X = this._points4[1].X;
                    this._points4[2].Y = this._points4[1].Y;

                    this._points4[3].X = x2;
                    this._points4[3].Y = this._points4[1].Y;

                    //7 is the MinConnectorSize of the connector
                    _linePointStart = new Point(xoffset1 - (int)(minConnectorSize * .5), y1 + h1);
                    _linePointEnd = new Point(x2 - minConnectorSize, yoffset2 - (int)(minConnectorSize * .5));

                    return;
                }
                if (y1 > y2 + h2)
                {
                    /*			      |---[r2]
                     *                |
                     *         |------| 
                     *         |
                     *        [r1]
                     *	
                    */

                    //this._points4[0].X = xoffset1;
                    //this._points4[0].Y = y1 - 5;

                    //this._points4[1].X = xoffset1;
                    //this._points4[1].Y = (y1 + h1 + y2) / 2 + index * 5;

                    //this._points4[2].X = xoffset2;
                    //this._points4[2].Y = this._points4[1].Y;

                    //this._points4[3].X = xoffset2;
                    //this._points4[3].Y = y2 + h2;
                    //this._numPoints = 4;


                    /*	       [r2]
                     *          |
                     *          |	
                     *          |
                     * [r1]-----|
                     * 
                     * */

                    this._points4[0].X = x1 + w1 + 5;
                    this._points4[0].Y = yoffset1;

                    this._points4[1].X = x1 + w1;
                    this._points4[1].Y = yoffset1;

                    this._points4[2].X = xoffset2;
                    this._points4[2].Y = yoffset1;

                    this._points4[3].X = xoffset2;
                    this._points4[3].Y = y2 + h2;

                    _linePointStart = new Point(xoffset1 - (int)(minConnectorSize * .5), y1 - minConnectorSize);
                    _linePointEnd = new Point(x2 - minConnectorSize, yoffset2 - (int)(minConnectorSize * .5));

                    return;
                }

                /*			      |---[r2]
                 *        [r1]----|
                 *	
                 */

                /*
                 * override location and size
                    this is required when the connection is starting at the wrong point 
                 * because of the custom drawings
                 */

                x1 = r1.GetPreferredX(); y1 = r1.GetPreferredY();
                x2 = r2.GetPreferredX(); y2 = r2.GetPreferredY();
                w1 = r1.GetPreferredWidth(); h1 = r1.GetPreferredHeight();
                w2 = r2.GetPreferredWidth(); h2 = r2.GetPreferredHeight();
                yoffset1 = r1.GetPreferredY() + (int)Math.Round(r1.GetPreferredHeight() * .5);
                yoffset2 = r2.GetPreferredY() + (int)Math.Round(r2.GetPreferredHeight() * .5);
                /************************/

                this._points4[0].X = x1 + w1;
                this._points4[0].Y = yoffset1;
                this._points4[1].X = this._points4[0].X + (x2 - this._points4[0].X) / 2;
                this._points4[1].Y = this._points4[0].Y;
                this._points4[2].X = this._points4[1].X;
                this._points4[2].Y = yoffset2;
                this._points4[3].X = x2;
                this._points4[3].Y = this._points4[2].Y;

                _linePointStart = new Point(x1 + w1, yoffset1 - (int)(minConnectorSize * .5));
                _linePointEnd = new Point(x2 - minConnectorSize, yoffset2 - (int)(minConnectorSize * .5));
                return;
            }
            else if (x1 + w1 <= x2 + w2)
            {
                /*
                 * [r1]
                 *   [r2]
                 * */

                if (y1 + h1 < y2)
                {
                    /* [r1]
                     *  |
                     *  |---|
                     *	    |
                     *	   [r2]
                     * */

                    this._points4[0].X = xoffset1;
                    this._points4[0].Y = y1 + h1;
                    this._points4[3].X = xoffset2;
                    this._points4[3].Y = y2 - 5;
                    this._points4[1].X = this._points4[0].X;
                    //this._points4[1].Y = (y1 + h1) + Half((y2 - (y1 + h1)));  //(y1 + h1 + y2) / 2 + index * 5;
                    this._points4[1].Y = (y1 + h1 + y2) / 2 + index * 5;

                    this._points4[2].X = this._points4[3].X;
                    this._points4[2].Y = this._points4[1].Y;


                    _linePointStart = new Point(xoffset1 - (int)(minConnectorSize * .5), y1 + h1);
                    _linePointEnd = new Point(xoffset2 - (int)(minConnectorSize * .5), y2 - minConnectorSize);
                    return;
                }
                if (y1 > y2 + h2)
                {
                    /*	       [r2]
                     *          |
                     *  |-------|	
                     *  |
                     * [r1]
                     * 
                     * */

                    this._points4[0].X = xoffset1;
                    this._points4[0].Y = y1 - 5;
                    this._points4[1].X = xoffset1;
                    this._points4[1].Y = (y2 + h2 + y1) / 2 + index * 5;//right
                    this._points4[2].X = xoffset2;
                    this._points4[2].Y = this._points4[1].Y;//right
                    this._points4[3].X = xoffset2;
                    this._points4[3].Y = y2 + h2;

                    /*	       [r2]
                     *          |
                     *          |	
                     *          |
                     * [r1]-----|
                     * 
                     * */

                    //this._points4[0].X = x1 + w1;
                    //this._points4[0].Y = yoffset1;

                    //this._points4[1].X = x1 + w1;
                    //this._points4[1].Y = yoffset1;

                    //this._points4[2].X = xoffset2;
                    //this._points4[2].Y = yoffset1;

                    //this._points4[3].X = xoffset2;
                    //this._points4[3].Y = y2 + h2;
                    //this._numPoints = 4;


                    _linePointStart = new Point(xoffset1 - (int)(minConnectorSize * .5), y1 - minConnectorSize);
                    _linePointEnd = new Point(xoffset2 - (int)(minConnectorSize * .5), y2 + h2);
                    return;
                }

                //the two objects collided
                return;

            }
            this._reverse = true;
            this.ComputePath(r2, r1);
        }

        #endregion

        /// <summary>
        /// Free memory
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();

            _points4 = null;
            _fromWidget = null;
            _toWidget = null;
        }

    }
}
