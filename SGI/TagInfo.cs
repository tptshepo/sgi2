﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SGI
{
    /// <summary>
    /// Stores more information about an object
    /// </summary>
    public class TagInfo : IBaseCollectionItem
    {
        /// <summary>
        /// Gets or sets the id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the text
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the tag
        /// </summary>
        public object Tag { get; set; }

        public TagInfo()
        {
        }

        public TagInfo(string id, string text)
        {
            Id = id;
            Text = text;
        }

    }

    /// <summary>
    /// Taginfo collection class
    /// </summary>
    public class TagsInfo : BaseCollection<TagInfo>
    {

    }
}
