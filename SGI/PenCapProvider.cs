using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace SGI
{
    /// <summary>
    /// Interface that has to be implemented by a Pen Cap Provider
    /// </summary>
    [Serializable]
    public class PenCapProvider: IDisposable
    {
        private GraphicsPath _path = null;
       
        /// <summary>
        /// Constructor
        /// </summary>
        public PenCapProvider()
        {
            _path = new GraphicsPath();
        }

        /// <summary>
        /// Returns a arrow cap
        /// </summary>
        /// <returns></returns>
        public CustomLineCap GetSingleRepresentation()
        {
            CustomLineCap singleCap;

            int width = 0, height = 0;
            int span = 0;
            Matrix translateMatrix = null;
            width = 2; height = 2;

            _path.StartFigure();
            _path.AddPolygon(new PointF[] { new PointF(0, 0), new PointF(width / 2, height), new PointF(width, 0) });

            // set the single arrow.
            span = height;
            translateMatrix = new Matrix();
            translateMatrix.Translate(-width / 2, -span);
            _path.Transform(translateMatrix);

            _path.CloseFigure();

            singleCap = new CustomLineCap(null, _path);
            singleCap.BaseInset = span;

            return singleCap;
        }

        #region IDisposable Members

        /// <summary>
        /// Free Memory
        /// </summary>
        public void Dispose()
        {
            _path.Dispose();
            _path = null;
        }

        #endregion
    }
}
