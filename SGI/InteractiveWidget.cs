using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace SGI
{

    /// <summary>
    /// This MovableWidget class adds moving, Selecting and Resizing support to the Widget
    /// of the control with the mouse
    /// </summary>
    [Serializable]
    public abstract class InteractiveWidget : Widget, ISelectable
    {
        #region Fields
        
        private WidgetController _controller;

        private Color _selectedForeColor = Color.Orange;
        private Color _selectedBorderColor = Color.Blue;
        private Color _selectedBackColor = Color.Blue;
        private Color _hoverBackColor = Color.LightBlue;

        private Color _flashBackColor = Color.Red;
        private Color _flashForeColor = Color.White;

        private bool _selectable = true;
        private bool _connectable = false;
        private LineConnector _lineConnector;

        private bool _selected = false;
        private bool _toggleForeColor = false;
        private bool _toogleBackcolor = false;
        private bool _toogleHoverBackcolor = false;
        private bool _hovering = false;

       // private Color _currentBackColor = Color.Black;

        /// <summary>
        /// Fires when a widget is selected
        /// </summary>
        public event EventHandler OnSelected;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler OnUnSelected;

        // tooltip
        private WidgetToolTip _toolTip;
        private string _toolTipText = string.Empty;

        //private Timer _flashTimer;

        #endregion

        #region Properties

        public bool ToogleHoverBackcolor
        {
            get { return _toogleHoverBackcolor; }
            set { _toogleHoverBackcolor = value; }
        }

        //public Color CurrentBackColor
        //{
        //    get { return _currentBackColor; }
        //}

        /// <summary>
        /// Gets or sets the border color when the widget is selected
        /// </summary>
        public Color SelectedForeColor
        {
            get { return _selectedForeColor; }
            set
            {
                if (_selectedForeColor != value)
                {
                    _selectedForeColor = value;
                }
            }
        }

        /// <summary>
        /// Gets or set the border color
        /// </summary>
        public Color SelectedBorderColor
        {
            get { return _selectedBorderColor; }
            set { _selectedBorderColor = value; }
        }

        /// <summary>
        /// Gets or sets the backcolor when the widget is selected
        /// </summary>
        public Color SelectedBackColor
        {
            get { return _selectedBackColor; }
            set { _selectedBackColor = value; }
        }

        /// <summary>
        /// Gets or sets the text for the tooltip
        /// </summary>
        public string ToolTipText
        {
            get { return _toolTipText; }
            set { _toolTipText = value; }
        }

        /// <summary>
        /// Gets or sets the backcolor when the mouse hovers over the widget.
        /// </summary>
        public Color HoverBackColor
        {
            get { return _hoverBackColor; }
            set { _hoverBackColor = value; }
        }

        /// <summary>
        /// Gets or sets if the forecolor is changed when the widget is selected.
        /// </summary>
        public bool ToggleForeColor
        {
            get { return _toggleForeColor; }
            set { _toggleForeColor = value; }
        }

        /// <summary>
        /// Gets or sets if the widget is selected
        /// </summary>
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }

        /// <summary>
        /// Gets or sets if the backcolor can change when the widget is selected
        /// </summary>
        public bool ToggleBackcolor
        {
            get { return _toogleBackcolor; }
            set { _toogleBackcolor = value; }
        }

        /// <summary>
        /// Gets or sets the connection connector object
        /// </summary>
        public LineConnector Connector
        {
            get { return _lineConnector; }
            set { _lineConnector = value; }
        }

        /// <summary>
        /// Gets or sets if the line connectors are visible
        /// </summary>
        public bool Connectable
        {
            get { return _connectable; }
            set
            {
                _connectable = value;
            }
        }


        /// <summary>
        /// Gets the motion support for the widget element
        /// </summary>
        public WidgetController Controller
        {
            get { return _controller; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">X location</param>
        /// <param name="y">Y location</param>
        /// <param name="width">Width of the element</param>
        /// <param name="height">Height of the element</param>
        public InteractiveWidget(int x, int y, int width, int height)
            :
            base(x, y, width, height)
        {
            _controller = new WidgetController(this);

            //_flashTimer = new Timer();
            //_flashTimer.Enabled = false;
            //_flashTimer.Interval = 500;
            //_flashTimer.Tick += new EventHandler(_flashTimer_Tick);
        }
        #endregion

        #region Methods

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="bgcolor"></param>
        ///// <param name="textColor"></param>
        //public void FlashColor(Color bgcolor, Color textColor)
        //{
        //    _flashBackColor = bgcolor;
        //    _flashForeColor = textColor;
        //    _flashTimer.Enabled = true;
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        //public void StopFlashColor()
        //{
        //    _flashTimer.Enabled = false;
        //    this.BackColor = _defaultBackColor;
        //    this.ForeColor = _defaultForeColor;
        //}


        /// <summary>
        /// Selects the widget
        /// </summary>
        public void Select()
        {
            // check if the widget is allowed to be connected to
            if (_connectable)
            {
                if (_lineConnector != null)
                {
                    if (!this.Parent.Control.MultipleSelected)
                    {
                        if (!this.Parent.Control.MultiSelecting)
                        {
                            _lineConnector.Visible = true;
                            _lineConnector.BringToFront();
                        }
                    }
                }
            }

            // check if the widget is selectable
            if (_selectable)
            {
                // check if is not selected already
                if (_selected)
                    return;

                _selected = true;

                if (OnSelected != null)
                    OnSelected(this, EventArgs.Empty);
            }

            if (this._controller.Movable)
            {
            }
        }

        /// <summary>
        /// Removes the selection from the control
        /// </summary>
        public void UnSelect()
        {
            _selected = false;

            if (this.Connectable)
            {
                if (_lineConnector != null)
                    _lineConnector.Visible = false;
            }

            if (OnUnSelected != null)
                OnUnSelected(this, EventArgs.Empty);

            Invalidate();
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateLineConnectorLocation()
        {
            if (_lineConnector != null)
            {
                _lineConnector.Location = GetLineConnectorLocation();
            }
        }

        /// <summary>
        /// Get the location of the line connector
        /// </summary>
        /// <returns></returns>
        protected virtual Point GetLineConnectorLocation()
        {
            int gap = 16;
            return new Point(this.X + (int)(this.Width * .5) - 30, this.Y - gap);
        }

        /// <summary>
        /// Get a <see cref="Rectangle"/> to indicate selection area
        /// </summary>
        /// <returns></returns>
        protected virtual Rectangle GetSelectionArea()
        {
            return this.ClientRectangle;
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Flash timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _flashTimer_Tick(object sender, EventArgs e)
        {
            //_flashTimer.Enabled = false;

            //if (!_selected)
            //{
            //    if (!_hover)
            //    {
            //        if (toggler == 0)
            //        {
            //            this.BackColor = _flashBackColor;
            //            this.ForeColor = _flashForeColor;
            //            toggler = 1;
            //        }
            //        else
            //        {
            //            this.BackColor = _defaultBackColor;
            //            this.ForeColor = _defaultForeColor;
            //            toggler = 0;
            //        }
            //    }
            //    else
            //        this.ForeColor = _defaultForeColor;
            //}

            //_flashTimer.Enabled = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override Color GetCurrentForeColor()
        {
            if (Selected)
            {
                if (ToggleForeColor)
                {
                    return _selectedForeColor;
                }
                else
                    return this.ForeColor;
            }
            else
                return this.ForeColor;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override Color GetCurrentBackColor()
        {
            Color currentColor = this.BackColor;

            // change the backcolor when widget is selected
            if (_toogleBackcolor)
            {
                if (_selected)
                {
                    currentColor = _selectedBackColor;
                }
            }

            if (_hovering) // hover is on
            {
                if (!_selected) // widget no selected
                {
                    if (_toogleHoverBackcolor)
                    {
                        // hovering
                        currentColor = _hoverBackColor;
                    }
                }
            }
            else // hover off
            {
                if (!_selected) // widget no selected
                {
                    // hover item
                    //if (ShapeShading == Shape3D.Shaded3D)
                    //{
                    //    Brush pathBrush = BuildBrush();
                    //    graphics.FillRectangle(pathBrush, GetSelectionArea(graphics));
                    //    pathBrush.Dispose();
                    //}
                    //else
                    //{

                    currentColor = this.BackColor;
                    //}
                }
            }

            return currentColor;
        }

        /// <summary>
        /// Draw the widget
        /// </summary>
        /// <param name="graphics"></param>
        protected override void Draw(Graphics graphics)
        {
            base.Draw(graphics);

            SolidBrush brush = new SolidBrush(GetCurrentBackColor());
            graphics.FillRectangle(brush, GetSelectionArea());

            brush.Dispose();
            brush = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="posX"></param>
        /// <param name="posY"></param>
        protected override void OnMouseEnter(int posX, int posY)
        {
            _hovering = true;
            //_toolTipTimer.Enabled = true;
            base.OnMouseEnter(posX, posY);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="posX"></param>
        /// <param name="posY"></param>
        protected override void OnMouseLeave(int posX, int posY)
        {
            _hovering = false;
            //_toolTipTimer.Enabled = false;
            //HideToolTip();
            base.OnMouseLeave(posX, posY);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="posX"></param>
        /// <param name="posY"></param>
        /// <param name="buttons"></param>
        protected override void OnMouseDown(int posX, int posY, System.Windows.Forms.MouseButtons buttons)
        {
            UpdateLineConnectorLocation();
            base.OnMouseDown(posX, posY, buttons);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="container"></param>
        protected override void OnAddToContainer(WidgetCanvas container)
        {
            if (_connectable)
            {
                _lineConnector = new LineConnector(0, 0, 12, 10);
                _lineConnector.Visible = false;
                _lineConnector.ParentWidget = this;
                container.Widgets.Add(_lineConnector);
                _lineConnector.BringToFront();
            }

            base.OnAddToContainer(container);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="container"></param>
        protected override void OnRemoveFromContainer(WidgetCanvas container)
        {
            if (_connectable)
            {
                container.Widgets.Remove(_lineConnector);
                _lineConnector = null;
            }

            //_flashTimer.Enabled = false;
            //_flashTimer.Tick -= new EventHandler(_flashTimer_Tick);
            //_flashTimer = null;

            if (_controller != null)
            {
                _controller.Dispose();
                _controller = null;
            }

            base.OnRemoveFromContainer(container);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        protected override void OnLocationChanged(int x, int y, int width, int height)
        {
            UpdateLineConnectorLocation();
            base.OnLocationChanged(x, y, width, height);
        }

        #endregion
    }
}
