﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SGI;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using System.Xml.Linq;

namespace SGI
{
    /// <summary>
    /// Used to build custom rules around connecting two Entities together
    /// </summary>
    /// <param name="el1"></param>
    /// <param name="el2"></param>
    /// <returns></returns>
    public delegate bool CanConnect(SGIEntity el1, SGIEntity el2);

    /// <summary>
    /// Canvas is a container control for the <see cref="SGIEntity"/> objects
    /// </summary>
    public partial class Canvas : UserControl
    {
        #region Fields

        private SGIScene _currentScene;
        private Color _backColor = Color.White;
        private List<TagInfo> _drillPath;

        private int _defaultWidth = 135;
        private int _defaultHeight = 30;

        private bool _autoUpdateDrillPath = true;
        #endregion

        #region Events

        /// <summary>
        /// This event will fire when a scene path is selected
        /// </summary>
        public event EventHandler OnPathSelected;

        /// <summary>
        /// This event will fire when two <see cref="SGIEntity"/> are connected together
        /// </summary>
        public event EventHandler<EntityConnectionEventArgs> OnEntityConnected;

        /// <summary>
        /// This event will fire when an <see cref="SGIEntity"/> is added to the <see cref="Canvas"/>
        /// </summary>
        public event EventHandler<EntityEventArgs> OnEntityAdded;

        /// <summary>
        /// This event will fire when an <see cref="SGIEntity"/> is removed from the <see cref="Canvas"/>
        /// </summary>
        public event EventHandler<EntityEventArgs> OnEntityRemoved;

        /// <summary>
        /// This event will fire when an <see cref="SGIEntity"/> is deleted from the <see cref="Canvas"/>
        /// </summary>
        public event EventHandler<EntityEventArgs> OnEntityDelete;

        /// <summary>
        /// This event will fire when an <see cref="SGIEntity"/> is successfully deleted from the <see cref="Canvas"/>
        /// </summary>
        public event EventHandler<EntityEventArgs> OnEntityDeleted;

        /// <summary>
        /// This event will fire when an <see cref="SGIEntityConnection"/> is deleted from the <see cref="Canvas"/>
        /// </summary>
        public event EventHandler<EntityConnectionEventArgs> OnEntityConnectionDelete;

        /// <summary>
        /// This event will fire when an <see cref="SGIEntityConnection"/> is successfully deleted from the <see cref="Canvas"/>
        /// </summary>
        public event EventHandler<EntityConnectionEventArgs> OnEntityConnectionDeleted;

        /// <summary>
        /// This event will fire when an <see cref="SGIEntity"/> property changes
        /// </summary>
        public event EventHandler OnPropertyChange;

        /// <summary>
        /// This event will fire when an <see cref="SGIEntity"/> mouse up event fires
        /// </summary>
        public event EventHandler OnEntityMouseUp;


        /// <summary>
        /// This event will fire when the mouse is clicked on the canvas white space
        /// </summary>
        public event EventHandler OnWhiteSpaceClick;

        /// <summary>
        /// This event will fire when an <see cref="SGIEntity"/> is selected from the <see cref="Canvas"/>
        /// </summary>
        public event EventHandler<EventArgs> OnEntitySelected;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<EventArgs> OnDeleteCompleted;

        #endregion

        #region Properties

        public override Color BackColor
        {
            get
            {
                return _backColor;
            }
            set
            {
                this.wgCanvas.BackColor = value;
                _backColor = value;
            }
        }

        /// <summary>
        /// Get the core internal widget canvas
        /// </summary>
        [Browsable(false)]
        public WidgetCanvas CoreCanvas
        {
            get { return this.wgCanvas; }
        }

        [DefaultValue(false)]
        public bool ShowGrid
        {
            get { return this.wgCanvas.ShowGrid; }
            set { this.wgCanvas.ShowGrid = value; }
        }

        /// <summary>
        /// Gets or sets if the path bar is 
        /// automatically updated internal
        /// </summary>
        [Browsable(false)]
        public bool AutoUpdateDrillPath
        {
            get { return _autoUpdateDrillPath; }
            set { _autoUpdateDrillPath = value; }
        }

        /// <summary>
        /// Gets a list of entities 
        /// </summary>
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IEnumerable<SGIEntity> Entities
        {
            get
            {
                var list = from Widget ent in this.Items.List
                           where (ent is SGIEntity)
                           select ent;

                var ents = from SGIEntity ls in list select ls;
                return ents;
            }
        }

        /// <summary>
        /// Gets a list of connections 
        /// </summary>
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IEnumerable<SGIEntityConnection> Connections
        {
            get
            {
                var list = from Widget ent in this.Items.List
                           where (ent is SGIEntityConnection)
                           select ent;

                var conns = from SGIEntityConnection ls in list select ls;
                return conns;
            }
        }


        /// <summary>
        /// Gets a list of scenes
        /// </summary>
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IEnumerable<SGIScene> Scenes
        {
            get
            {
                var list = from Widget ent in this.Items.List
                           where (ent is SGIScene)
                           select ent;

                var scenes = from SGIScene sc in list select sc;
                return scenes;
            }
        }

        /// <summary>
        /// Gets the base canvas object
        /// </summary>
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public WidgetCanvas BaseCanvas
        {
            get
            {
                return this.wgCanvas;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating if multiple entities are being deleted
        /// </summary>
        [Browsable(false)]
        public bool MultiDelete { get; set; }


        /// <summary>
        /// Gets or sets a value indicating if multiple delete is allowed
        /// </summary>
        [Browsable(false)]
        public bool CanMultiDelete { get; set; }

        /// <summary>
        /// Gets or sets if the multi delete message has been shown already
        /// </summary>
        [Browsable(false)]
        public bool MultiDeleteMessageShown { get; set; }


        /// <summary>
        /// Gets or sets if the multi delete operation was cancelled
        /// </summary>
        [Browsable(false)]
        public bool MultiDeleteCancelled { get; set; }

        /// <summary>
        /// Gets or sets whether the view is in read only mode
        /// </summary>
        [Browsable(false)]
        public bool ReadOnly { get; set; }

        /// <summary>
        /// Gets if the current scene is the root scene
        /// </summary>
        [Browsable(false)]
        public bool IsRootScene
        {
            get { return _currentScene.Equals(RootScene); }
        }

        /// <summary>
        /// Gets the Path of the scene
        /// </summary>
        [Browsable(false)]
        public List<string> Paths
        {
            get
            {
                return _drillPath.Select(i => i.Text).ToList();
            }
        }


        /// <summary>
        /// Gets the <see cref="SGIEntity"/> collection
        /// </summary>
        [Browsable(false)]
        public WidgetsCollection Items { get { return this.wgCanvas.Widgets; } }

        /// <summary>
        /// Gets or sets if the drill through is autometically determined by the canvas when
        /// double clicking the <see cref="SGIEntity"/>.
        /// </summary>
        [Browsable(false)]
        public bool AutomaticDrillThrough { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="SGIEntityConnection"/> collection
        /// </summary>
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public SGIEntityConnectionCollection EntityConnectionList { get; set; }


        /// <summary>
        /// Gets or sets the selected object
        /// </summary>
        [Browsable(false)]
        public object SelectedObject { get; set; }


        /// <summary>
        /// Gets or sets the current loaded <see cref="SGIScene"/>
        /// </summary>
        [Browsable(false)]
        public SGIScene CurrentScene
        {
            get { return _currentScene; }
            set { _currentScene = value; }
        }

        /// <summary>
        /// Gets or sets the context menu strip for the canvas
        /// </summary>
        [Browsable(false)]
        public ContextMenuStrip CanvasContextMenu
        {
            get { return wgCanvas.ContextMenuStrip; }
            set { wgCanvas.ContextMenuStrip = value; }
        }


        /// <summary>
        /// Gets or sets the root scene
        /// </summary>
        [Browsable(false)]
        public SGIScene RootScene { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public Canvas()
        {
            InitializeComponent();

            _drillPath = new List<TagInfo>();

            wgCanvas.Widgets.AddWidget += new WidgetsCollection.WidgetEventArgs(Widgets_AddWidget);
            wgCanvas.Widgets.RemovedWidget += new WidgetsCollection.WidgetEventArgs(Widgets_RemovedWidget);

            wgCanvas.OnDelete += new EventHandler(wgCanvas_OnDelete);
            wgCanvas.OnClicked += new EventHandler(wgCanvas_OnClicked);

            EntityConnectionList = new SGIEntityConnectionCollection();

            this.wgCanvas.MultiSelectSupported = true;

            CanMultiDelete = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        public void HidePathBar()
        {
            this.pathBar.Visible = false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ShowPathBar()
        {
            this.pathBar.Visible = true;
        }

        /// <summary>
        /// Create the root  <see cref="SGIScene"/>
        /// </summary>
        public void CreateRootScene(string name)
        {
            _drillPath.Clear();
            RootScene = CreateScene("0", name);
        }
        /// <summary>
        /// 
        /// </summary>
        public void CreateRootScene()
        {
            CreateRootScene("Root");
        }

        /// <summary>
        /// Removes everything including the path bar items
        /// </summary>
        public void ClearAll()
        {
            _drillPath.Clear();
            ClearEntities();
            this.UpdateDrillPath();
            this.wgCanvas.Invalidate();
        }

        /// <summary>
        /// Clear all loaded <see cref="SGIScene"/>
        /// </summary>
        public void ClearEntities()
        {
            int countEnt = this.wgCanvas.Widgets.List.Count;

            for (int i = 0; i < this.wgCanvas.Widgets.List.Count; i++)
            {
                this.wgCanvas.Widgets.Remove(this.wgCanvas.Widgets.List[0]);
            }

            EntityConnectionList = new SGIEntityConnectionCollection();

            _currentScene = null;
        }

        /// <summary>
        /// Drills through an Entity
        /// </summary>
        /// <param name="viewScene"></param>
        /// <returns>Returns a nested scene</returns>
        public void ShowScene(SGIScene viewScene)
        {
            viewScene.BringToFront();
            viewScene.Visible = true;
        }

        /// <summary>
        /// Updates the drill path based on the opened <see cref="SGIScene"/>
        /// </summary>
        public void UpdateDrillPath()
        {
            foreach (var con in pathBar.Items)
            {
                con.Click -= new EventHandler(scenePath_Click);
                con.DisposeObjects();
            }

            pathBar.SuspendLayout();

            pathBar.ClearPaths();

            for (int i = 0; i < _drillPath.Count; i++)
            {
                PathBarItem path = new PathBarItem(_drillPath[i].Text);
                pathBar.AddPath(path);
                path.Click += new EventHandler(scenePath_Click);
                path.Tag = _drillPath[i];

                if (_drillPath.Count == 1) // only one item
                    path.SetStyle(PathBarItem.Styles.OnlyActive);
                else if (i == _drillPath.Count - 2) // item before last
                {
                    path.SetStyle(PathBarItem.Styles.InactiveNextActive);
                }
                else if (i == _drillPath.Count - 1) // last item
                {
                    path.SetStyle(PathBarItem.Styles.Active);
                }
                else // in between items
                {
                    path.SetStyle(PathBarItem.Styles.Inactive);
                }
            }

            pathBar.ResumeLayout();
            //pathBar.Visible = true;
        }

        /// <summary>
        /// Moves to the root <see cref="SGIScene"/>
        /// </summary>
        public void MoveToRootScene()
        {
            CreateRootScene();
        }

        /// <summary>
        /// 
        /// </summary>
        public void InvalidateCanvas()
        {
            this.wgCanvas.InvalidateAll();
            this.wgCanvas.Invalidate();
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Delete <see cref="SGIEntity"/> from canvas
        /// </summary>
        /// <param name="ent"></param>
        internal void DeleteEntityFromCanvas(SGIEntity ent)
        {
            EntityEventArgs args = new EntityEventArgs(ent);

            if (OnEntityDelete != null)
                OnEntityDelete(this, args);

            if (!args.Cancel)
            {
                this.wgCanvas.RemoveSelectionObject(ent);
                this.RemoveEntity(ent);
                SelectedObject = null;

                this.Invalidate();

                if (OnEntityDeleted != null)
                    OnEntityDeleted(this, new EntityEventArgs(ent));
            }

        }

        /// <summary>
        /// Remove entity from canvas
        /// </summary>
        /// <param name="ent"></param>
        public void DeleteEntity(SGIEntity ent)
        {
            DeleteEntityFromCanvas(ent);
        }

        /// <summary>
        /// Delete <see cref="SGIEntityConnection"/> from canvas
        /// </summary>
        /// <param name="conn"></param>
        internal void DeleteEntityConnectionFromCavas(SGIEntityConnection conn)
        {
            EntityConnectionEventArgs args = new EntityConnectionEventArgs(conn);

            if (OnEntityConnectionDelete != null)
                OnEntityConnectionDelete(this, args);

            if (!args.Cancel)
            {
                this.RemoveConnection(conn);
                SelectedObject = null;
                this.Invalidate();

                if (OnEntityConnectionDeleted != null)
                    OnEntityConnectionDeleted(this, new EntityConnectionEventArgs(conn));
            }

        }

        /// <summary>
        /// Remove connection from canvas
        /// </summary>
        /// <param name="conn"></param>
        public void DeleteConnection(SGIEntityConnection conn)
        {
            DeleteEntityConnectionFromCavas(conn);
        }
        #region Creation

        /// <summary>
        /// Create an <see cref="SGIEntity"/> on the <see cref="Canvas"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public T CreateEntity<T>(string name, string text) where T : new()
        {
            return CreateEntity<T>(name, text, false, false, false,
                new Point(50, 50), new Size(_defaultWidth, _defaultHeight));
        }

        /// <summary>
        /// Create an <see cref="SGIEntity"/> on the <see cref="Canvas"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public T CreateEntity<T>(string name, string text, bool movable) where T : new()
        {
            return CreateEntity<T>(name, text, movable, false, false,
                new Point(50, 50), new Size(_defaultWidth, _defaultHeight));
        }

        /// <summary>
        /// Create an <see cref="SGIEntity"/> on the <see cref="Canvas"/>
        /// </summary>
        /// <param name="text">Display text of the Entity</param>
        /// <returns></returns>
        public T CreateEntity<T>(string text) where T : new()
        {
            return CreateEntity<T>("Enity_" + Utilities.GenId(), text, false, false, false,
                new Point(50, 50), new Size(_defaultWidth, _defaultHeight));
        }

        /// <summary>
        /// Create an <see cref="SGIEntity"/> on the <see cref="Canvas"/>
        /// </summary>
        /// <param name="text">Display text of the Entity</param>
        /// <returns></returns>
        public T CreateEntity<T>(string text, Point location, Size size) where T : new()
        {
            return CreateEntity<T>("Enity_" + Utilities.GenId(), text, false, false, false, location, size);
        }

        /// <summary>
        /// Create an <see cref="SGIEntity"/> on the <see cref="Canvas"/>
        /// </summary>
        /// <param name="text">Display text of the Entity</param>
        /// <returns></returns>
        public T CreateEntity<T>(string name, string text, Point location, Size size) where T : new()
        {
            return CreateEntity<T>(name, text, false, false, false, location, size);
        }

        /// <summary>
        /// Create an <see cref="SGIEntity"/> on the <see cref="Canvas"/>
        /// </summary>
        /// <param name="text">Display text of the Entity</param>
        /// <param name="movable">Indicates if the Entity can be moved</param>
        /// <returns></returns>
        public T CreateEntity<T>(string text, bool movable) where T : new()
        {
            return CreateEntity<T>("Enity_" + Utilities.GenId(), text, movable, false, false,
                new Point(50, 50), new Size(_defaultWidth, _defaultHeight));
        }

        /// <summary>
        /// Create an <see cref="SGIEntity"/> on the <see cref="Canvas"/>
        /// </summary>
        /// <param name="text">Display text of the Entity</param>
        /// <param name="movable">Indicates if the Entity can be moved</param>
        /// <param name="resizable">Indicates if the Entity can be resized</param>
        /// <param name="connectable">Indicates if the Entity can connect to other objects </param>
        /// <returns></returns>
        public T CreateEntity<T>(string text, bool movable, bool resizable, bool connectable) where T : new()
        {
            return CreateEntity<T>("Enity_" + Utilities.GenId(), text, movable, resizable, connectable,
                new Point(50, 50), new Size(_defaultWidth, _defaultHeight));
        }


        /// <summary>
        /// Create an <see cref="SGIEntity"/> on the <see cref="Canvas"/>
        /// </summary>
        /// <param name="text">Display text of the Entity</param>
        /// <param name="movable">Indicates if the Entity can be moved</param>
        /// <param name="resizable">Indicates if the Entity can be resized</param>
        /// <param name="connectable">Indicates if the Entity can connect to other objects </param>
        /// <returns></returns>
        public T CreateEntity<T>(string text, bool movable, bool resizable, bool connectable, Point location) where T : new()
        {
            return CreateEntity<T>("Enity_" + Utilities.GenId(), text, movable, resizable, connectable,
                location, new Size(_defaultWidth, _defaultHeight));
        }

        /// <summary>
        /// Create an <see cref="SGIEntity"/> on the <see cref="Canvas"/>
        /// </summary>
        /// <param name="text">Display text of the Entity</param>
        /// <param name="movable">Indicates if the Entity can be moved</param>
        /// <param name="resizable">Indicates if the Entity can be resized</param>
        /// <param name="connectable">Indicates if the Entity can connect to other objects </param>
        /// <returns></returns>
        public T CreateEntity<T>(string name, string text, bool movable, bool connectable) where T : new()
        {
            return CreateEntity<T>(name, text, movable, false, connectable,
                 new Point(50, 50), new Size(_defaultWidth, _defaultHeight));
        }

        /// <summary>
        /// Create an <see cref="SGIEntity"/> on the <see cref="Canvas"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="text"></param>
        /// <param name="movable"></param>
        /// <param name="resizable"></param>
        /// <param name="connectable"></param>
        /// <param name="location"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public T CreateEntity<T>(string name, string text, bool movable, bool resizable, bool connectable, Point location, Size size) where T : new()
        {
            T entity = new T();

            SGIEntity internalEntity = entity as SGIEntity;

            if (internalEntity != null)
            {
                //Properties
                internalEntity.Location = new Point(location.X, location.Y);
                internalEntity.Size = new Size(size.Width, size.Height);

                internalEntity.Name = name;
                internalEntity.Text = text;
                internalEntity.Controller.Movable = movable;
                internalEntity.Controller.Resizable = resizable;
                internalEntity.Connectable = connectable;
                internalEntity.Location = location;
                internalEntity.Size = size;

                if (RootScene == null)
                    this.CreateRootScene(); // create default scene

                // events
                internalEntity.OnSelected += new EventHandler(entity_OnSelected);
                internalEntity.DoubleClick += new EventHandler(entity_DoubleClick);
                ((WidgetBase)internalEntity).PropertyChange += new EventHandler(Canvas_PropertyChange);
                internalEntity.MouseUp += new MouseEventHandler(entity_MouseUp);

                //add to scene collection   
                this.wgCanvas.Widgets.Add(internalEntity);
                _currentScene.AddEntity(internalEntity);
            }

            return entity;
        }

        /// <summary>
        /// Creates a scene on the Canvas
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public SGIScene CreateScene(string text)
        {
            return CreateScene("Scene_" + Utilities.GenId(), text);
        }

        /// <summary>
        /// Create Entity
        /// </summary>
        /// <param name="id"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public SGIScene CreateScene(string id, string text)
        {
            this.ClearEntities();

            SGIScene scene = new SGIScene(text);
            scene.ParentCanvas = this;
            scene.Name = id;
            _currentScene = scene;
            scene.OnVisibilityChangedComplete += new EventHandler(scene_OnVisibilityChangedComplete);
            this.wgCanvas.Widgets.Add(scene);
            _drillPath.Add(new TagInfo(id, text));

            // auto update path bar
            if (_autoUpdateDrillPath)
                this.UpdateDrillPath();

            return scene;
        }

        /// <summary>
        /// Get the SGIEntity object by name 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public SGIEntity GetEntity(string name)
        {
            return this.Entities.Single(i => i.Name == name);
        }

        /// <summary>
        /// Creates a scene on the Canvas
        /// </summary>
        /// <param name="text"></param>
        /// <param name="visible"></param>
        /// <returns></returns>
        public SGIScene CreateScene(string text, bool visible)
        {
            SGIScene scene = CreateScene(text);
            scene.Visible = visible;
            return scene;
        }


        /// <summary>
        /// Creates a connection on of the Canvas
        /// </summary>
        /// <param name="fromEntity"></param>
        /// <param name="toEntity"></param>
        /// <param name="isMouseConnected"></param>
        /// <returns></returns>
        public SGIEntityConnection CreateConnection(SGIEntity fromEntity, SGIEntity toEntity, bool isMouseConnected)
        {
            return CreateConnection("Connection_" + Utilities.GenId(), fromEntity, toEntity, isMouseConnected, false);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromEntity"></param>
        /// <param name="toEntity"></param>
        /// <param name="isMouseConnected"></param>
        /// <param name="canDelete"></param>
        /// <returns></returns>
        public SGIEntityConnection CreateConnection(SGIEntity fromEntity, SGIEntity toEntity, bool isMouseConnected, bool canDelete)
        {
            return CreateConnection("Connection_" + Utilities.GenId(), fromEntity, toEntity, isMouseConnected, canDelete);
        }
        /// <summary>
        /// Creates a connection on of the Canvas
        /// </summary>
        /// <param name="fromEntity"></param>
        /// <param name="toEntity"></param>
        /// <returns></returns>
        public SGIEntityConnection CreateConnection(SGIEntity fromEntity, SGIEntity toEntity)
        {
            return CreateConnection("Connection_" + Utilities.GenId(), fromEntity, toEntity, false, false);
        }

        /// <summary>
        /// Creates a connection on of the Canvas
        /// </summary>
        /// <param name="name"></param>
        /// <param name="fromEntity"></param>
        /// <param name="toEntity"></param>
        /// <param name="isMouseConnected"></param>
        /// <returns></returns>
        public SGIEntityConnection CreateConnection(string name, SGIEntity fromEntity, SGIEntity toEntity, bool isMouseConnected, bool canDelete)
        {
            if (fromEntity != null && toEntity != null)
            {
                //check if Entitys support connections
                //if (!fromEntity.Connectable)
                //    return null;

                //if (!toEntity.Connectable)
                //    return null;

                /* check if the two entities are not already connected */
                /*
                 [from] ---> [to]
                 */
                var connectionFromExist = (from c in this.EntityConnectionList
                                           where (c.FromWidget == fromEntity) && (c.ToWidget == toEntity)
                                           select c).FirstOrDefault();
                if (connectionFromExist != null)
                {
                    SGIEntityConnection connExist = new SGIEntityConnection(fromEntity, toEntity);
                    connExist.IsNewConnection = false;
                    if (isMouseConnected)
                    {
                        //raise connected event
                        if (OnEntityConnected != null)
                        {
                            EntityConnectionEventArgs arg = new EntityConnectionEventArgs(connExist);
                            OnEntityConnected(this, arg);
                            if (arg.Cancel)
                            {
                                return null;
                            }
                        }
                    }
                    return connExist;
                }


                /* check reverse connection */
                /*
                 [from] <--- [to]
                 */
                var connectionToExist = (from c in this.EntityConnectionList
                                         where (c.FromWidget == toEntity) && (c.ToWidget == fromEntity)
                                         select c).FirstOrDefault();
                if (connectionToExist != null)
                {
                    SGIEntityConnection connExist = new SGIEntityConnection(fromEntity, toEntity);
                    connExist.IsNewConnection = false;
                    if (isMouseConnected)
                    {
                        //raise connected event
                        if (OnEntityConnected != null)
                        {
                            EntityConnectionEventArgs arg = new EntityConnectionEventArgs(connExist);
                            OnEntityConnected(this, arg);
                            if (arg.Cancel)
                            {
                                return null;
                            }
                        }
                    }
                    return connExist;
                }


                //create the connection
                SGIEntityConnection connNew = new SGIEntityConnection(fromEntity, toEntity);
                connNew.Name = name;
                connNew.Connected = true;
                connNew.CanDelete = canDelete;

                //connection events
                connNew.OnSelected += new EventHandler(entity_OnSelected);
                ((WidgetBase)connNew).PropertyChange += new EventHandler(Canvas_PropertyChange);

                // send connection back
                connNew.SendToBack();

                // set the scene of the connection
                _currentScene.AddConnection(connNew);

                // add to the canvas
                this.wgCanvas.Widgets.Add(connNew);

                if (isMouseConnected)
                {
                    //raise connected event
                    if (OnEntityConnected != null)
                    {
                        EntityConnectionEventArgs arg = new EntityConnectionEventArgs(connNew);
                        OnEntityConnected(this, arg);
                        if (arg.Cancel)
                        {
                            RemoveConnection(connNew);
                            InvalidateCanvas();
                            connNew = null;
                            return null;
                        }
                    }
                }

                //set connection out on fromEntity
                if (fromEntity.ConnectionOut == null)
                    fromEntity.ConnectionOut = new SGIEntityConnectionCollection();

                fromEntity.ConnectionOut.Add(connNew);

                //set connection in on toEntity
                if (toEntity.ConnectionIn == null)
                    toEntity.ConnectionIn = new SGIEntityConnectionCollection();

                toEntity.ConnectionIn.Add(connNew);

                return connNew;
            }

            return null;

        }

        /// <summary>
        /// Removes an Entity from the Canvas
        /// </summary>
        /// <param name="entity"></param>
        public void RemoveEntity(SGIEntity entity)
        {
            // events
            entity.OnSelected -= new EventHandler(entity_OnSelected);
            entity.DoubleClick -= new EventHandler(entity_DoubleClick);
            ((WidgetBase)entity).PropertyChange -= new EventHandler(Canvas_PropertyChange);
            entity.MouseUp -= new MouseEventHandler(entity_MouseUp);

            this.wgCanvas.Widgets.Remove(entity); //remove entity from canvas
            //entity = null;
        }

        /// <summary>
        /// Removes a scene from the Canvas
        /// </summary>
        /// <param name="scene"></param>
        public void RemoveScene(SGIScene scene)
        {
            scene.OnVisibilityChangedComplete -= new EventHandler(scene_OnVisibilityChangedComplete);

            for (int i = 0; i < _drillPath.Count; i++)
            {
                if (_drillPath[i].Id == scene.Name)
                {
                    _drillPath.RemoveAt(i);
                    break;
                }
            }

            this.wgCanvas.Widgets.Remove(scene);
            scene = null;
            MoveToRootScene();
        }

        /// <summary>
        /// Removes a connection from the Canvas
        /// </summary>
        /// <param name="conn"></param>
        public void RemoveConnection(SGIEntityConnection conn)
        {
            conn.OnSelected -= new EventHandler(entity_OnSelected);
            ((WidgetBase)conn).PropertyChange -= new EventHandler(Canvas_PropertyChange);

            this.wgCanvas.Widgets.Remove(conn); //entity from canvas
            conn = null;
        }
        public void CleanConnection(SGIEntityConnection conn)
        {
            conn.OnSelected -= new EventHandler(entity_OnSelected);
            ((WidgetBase)conn).PropertyChange -= new EventHandler(Canvas_PropertyChange);
        }


        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void wgCanvas_OnClicked(object sender, EventArgs e)
        {
            if (OnWhiteSpaceClick != null)
                OnWhiteSpaceClick(this, EventArgs.Empty);
        }

        /// <summary>
        /// Mouse up event handlers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void entity_MouseUp(object sender, MouseEventArgs e)
        {
            if (OnEntityMouseUp != null)
                OnEntityMouseUp(sender, EventArgs.Empty);
        }

        /// <summary>
        /// Canvas property change event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Canvas_PropertyChange(object sender, EventArgs e)
        {
            if (OnPropertyChange != null)
                OnPropertyChange(sender, EventArgs.Empty);
        }

        /// <summary>
        /// Canvas delete event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void wgCanvas_OnDelete(object sender, EventArgs e)
        {
            if (ReadOnly) return;

            List<Widget> widgets = this.wgCanvas.SelectedObjects;
            MultiDeleteMessageShown = false;
            MultiDeleteCancelled = false;

            if (widgets.Count > 1)
            {
                if (!CanMultiDelete)
                    return;

                MultiDelete = true;
                foreach (var item in widgets)
                {
                    if (item.CanDelete)
                    {
                        DeleteObject(item);
                        MultiDeleteMessageShown = true;
                        if (MultiDeleteCancelled)
                            break;
                    }
                }
            }
            else
            {
                if (((Widget)SelectedObject).CanDelete)
                {
                    MultiDelete = false;
                    DeleteObject(SelectedObject);
                }
            }

            // fires when all selected objects have been deleted
            if (OnDeleteCompleted != null)
                OnDeleteCompleted(this, EventArgs.Empty);
        }

        private void DeleteObject(object selectedObject)
        {
            if (selectedObject != null)
            {
                if (selectedObject is SGIEntity)
                {
                    SGIEntity ent = selectedObject as SGIEntity;
                    if (ent.CanDelete)
                        this.DeleteEntityFromCanvas(ent);
                }
                else if (selectedObject is SGIEntityConnection)
                {
                    SGIEntityConnection conn = selectedObject as SGIEntityConnection;
                    DeleteEntityConnectionFromCavas(conn);
                }
            }
        }

        /// <summary>
        /// <see cref="SGIEntity"/> double click event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void entity_DoubleClick(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// <see cref="SGIEntity"/> selected event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void entity_OnSelected(object sender, EventArgs e)
        {
            this.SelectedObject = sender;

            if (OnEntitySelected != null)
                OnEntitySelected(sender, EventArgs.Empty);
        }

        /// <summary>
        /// <see cref="SGIScene"/> path click event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void scenePath_Click(object sender, EventArgs e)
        {
            TagInfo scene = ((PathBarItem)sender).Tag as TagInfo;

            int cutOffIndex = 0;
            for (int i = 0; i < _drillPath.Count; i++)
            {
                if (_drillPath[i].Id == scene.Id)
                {
                    cutOffIndex = i;
                }
            }

            //remove all scenes after the cut off
            int range = (_drillPath.Count - 1) - cutOffIndex;

            if (range > 0)
                _drillPath.RemoveRange(cutOffIndex + 0, range + 1); // remove two items

            if (OnPathSelected != null)
                OnPathSelected(scene, EventArgs.Empty);
        }

        /// <summary>
        /// <see cref="SGIScene"/> on visibility change event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scene_OnVisibilityChangedComplete(object sender, EventArgs e)
        {
            for (int i = 0; i < wgCanvas.Widgets.List.Count; i++)
            {
                if (wgCanvas.Widgets.List[i] is ResizeConnector)
                {
                    ((ResizeConnector)wgCanvas.Widgets.List[i]).Border = false;
                    ((ResizeConnector)wgCanvas.Widgets.List[i]).BackColor = Color.Transparent;
                    wgCanvas.Widgets.List[i].Visible = false;
                    Invalidate();
                }
            }
        }

        /// <summary>
        /// <see cref="Widget"/> on add event handler
        /// </summary>
        /// <param name="Widget"></param>
        protected void Widgets_AddWidget(IWidget Widget)
        {
            if (Widget is SGIEntity)
            {
                if (OnEntityAdded != null)
                    OnEntityAdded(this, new EntityEventArgs((SGIEntity)Widget));
            }

            if (Widget is SGIEntityConnection)
            {
                EntityConnectionList.Add((SGIEntityConnection)Widget);
            }
        }

        /// <summary>
        /// <see cref="Widget"/> on remove event handler
        /// </summary>
        /// <param name="Widget"></param>
        protected void Widgets_RemovedWidget(IWidget Widget)
        {
            if (Widget is SGIEntity)
            {
                if (OnEntityRemoved != null)
                    OnEntityRemoved(this, new EntityEventArgs((SGIEntity)Widget));
            }

            if (Widget is SGIEntityConnection)
                EntityConnectionList.Remove((SGIEntityConnection)Widget);
        }

        /// <summary>
        /// <see cref="Canvas"/> on connect event handler
        /// </summary>
        /// <param name="srcWidget"></param>
        /// <param name="distWidget"></param>
        protected void wgCanvas_OnConnect(SGI.IWidget srcWidget, SGI.IWidget distWidget)
        {
            SGIEntity el1 = srcWidget as SGIEntity;
            SGIEntity el2 = distWidget as SGIEntity;
            CreateConnection(el1, el2, true);
        }

        #endregion
    }
}
