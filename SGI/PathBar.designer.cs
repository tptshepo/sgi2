﻿namespace SGI
{
    partial class PathBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlPaths = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // pnlPaths
            // 
            this.pnlPaths.BackColor = System.Drawing.Color.Transparent;
            this.pnlPaths.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPaths.Location = new System.Drawing.Point(0, 0);
            this.pnlPaths.Name = "pnlPaths";
            this.pnlPaths.Size = new System.Drawing.Size(412, 24);
            this.pnlPaths.TabIndex = 3;
            // 
            // PathBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::SGI.Properties.Resources.inactive_fill;
            this.Controls.Add(this.pnlPaths);
            this.Name = "PathBar";
            this.Size = new System.Drawing.Size(412, 24);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlPaths;
    }
}
