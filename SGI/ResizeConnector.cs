using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace SGI
{
    /// <summary>
    /// The Connector object is used to resize the <see cref="Widget"/> using the mouse
    /// </summary>
    [Serializable]
    public class ResizeConnector : Widget
    {
        private Widget _parentWidget;

        /// <summary>
        /// Gets or sets the parent control of the Connector
        /// </summary>
        public Widget ParentWidget
        {
            get { return _parentWidget; }
            set { _parentWidget = value; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public ResizeConnector(Widget parent, int x, int y)
            :
            base(x, y, 1, 1)
        {
            this.Shape = Shapes.Rectangle;
            this.BorderColor = Color.Black;
            this.BackColor = Color.FromArgb(0, 255, 0);
            _parentWidget = parent;
        }


        protected override void OnRemoveFromContainer(WidgetCanvas container)
        {
            _parentWidget = null;
            base.OnRemoveFromContainer(container);
        }

        /// <summary>
        /// Fires when the location of the connector changes
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        protected override void OnLocationChanged(int x, int y, int width, int height)
        {
            base.OnLocationChanged(x, y, width, height);
        }

        /// <summary>
        /// Draws the Connector on screen
        /// </summary>
        /// <param name="graphics"></param>
        protected override void Draw(Graphics graphics)
        {
            if (Visible)
                base.Draw(graphics);
        }

    }
}
