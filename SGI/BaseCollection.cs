﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;

namespace SGI
{
    /// <summary>
    /// Base class for all collection classes
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable()]
    public abstract class BaseCollection<T> : IList<T>, ICollection<T>, IEnumerable<T>,
            IList, ICollection
            where T : class, IBaseCollectionItem, new()
    {
        #region Fields

        private List<T> items = new List<T>();

        #endregion

        #region Events
        public event EventHandler ItemAdded;
        public event EventHandler ItemRemoved;
        #endregion

        #region Methods

        #endregion

        #region IList<T> Members
        /// <summary>
        /// Searches for the specified object and returns the zero-based index of the
        /// first occurrence within the entire System.Collections.Generic.List.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int IndexOf(T item)
        {
            return items.IndexOf(item);
        }

        /// <summary>
        ///  Inserts an element into the System.Collections.Generic.List at the specified
        ///  index.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        public void Insert(int index, T item)
        {
            items.Insert(index, item);
            if (ItemAdded != null)
                ItemAdded(item, EventArgs.Empty);
        }

        /// <summary>
        /// Removes the element at the specified index of the System.Collections.Generic.List.
        /// </summary>
        /// <param name="index"></param>
        public void RemoveAt(int index)
        {
            T item = this[index];
            items.RemoveAt(index);
            if (ItemRemoved != null)
                ItemRemoved(item, EventArgs.Empty);
        }

        /// <summary>
        /// Gets or sets an object to the collection
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T this[int index]
        {
            get { return items[index]; }
            set { items[index] = value; }
        }

        #endregion

        #region ICollection<T> Members

        /// <summary>
        /// Adds an object to the end of the System.Collections.Generic.List.
        /// </summary>
        /// <param name="item"></param>
        public virtual void Add(T item)
        {
            items.Add(item);
            if (ItemAdded != null)
                ItemAdded(item, EventArgs.Empty);
        }

        /// <summary>
        /// Removes all elements from the System.Collections.Generic.List.
        /// </summary>
        public void Clear()
        {
            items.Clear();
        }

        /// <summary>
        /// Determines whether an element is in the System.Collections.Generic.List.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(T item)
        {
            return items.Contains(item);
        }

        /// <summary>
        /// Copies the entire System.Collections.Generic.List to a compatible one-dimensional
        /// array, starting at the specified index of the target array.
        /// </summary>
        /// <param name="array"></param>
        /// <param name="arrayIndex"></param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            items.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Gets the number of elements actually contained in the System.Collections.Generic.List.
        /// </summary>
        public int Count
        {
            get { return items.Count; }
        }

        /// <summary>
        /// Determines whether the collection is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the System.Collections.Generic.List.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Remove(T item)
        {
            bool ret = items.Remove(item);
            if (ItemRemoved != null)
                ItemRemoved(item, EventArgs.Empty);
            return ret;
        }

        #endregion

        #region IEnumerable<T> Members

        /// <summary>
        /// Returns an enumerator that iterates through the System.Collections.Generic.List.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<T> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Returns an enumerator that iterates through the System.Collections.Generic.List.
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)items).GetEnumerator();
        }

        #endregion

        #region IList Members

        /// <summary>
        /// Adds an object to the end of the System.Collections.Generic.List.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        int IList.Add(object value)
        {
            Add(value as T);
            return Count - 1;
        }

        /// <summary>
        /// Removes all elements from the System.Collections.Generic.List.
        /// </summary>
        void IList.Clear()
        {
            Clear();
        }

        /// <summary>
        /// Determines whether an element is in the System.Collections.Generic.List.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        bool IList.Contains(object value)
        {
            return Contains(value as T);
        }

        /// <summary>
        /// Searches for the specified object and returns the zero-based index of the
        /// first occurrence within the entire System.Collections.Generic.List.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        int IList.IndexOf(object value)
        {
            return IndexOf(value as T);
        }

        /// <summary>
        /// Inserts an element into the System.Collections.Generic.List at the specified
        /// index.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        void IList.Insert(int index, object value)
        {
            Insert(index, value as T);
        }

        /// <summary>
        /// Determines whether the collection has a fixed size
        /// </summary>
        bool IList.IsFixedSize
        {
            get { return false; }
        }

        /// <summary>
        /// Determines whether the collection is read-only.
        /// </summary>
        bool IList.IsReadOnly
        {
            get { return IsReadOnly; }
        }
        /// <summary>
        /// Removes the first occurrence of a specific object from the System.Collections.Generic.List.
        /// </summary>
        /// <param name="value"></param>
        void IList.Remove(object value)
        {
            Remove(value as T);
        }
        /// <summary>
        /// Removes the first occurrence of a specific object from the System.Collections.Generic.List.
        /// </summary>
        /// <param name="index"></param>
        void IList.RemoveAt(int index)
        {
            RemoveAt(index);
        }

        /// <summary>
        /// Gets or sets an object to the collection
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        object IList.this[int index]
        {
            get { return this[index]; }
            set { this[index] = value as T; }
        }

        #endregion

        #region ICollection Members

        void ICollection.CopyTo(Array array, int index)
        {
        }

        int ICollection.Count
        {
            get { return Count; }
        }

        bool ICollection.IsSynchronized
        {
            get { return false; }
        }

        object ICollection.SyncRoot
        {
            get { return null; }
        }

        #endregion
    }
}
