﻿namespace SGI
{
    partial class Canvas
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Canvas));
            this.wgCanvas = new SGI.WidgetCanvas();
            this.pathBar = new SGI.PathBar();
            this.SuspendLayout();
            // 
            // wgCanvas
            // 
            this.wgCanvas.AutoScroll = false;
            this.wgCanvas.BackColor = System.Drawing.Color.White;
            this.wgCanvas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wgCanvas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wgCanvas.Location = new System.Drawing.Point(0, 24);
            this.wgCanvas.MultiSelectSupported = false;
            this.wgCanvas.Name = "wgCanvas";
            this.wgCanvas.ScrollGap = 5;
            this.wgCanvas.ShowGrid = false;
            this.wgCanvas.Size = new System.Drawing.Size(344, 201);
            this.wgCanvas.SupportAutoScroll = true;
            this.wgCanvas.TabIndex = 7;
            this.wgCanvas.OnConnect += new SGI.WidgetCanvas.ConnectWidgetEventHandler(this.wgCanvas_OnConnect);
            // 
            // pathBar
            // 
            this.pathBar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pathBar.BackgroundImage")));
            this.pathBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pathBar.Location = new System.Drawing.Point(0, 0);
            this.pathBar.Name = "pathBar";
            this.pathBar.Size = new System.Drawing.Size(344, 24);
            this.pathBar.TabIndex = 8;
            // 
            // Canvas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.wgCanvas);
            this.Controls.Add(this.pathBar);
            this.Name = "Canvas";
            this.Size = new System.Drawing.Size(344, 225);
            this.ResumeLayout(false);

        }

        #endregion

        private SGI.WidgetCanvas wgCanvas;
        private PathBar pathBar;
    }
}
