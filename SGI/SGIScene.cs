﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SGI;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using System.Xml.Linq;

namespace SGI
{

    /// <summary>
    /// The scene widget is used to host other widgets like a window
    /// </summary>
    [Serializable()]
    public class SGIScene : Widget, IExportable
    {
        private string _guidId = string.Empty;
        private int _columns = 0;

        /// <summary>
        /// This event will fire when the visibility of the <see cref="SGIScene"/> changes
        /// </summary>
        public event EventHandler OnVisibilityChangedComplete;

        /// <summary>
        /// Gets or sets the entity that ownes this scene
        /// </summary>
        public SGIEntity OwnerEntity { get; set; }

        /// <summary>
        /// Gets or sets the parent <see cref="Canvas"/> that this <see cref="SGIScene"/> belongs to
        /// </summary>
        public Canvas ParentCanvas { get; set; }

        /// <summary>
        /// Gets or sets the guid id
        /// </summary>
        public string GuidId
        {
            get { return _guidId; }
            set { _guidId = value; }
        }

        /// <summary>
        /// Gets a list of entities that belong to this scene
        /// </summary>
        public IEnumerable<SGIEntity> Entities
        {
            get
            {
                var list = from Widget ent in this.ParentCanvas.Items.List
                           where (ent is SGIEntity) && (((SGIEntity)ent).ContainerScene == this)
                           select ent;

                var ents = from SGIEntity ls in list select ls;
                return ents;
            }
        }

        /// <summary>
        /// Gets a list of connections that belong to this scene
        /// </summary>
        public IEnumerable<SGIEntityConnection> Connections
        {
            get
            {
                var list = from Widget ent in this.ParentCanvas.Items.List
                           where (ent is SGIEntityConnection) && (((SGIEntityConnection)ent).ContainerScene == this)
                           select ent;

                var conns = from SGIEntityConnection ls in list select ls;
                return conns;
            }
        }

        /// <summary>
        /// Gets a list of connected entities
        /// </summary>
        public IEnumerable<SGIEntity> ConnectedEntities
        {
            get
            {
                var list = from ent in Entities
                           where ((ent.ConnectionIn.Count > 0) || (ent.ConnectionOut.Count > 0))
                           select ent;
                return list;
            }
        }

        /// <summary>
        /// Get non connection entities
        /// </summary>
        public IEnumerable<SGIEntity> NonConnectionEntities
        {
            get
            {
                var list = from ent in Entities
                           where ((ent.ConnectionIn.Count == 0) && (ent.ConnectionOut.Count == 0))
                           select ent;

                return list;
            }
        }

        /// <summary>
        /// Sets the scene transparent
        /// </summary>
        /// <param name="backcolor"></param>
        public void SetAsTransparent(Color backcolor)
        {
            this.Border = false;
            this.BackColor = backcolor;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public SGIScene()
            : base(10, 10, 1, 1)
        {
            this._guidId = System.Guid.NewGuid().ToString();
            this.ShowText = false;
            this.Shape = Shapes.Rectangle;

            //blue theme
            this.BackColor = Color.FromArgb(64, 64, 64);
            this.BorderColor = Color.Gray;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="text"></param>
        public SGIScene(string text)
            : this()
        {
            this.Text = text;
        }

        /// <summary>
        /// Gets the string value of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Text;
        }

        /// <summary>
        /// Returns points relative to the Scene's location
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Point PointToClient(Point p)
        {
            return new Point(this.X + p.X, this.Y + p.Y);
        }

        /// <summary>
        /// Clears the selection of<see cref="SGIEntity"/>
        /// </summary>
        public void ClearSelection()
        {
            //clear connection dependancies
            foreach (var ent in Entities)
            {
                ISelectable sel = ent as ISelectable;
                if (sel != null)
                    sel.UnSelect();
            }

            foreach (var ent in Connections)
            {
                ISelectable sel = ent as ISelectable;
                if (sel != null)
                    sel.UnSelect();
            }
        }

        /// <summary>
        /// Auto resize <see cref="SGIEntity"/> to the size of the text
        /// </summary>
        private void AutoResizeEntities()
        {
            foreach (var ent in Entities)
            {
                ent.Width = ent.MinWidth;
            }
        }

        /// <summary>
        /// Align <see cref="SGIEntity"/>
        /// </summary>
        private void AlignEntities(bool fixColumns)
        {
            int startX = this.X + 30; //starting gab
            int startY = this.Y + 20;

            int colCount = 0;

            _columns = 1;
            SGIEntity[] entityList = NonConnectionEntities.ToArray();
            for (int i = 0; i < entityList.Count(); i++)
            {
                // create a new row
                if (colCount == _columns) // check for row cut-off's
                {
                    startX = this.X + 30;
                    startY += entityList[i].Height + 30;
                    colCount = 0;
                }

                // position the entity with the new positions
                entityList[i].X = startX; //set left
                entityList[i].Y = startY; //set top

                startX += entityList[i].Width + 30; //move to left

                colCount++;

                if (!fixColumns)
                {
                    //check if the entity does not overlap on the container
                    if (entityList[i].X + entityList[i].Width + 10 > this.Parent.Control.Width)
                    {
                        //start a new row
                        _columns = colCount;
                        i--; // fix the last entity
                    }
                    else
                        _columns++; //inclement the number of columns allowed
                }
            }

            this.Parent.Control.Invalidate();
        }

        /// <summary>
        /// Aligns entities into a single line order
        /// </summary>
        public virtual void AlignToGrid(bool fixColumn)
        {
            if (Entities.Count() == 0)
                return;

            AutoResizeEntities();

            // get the bigets width
            int maxWidth = Entities.Max(ent => ent.Width);

            // set the width to the other entities
            foreach (SGIEntity ent in Entities)
            {
                ent.Width = maxWidth;
            }

            AlignEntities(fixColumn);

            this.Parent.Control.Invalidate();
        }

        /// <summary>
        /// Clears all entities in the scene
        /// </summary>
        public void ClearAll()
        {
            int countEnt = Entities.Count();
            int counter = 0;

            while (counter < countEnt)
            {
                foreach (var ent in Entities)
                {
                    this.ParentCanvas.RemoveEntity(ent);
                    break;
                }

                counter++;
            }

            countEnt = Connections.Count();
            counter = 0;
            while (counter < countEnt)
            {
                foreach (var conn in Connections)
                {
                    this.ParentCanvas.RemoveConnection(conn);
                    break;
                }
                counter++;
            }
        }

        /// <summary>
        /// Clear all the cconnections
        /// </summary>
        public void ClearConnections()
        {
            int countEnt = Connections.Count();
            int counter = 0;
            while (counter < countEnt)
            {
                foreach (var conn in Connections)
                {
                    this.ParentCanvas.RemoveConnection(conn);
                    break;
                }
                counter++;
            }
        }

        /// <summary>
        /// Fires when the location and size of this object changes
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        protected override void OnLocationChanged(int x, int y, int width, int height)
        {
            base.OnLocationChanged(x, y, width, height);
        }

        /// <summary>
        /// Fires when the view is added to the container
        /// </summary>
        /// <param name="container"></param>
        protected override void OnAddToContainer(WidgetCanvas container)
        {
            base.OnAddToContainer(container);
        }

        /// <summary>
        /// Fires when the widget is removed from the container
        /// </summary>
        /// <param name="container"></param>
        protected override void OnRemoveFromContainer(WidgetCanvas container)
        {
            ClearAll();

            ParentCanvas = null;

            base.OnRemoveFromContainer(container);
        }

        /// <summary>
        /// Fires when the Scene's visibility changes
        /// </summary>
        /// <param name="visible"></param>
        protected override void OnVisibilityChanged(bool visible)
        {
            // change the visibility of the sub items
            foreach (var ent in Entities)
            {
                foreach (var conn in ent.ConnectionOut)
                {
                    conn.Visible = visible;
                }
                ent.Visible = visible;
            }

            ((WidgetCanvas)this.Parent).Invalidate();

            base.OnVisibilityChanged(visible);

            if (OnVisibilityChangedComplete != null)
                OnVisibilityChangedComplete(this, EventArgs.Empty);
        }

        /// <summary>
        /// Fires when the mouse is up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void widget_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            ((SGIEntity)sender).OriginalPoint = new Point(((SGIEntity)sender).X - this.X, ((SGIEntity)sender).Y - this.Y);
        }

        #region IContainerWidget Members

        /// <summary>
        /// Adds an Entity to the scene
        /// </summary>
        /// <param name="entity"></param>
        internal void AddEntity(SGIEntity entity)
        {
            entity.Visible = this.Visible;
            entity.MouseUp += new System.Windows.Forms.MouseEventHandler(widget_MouseUp);
            entity.ContainerScene = this;
        }

        /// <summary>
        /// Add connection to scene
        /// </summary>
        /// <param name="conn"></param>
        internal void AddConnection(SGIEntityConnection conn)
        {
            conn.Visible = this.Visible;
            conn.ContainerScene = this;
        }

        #endregion

        #region IExportable Members

        /// <summary>
        /// Get the list of attributes to export
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> Attributes()
        {
            Dictionary<string, string> att = new Dictionary<string, string>();

            att.Add("Name", this.Name);
            att.Add("Text", this.Text);
            att.Add("Owner", this.OwnerEntity == null ? "" : this.OwnerEntity.Name);

            return att;
        }

        /// <summary>
        /// Gets if the object can export
        /// </summary>
        /// <returns></returns>
        public bool CanExport()
        {
            return true;
        }

        #endregion
    }

}
