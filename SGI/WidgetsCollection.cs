﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SGI
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable()]
    public class WidgetsCollection
    {
        private WidgetCanvas _owner;
        private WidgetCollection _collection;

        #region Events
        /// <summary>
        /// IWidget delegate
        /// </summary>
        /// <param name="Widget"></param>
        public delegate void WidgetEventArgs(IWidget Widget);
        /// <summary>
        /// Remove widget event
        /// </summary>
        public event WidgetEventArgs RemovedWidget;
        /// <summary>
        /// Add widget event
        /// </summary>
        public event WidgetEventArgs AddWidget;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public WidgetCollection List
        {
            get { return _collection; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="owner"></param>
        public WidgetsCollection(WidgetCanvas owner)
        {
            _owner = owner;
            _collection = new WidgetCollection();
            _collection.ItemAdded += new EventHandler(_collection_ItemAdded);
            _collection.ItemRemoved += new EventHandler(_collection_ItemRemoved);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _collection_ItemRemoved(object sender, EventArgs e)
        {
            if (RemovedWidget != null)
                RemovedWidget((IWidget)sender);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _collection_ItemAdded(object sender, EventArgs e)
        {
            if (AddWidget != null)
                AddWidget((IWidget)sender);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public void Add(Widget widget)
        {
            IWidget iWidget = (IWidget)widget;

            _collection.Add(widget);

            iWidget.Parent = _owner;
            iWidget.OnAddToContainer(_owner);
            _owner.UpdateScroller();
            _owner.Invalidate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public void BringToFront(Widget widget)
        {
            int index = _collection.IndexOf(widget);

            if (index == -1)
                return;

            if (index == _collection.Count - 1)
                return;

            IWidget temp = (IWidget)_collection[index];
            temp.Invalidate(0, 0, temp.Width, temp.Height);
            _collection.RemoveAt(index);
            _collection.Insert(_collection.Count - 1, widget);
            widget.Invalidate(0, 0, widget.Width, widget.Height);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Clear()
        {
            _owner.BeginUpdate();
            try
            {
                for (int i = 0; i < _collection.Count; i++)
                {
                    i = 0;
                    _collection.RemoveAt(i);
                }
            }
            finally
            {
                _owner.EndUpdate();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="widget"></param>
        public void Insert(int index, Widget widget)
        {
            IWidget iWidget = (IWidget)widget;

            _collection.Insert(index, widget);

            iWidget.Parent = _owner;
            iWidget.OnAddToContainer(_owner);
            _owner.UpdateScroller();
            iWidget.Invalidate(0, 0, widget.Width, widget.Height);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="widget"></param>
        public void Remove(Widget widget)
        {
            if (widget == null)
                return;

            IWidget iWidget = (IWidget)widget;

            _collection.Remove(widget);

            iWidget.Parent = NullControlService.Instance;
            iWidget.OnRemoveFromContainer(_owner);
            iWidget.Dispose();
            _owner.UpdateScroller();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        public void RemoveAt(int index)
        {
            Widget widget = _collection[index];

            if (widget == null)
                return;

            this.Remove(widget);
        }
    }
}
