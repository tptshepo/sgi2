using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;

namespace SGI
{
    /// <summary>
    /// Represents an abstract light Widget.
    /// </summary>
    /// <remarks>
    /// This class implements the <see cref="IWidget"/> interface and provides
    /// facilities that help the developer to implement various Widgets.
    /// </remarks>
    [Serializable]
    public abstract class WidgetBase : IWidget
    {
        #region Fields
        private int _x;
        private int _y;
        private int _width;
        private int _height;
        private bool _dragging = false;
        private ContextMenuStrip _contextMenuStrip;
        private IControlService _parent;
        private bool _visible = true;
        private bool _enabled = true;
        private bool _zOrder = false;
        private bool _disposing = false;
        private string _fontName = "Arial";
        private float _fontSize = 8f;
        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler PropertyChange;

        /// <summary>
        /// Gets or sets the font name
        /// </summary>
        public string FontName
        {
            get { return _fontName; }
            set { _fontName = value; }
        }

        /// <summary>
        /// Gets or sets the font size
        /// </summary>
        public float FontSize
        {
            get { return _fontSize; }
            set { _fontSize = value; }
        }

        /// <summary>
        /// Gets or sets a parent object that provides support for
        /// <see cref="IControlService"/>.
        /// </summary>
        [Browsable(false)]
        public IControlService Parent
        {
            get
            {
                return _parent;
            }

            set
            {
                if (null == value)
                    _parent = NullControlService.Instance;
                else
                {
                    if (_parent != value)
                    {
                        _parent = value;
                        OnPropertyChange();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the horizontal coordinate of the Widget relative to it's
        /// parent object.
        /// </summary>
        public int X
        {
            get
            {
                return _x;
            }

            set
            {
                if (_x != value)
                {
                    Invalidate();
                    _x = value;
                    Invalidate();
                    this.OnLocationChanged(_x, _y, _width, _height);
                    OnPropertyChange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the vertical coordinate of the Widget relative to it's
        /// parent object.
        /// </summary>
        public int Y
        {
            get
            {
                return _y;
            }

            set
            {
                if (_y != value)
                {
                    Invalidate();
                    _y = value;
                    Invalidate();
                    this.OnLocationChanged(_x, _y, _width, _height);
                    OnPropertyChange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the width of the Widget.
        /// </summary>
        public int Width
        {
            get
            {
                return _width;
            }

            set
            {
                if (_width != value)
                {
                    //Invalidate();
                    _width = value;
                    Invalidate();
                    this.OnLocationChanged(_x, _y, _width, _height);
                    OnPropertyChange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the height of the Widget.
        /// </summary>
        public int Height
        {
            get
            {
                return _height;
            }

            set
            {
                if (_height != value)
                {
                    //Invalidate();
                    _height = value;
                    Invalidate();
                    this.OnLocationChanged(_x, _y, _width, _height);
                    OnPropertyChange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the dragging state.
        /// </summary>
        [Browsable(false)]
        public bool Dragging
        {
            get
            {
                return _dragging;
            }

            set
            {
                if (_dragging != value)
                {
                    _dragging = value;
                    OnPropertyChange();
                }
            }
        }

        /// <summary>
        ///Gets or sets the System.Windows.Forms.ContextMenuStrip associated with this control
        /// </summary>
        [DefaultValue("")]
        public virtual ContextMenuStrip ContextMenuStrip
        {
            get { return _contextMenuStrip; }
            set
            {
                if (_contextMenuStrip != value)
                {
                    _contextMenuStrip = value;
                    OnPropertyChange();
                }
            }
        }

        /// <summary>
        /// Enables or disables the Widget.
        /// </summary>
        [Browsable(false)]
        public bool Enabled
        {
            get
            {
                return _enabled;
            }

            set
            {
                if (_enabled != value)
                {
                    _enabled = value;
                    Invalidate();
                    this.OnEnabilityChanged(value);
                    OnPropertyChange();
                }
            }
        }

        /// <summary>
        /// Shows or hides the Widget.
        /// </summary>
        [Browsable(false)]
        public bool Visible
        {
            get
            {
                return _visible;
            }

            set
            {
                if (_visible != value)
                {
                    _visible = value;
                    Invalidate();
                    this.OnVisibilityChanged(value);
                    OnPropertyChange();
                }
            }
        }

        /// <summary>
        /// Gets the Widget location - it's top left corner coordinates.
        /// </summary>
        [Browsable(false)]
        public virtual Point Location
        {
            get
            {
                return new Point(X, Y);
            }
            set
            {
                if (X != value.X)
                    X = value.X;

                if (Y != value.Y)
                    Y = value.Y;
            }
        }

        /// <summary>
        /// Gets the Widget size.
        /// </summary>
        [Browsable(false)]
        public virtual Size Size
        {
            get
            {
                return new Size(Width, Height);
            }
            set
            {
                if (Width != value.Width)
                    Width = value.Width;

                if (Height != value.Height)
                    Height = value.Height;
            }
        }

        /// <summary>
        /// Gets the bounds of the Widget relative to it's parent object.
        /// </summary>
        [Browsable(false)]
        public virtual Rectangle Bounds
        {
            get
            {
                return new Rectangle(0, 0, Width, Height);
            }
        }

        /// <summary>
        /// Moves the widget up in the zorder
        /// </summary>
        [Description("Moves a the widget up in z-order of widgets"), Category("Drawing")]
        [Browsable(false)]
        public bool ZOrder
        {
            get { return _zOrder; }
            set
            {
                if (_zOrder != value)
                {
                    _zOrder = value;
                }
            }
        }
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Widget"/> class with it's
        /// desired size and location.
        /// </summary>
        protected WidgetBase()
        {
            this._parent = NullControlService.Instance;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Widget"/> class with it's
        /// desired size and location.
        /// </summary>
        /// <param name="x">The horizontal coordinate of the Widget relative to
        /// it's parent object.</param>
        /// <param name="y">The vertical coordinate of the Widget relative to
        /// it's parent object.</param>
        /// <param name="width">The width of the Widget.</param>
        /// <param name="height">The height of the Widget.</param>
        protected WidgetBase(int x, int y, int width, int height)
            : this()
        {
            this._x = x;
            this._y = y;
            this._height = height;
            this._width = width;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Fires when a property changes
        /// </summary>
        public void OnPropertyChange()
        {
            if (PropertyChange != null)
                PropertyChange(this, EventArgs.Empty);
        }

        /// <summary>
        /// Invalidates the Widget's contents.
        /// </summary>
        public void Invalidate()
        {
            Invalidate(-2, -2, Width + 4, Height + 4);
        }

        /// <summary>
        /// Invalidates a rectangular area specified in the Widget's cooridinates.
        /// </summary>
        /// <param name="x">
        /// The x-coordinate of the upper-left corner of the Widget.
        /// </param>
        /// <param name="y">
        /// The y-coordinate of the upper-left corner of the Widget.
        /// </param>
        /// <param name="width">
        /// The width of the Widget.
        /// </param>
        /// <param name="height">
        /// The height of the Widget.
        /// </param>
        public virtual void Invalidate(int x, int y, int width, int height)
        {
            if (Parent == null)
                return;

            if ((null == Parent.Control) || !Parent.Control.Created)
                return;
            Parent.InvalidateRectangle(x + X, y + Y, width, height);
        }

        /// <summary>
        /// Override this method in derived classes to paint the Widgets.
        /// </summary>
        /// <param name="graphics">An instance of the <see cref="Graphics"/>
        /// object to paint on.</param>
        /// <remarks>
        /// A Widget can be non-rectangular.
        /// </remarks>
        protected abstract void Draw(Graphics graphics);
        void IWidget.Draw(Graphics graphics)
        {
            this.Draw(graphics);
        }

        /// <summary>
        /// Mouse down notification callback.
        /// </summary>
        /// <param name="posX">
        /// The horizontal position of the mouse.
        /// </param>
        /// <param name="posY">
        /// The vertical position of the mouse.
        /// </param>
        /// <param name="buttons">
        /// A <see cref="MouseButtons"/> that describes the mouse buttons
        /// pressed.
        /// </param>
        protected virtual void OnMouseDown(int posX, int posY, MouseButtons buttons)
        { }
        void IWidget.OnMouseDown(int posX, int posY, MouseButtons buttons)
        {
            this.OnMouseDown(posX, posY, buttons);
        }

        /// <summary>
        /// Fires when the widget is added to the container
        /// </summary>
        /// <param name="container"></param>
        protected virtual void OnAddToContainer(WidgetCanvas container)
        { }
        void IWidget.OnAddToContainer(WidgetCanvas container)
        {
            this.OnAddToContainer(container);
        }

        /// <summary>
        /// Fires when the widget is removed from the container
        /// </summary>
        protected virtual void OnRemoveFromContainer(WidgetCanvas container)
        {
            this._contextMenuStrip = null;
            this._parent = null;
        }
        void IWidget.OnRemoveFromContainer(WidgetCanvas container)
        {
            this.OnRemoveFromContainer(container);
        }

        /// <summary>
        /// Fires the double click event
        /// </summary>
        protected virtual void OnDoubleClick()
        { }
        void IWidget.OnDoubleClick()
        {
            this.OnDoubleClick();
        }

        /// <summary>
        /// Mouse up notification callback.
        /// </summary>
        /// <param name="posX">
        /// The horizontal position of the mouse.
        /// </param>
        /// <param name="posY">
        /// The vertical position of the mouse.
        /// </param>
        /// <param name="buttons">
        /// A <see cref="MouseButtons"/> that describes the mouse buttons
        /// pressed.
        /// </param>
        protected virtual void OnMouseUp(int posX, int posY, MouseButtons buttons)
        { }
        void IWidget.OnMouseUp(int posX, int posY, MouseButtons buttons)
        {
            this.OnMouseUp(posX, posY, buttons);
        }

        /// <summary>
        /// Mouse entered into the Widget's bounds notification callback.
        /// </summary>
        /// <param name="posX">The x-coordinate of the mouse.</param>
        /// <param name="posY">The y-coordinate of the mouse.</param>
        protected virtual void OnMouseEnter(int posX, int posY)
        { }
        void IWidget.OnMouseEnter(int posX, int posY)
        {
            this.OnMouseEnter(posX, posY);
        }

        /// <summary>
        /// Mouse left the Widget's bounds notification callback.
        /// </summary>
        /// <param name="posX">The x-coordinate of the mouse.</param>
        /// <param name="posY">The y-coordinate of the mouse.</param>
        protected virtual void OnMouseLeave(int posX, int posY)
        { }
        void IWidget.OnMouseLeave(int posX, int posY)
        {
            this.OnMouseLeave(posX, posY);
        }

        /// <summary>
        /// Mouse is being moved in the Widget's bounds notification callback.
        /// </summary>
        /// <param name="posX">The x-coordinate of the mouse.</param>
        /// <param name="posY">The y-coordinate of the mouse.</param>
        protected virtual void OnMouseHover(int posX, int posY)
        { }
        void IWidget.OnMouseHover(int posX, int posY)
        {
            this.OnMouseHover(posX, posY);
        }

        /// <summary>
        /// Mouse is being moved in the Widget's bounds notification callback.
        /// </summary>
        /// <param name="posX">The x-coordinate of the mouse.</param>
        /// <param name="posY">The y-coordinate of the mouse.</param>
        protected virtual void OnMouseMove(int posX, int posY)
        { }
        void IWidget.OnMouseMove(int posX, int posY)
        {
            this.OnMouseMove(posX, posY);
        }

        /// <summary>
        /// Fires when the location and size changes
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        protected virtual void OnLocationChanged(int x, int y, int width, int height)
        { }

        /// <summary>
        /// Fires when the visibility of the widget changes
        /// </summary>
        /// <param name="visible"></param>
        protected virtual void OnVisibilityChanged(bool visible) { }

        /// <summary>
        /// Fires when the enability of the widget changes
        /// </summary>
        /// <param name="visible"></param>
        protected virtual void OnEnabilityChanged(bool visible) { }

        /// <summary>
        /// Checks if the mouse is in the Widget's bounds.
        /// </summary>
        /// <param name="posX">The x-coordinate of the mouse.</param>
        /// <param name="posY">The y-coordinate of the mouse.</param>
        protected virtual bool HitTest(int posX, int posY)
        {
            if ((X <= posX) && (X + Width > posX) && (Y <= posY) && (Y + Height > posY))
                return true;
            return false;
        }

        /// <summary>
        /// Checks if the mouse has hit the object
        /// </summary>
        /// <param name="posX"></param>
        /// <param name="posY"></param>
        /// <returns></returns>
        bool IWidget.HitTest(int posX, int posY)
        {
            return this.HitTest(posX, posY);
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void Dispose()
        {
            _disposing = true;
        }

        /// <summary>
        /// Free memory
        /// </summary>
        public bool Disposing
        {
            get { return _disposing; }
        }
        #endregion

    }
}
