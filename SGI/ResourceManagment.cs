using System;
using System.Globalization;
using System.Reflection;
using System.Resources;

namespace SGI
{
    /// <summary>
    /// Resource dispenser.
    /// </summary>

    public class ResourceManagment
    {
        /// <summary>
        /// Gets a string from the ResourceManager
        /// </summary>
        /// <param name="resourceId"></param>
        /// <returns></returns>
        public static string GetString(string resourceId)
        {
            ResourceManager rm = new ResourceManager("strings", Assembly.GetExecutingAssembly());     
            return rm.GetString(resourceId);
        }
    }
}
