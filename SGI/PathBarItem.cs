﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SGI
{
    [DefaultEvent("Click"), ToolboxItem(false)]
    public partial class PathBarItem : UserControl
    {
        private string _text;

        [Browsable(true)]
        public override string Text
        {
            get
            {
                return _text;
            }
            set
            {
                toolTip1.SetToolTip(lblText, _text);
                lblText.Text = _text;
                _text = value;
            }
        }

        public override string ToString()
        {
            return _text;
        }

        public PathBarItem()
        {
            InitializeComponent();
            SetStyle(Styles.Inactive);
        }

        public PathBarItem(string text)
            : this()
        {
            _text = text;
            lblText.Text = text;
            toolTip1.SetToolTip(lblText, text);
        }

        public enum Styles { OnlyInactive, OnlyActive, Inactive, InactiveNextActive, Active };

        public void DisposeObjects()
        {
            toolTip1.Dispose();
            toolTip1 = null;
            pnlLeft.BackgroundImage.Dispose();
            pnlLeft.BackgroundImage = null;
            pnlFill.BackgroundImage.Dispose();
            pnlFill.BackgroundImage = null;
            pnlRight.BackgroundImage.Dispose();
            pnlRight.BackgroundImage = null;
        }

        public void SetStyle(Styles style)
        {
            if (style == Styles.Inactive)
            {
                pnlLeft.BackgroundImage = global::SGI.Properties.Resources.inactive_fill;
                pnlFill.BackgroundImage = global::SGI.Properties.Resources.inactive_fill;
                pnlRight.BackgroundImage = global::SGI.Properties.Resources.right_inactive;
                pnlRight.Width = 3;
                return;
            }

            if (style == Styles.InactiveNextActive)
            {
                pnlLeft.BackgroundImage = global::SGI.Properties.Resources.inactive_fill;
                pnlFill.BackgroundImage = global::SGI.Properties.Resources.inactive_fill;
                pnlRight.BackgroundImage = global::SGI.Properties.Resources.right_inactive_with_next_active;
                pnlRight.Width = 13;
                return;
            }

            if (style == Styles.Active)
            {
                pnlLeft.BackgroundImage = global::SGI.Properties.Resources.active_fill;
                pnlFill.BackgroundImage = global::SGI.Properties.Resources.active_fill;
                pnlRight.BackgroundImage = global::SGI.Properties.Resources.right_active;
                pnlRight.Width = 3;
                return;
            }

            if (style == Styles.OnlyActive)
            {
                pnlLeft.BackgroundImage = global::SGI.Properties.Resources.active_fill;
                pnlFill.BackgroundImage = global::SGI.Properties.Resources.active_fill;
                pnlRight.BackgroundImage = global::SGI.Properties.Resources.right_active;
                pnlRight.Width = 3;
                return;
            }

            if (style == Styles.OnlyInactive)
            {
                pnlLeft.BackgroundImage = global::SGI.Properties.Resources.inactive_fill;
                pnlFill.BackgroundImage = global::SGI.Properties.Resources.inactive_fill;
                pnlRight.BackgroundImage = global::SGI.Properties.Resources.right_inactive;
                pnlRight.Width = 3;
                return;
            }
        }

        private void lblText_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
        }

    }
}
