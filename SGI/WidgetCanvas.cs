using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;

namespace SGI
{
  /// <summary>
  /// Represents a container control for light Widgets.
  /// </summary>
  [Designer(typeof(WidgetCanvasDesigner)), ToolboxItem(false)]
  public class WidgetCanvas : Panel, IControlService
  {
    #region Fields

    private bool _showScrollbars = true;

    //tracking
    private WidgetsCollection _widgets;
    private IWidget _capturingWidget;
    private IWidget _currentTrackingWidget;
    private bool _updating;
    private Point _mouseLocation;

    // draw line
    private IWidget _dragLineCurrentTrackingWidget; //tracks the current widget the connecting line is over
    private bool _drawingReverseLine = false;
    private Point _originalPoint = new Point();
    private Point _lastPoint = new Point();

    // draw Rectangle
    private bool _drawingReverseRect = false;
    private Point _originalRectPoint = new Point();
    private Point _lastRectPoint = new Point();

    //used for connecting two widgets together
    private IWidget _sourceWidget;
    private IWidget _destinationWidget;

    //scrolling
    private int _scrollGap = 50;
    private bool _supportAutoScroll = true;

    //back color
    private Color _backColor1 = Color.White;
    private Color _backColor2 = Color.White;

    //selection
    private bool _multiSelectSupported = false;
    Rectangle _selectionArea = new Rectangle();
    private List<KeyValuePair<IWidget, Point>> _selectedWidgetList;
    private bool _moveMultipleWidgets = false;
    private int _oldX = 0;
    private int _oldY = 0;
    private IWidget _dragWidget;
    private bool _selectionDragging = false;
    private bool _multiSelecting = false;
    private bool _showGrid = false;


    Rectangle _dragOverlayRect = new Rectangle();
    private int _dragOverlayLeft = 0;
    private int _dragOverlayTop = 0;
    private int _dragOverlayRight = 0;
    private int _dragOverlayBottom = 0;

    #endregion

    #region Events

    /// <summary>
    /// Widget event delegate
    /// </summary>
    /// <param name="Widget"></param>
    public delegate void WidgetEventHandler(IWidget Widget);

    /// <summary>
    /// Connects two widgets together
    /// </summary>
    /// <param name="srcWidget"></param>
    /// <param name="distWidget"></param>
    public delegate void ConnectWidgetEventHandler(IWidget srcWidget, IWidget distWidget);

    /// <summary>
    /// This event will fire when we click on the white space of the canvas
    /// </summary>
    public event EventHandler OnClicked;

    /// <summary>
    /// Fires when connecting two widgets together
    /// </summary>
    public event ConnectWidgetEventHandler OnConnect;

    /// <summary>
    /// Fires when a widget is deleted
    /// </summary>
    public event EventHandler OnDelete;

    #endregion

    #region Constructor

    /// <summary>
    /// Initializes a new instance of the <see cref="WidgetCanvas"/>
    /// control.
    /// </summary>
    public WidgetCanvas()
    {
      SetStyle(ControlStyles.Opaque, true);
      SetStyle(ControlStyles.Selectable, true);
      SetStyle(ControlStyles.AllPaintingInWmPaint, true);
      SetStyle(ControlStyles.UserPaint, true);
      SetStyle(ControlStyles.UserMouse, true);
      SetStyle(ControlStyles.DoubleBuffer, true);

      BorderStyle = BorderStyle.Fixed3D;
      _widgets = new WidgetsCollection(this);
      _selectedWidgetList = new List<KeyValuePair<IWidget, Point>>();
    }

    #endregion

    #region Properties

    public int DragOverlayLeft
    {
      get { return _dragOverlayLeft; }
    }

    public int DragOverlayTop
    {
      get { return _dragOverlayTop; }
    }

    public int DragOverlayRight
    {
      get { return _dragOverlayRight; }
    }

    public int DragOverlayBottom
    {
      get { return _dragOverlayBottom; }
    }

    /// <summary>
    /// Gets the mouse location
    /// </summary>
    public Point MouseLocation
    {
      get { return _mouseLocation; }
    }

    /// <summary>
    /// Gets is the canvas is about to select all widgets
    /// </summary>
    public bool MultiSelecting
    {
      get { return _multiSelecting; }
    }

    /// <summary>
    /// Gets if more than one widget is selected
    /// </summary>
    public bool MultipleSelected
    {
      get { return _selectedWidgetList.Count > 1; }
    }

    /// <summary>
    /// Gets or sets if we should show the grid lines
    /// </summary>
    public bool ShowGrid
    {
      get { return _showGrid; }
      set
      {
        _showGrid = value;
        Invalidate();
      }
    }

    /// <summary>
    /// Gets a list of selected widgets
    /// </summary>
    public virtual List<Widget> SelectedObjects
    {
      get
      {
        List<Widget> list = new List<Widget>();
        foreach (var item in _selectedWidgetList)
        {
          list.Add((Widget)item.Key);
        }
        return list;
      }
    }

    /// <summary>
    /// Gets or sets if multiple widget selection is supported
    /// </summary>
    public bool MultiSelectSupported
    {
      get { return _multiSelectSupported; }
      set { _multiSelectSupported = value; }
    }

    /// <summary>
    /// Gets or sets is auto scrolling is supported
    /// </summary>
    public bool SupportAutoScroll
    {
      get { return _supportAutoScroll; }
      set { _supportAutoScroll = value; }
    }

    /// <summary>
    /// Gets a collection of all light Widgets that are handled by
    /// the <see cref="WidgetCanvas"/> control.
    /// </summary>
    public virtual WidgetsCollection Widgets
    {
      get
      {
        return _widgets;
      }
    }

    /// <summary>
    /// Gets or sets the distance between the 
    /// last widget and the scrollbar
    /// </summary>
    public int ScrollGap
    {
      get { return _scrollGap; }
      set { _scrollGap = value; }
    }

    /// <summary>
    /// Gets or sets if  whether we should show the scrollbars
    /// </summary>
    [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public bool ShowScrollbars
    {
      get { return _showScrollbars; }
      set { _showScrollbars = value; }
    }

    #endregion

    #region Methods

    /// <summary>
    /// Releases all managed and unmanaged resources owned by the
    /// <see cref="WidgetCanvas"/> object.
    /// </summary>
    /// <param name="disposing">
    /// <c>true</c> to release both managed and unmanaged resources;
    /// <c>false</c> to release only unmanaged resources.
    /// </param>
    protected override void Dispose(bool disposing)
    {
      base.Dispose(disposing);
    }

    /// <summary>
    /// Returns the flag indicating if the control is being updated.
    /// </summary>
    protected bool Updating
    {
      get
      {
        return _updating;
      }
    }

    /// <summary>
    /// Begins the update process.
    /// </summary>
    public void BeginUpdate()
    {
      _updating = true;
    }

    /// <summary>
    /// Ends the update process.
    /// </summary>
    public void EndUpdate()
    {
      _updating = false;
      UpdateScroller();
      Invalidate();
    }

    /// <summary>
    /// Calculates the extent (the size of the rectangle that bounds all
    /// the Widgets held by the <see cref="WidgetCanvas"/> control.
    /// </summary>
    public virtual void UpdateScroller()
    {
      if (!_showScrollbars)
      {
        AutoScrollMinSize = new Size(0, 0);
        AutoScrollPosition = new Point(0, 0);
        return;
      }

      if (Updating)
        return;

      if (!_supportAutoScroll)
        return;

      Point scrollPoint = AutoScrollPosition;
      scrollPoint.X = -scrollPoint.X;
      scrollPoint.Y = -scrollPoint.Y;
      Size size = new Size();
      foreach (IWidget Widget in Widgets.List)
      {
        if (Widget.Visible)
        {
          size.Width = Math.Max(size.Width, Widget.X + Widget.Width + _scrollGap);
          size.Height = Math.Max(size.Height, Widget.Y + Widget.Height + _scrollGap);
        }
      }
      AutoScrollMinSize = size;
      AutoScrollPosition = scrollPoint;
    }

    /// <summary>
    /// Retrieves the <see cref="IWidget"/> object at the specified control
    /// coordinates.
    /// </summary>
    /// <param name="posX">The horizontal coordinate.</param>
    /// <param name="posY">The vertical coordinate.</param>
    /// <returns>
    /// An <see cref="IWidget"/> the mouse points at or <c>null</c> if
    /// the mouse does not point at a Widget.
    /// </returns>
    protected virtual List<IWidget> HitTest(int posX, int posY)
    {
      List<IWidget> ret = new List<IWidget>();

      for (int i = Widgets.List.Count - 1; i >= 0; i--)
      {
        IWidget Widget = Widgets.List[i];
        if (Widget.Visible && Widget.HitTest(posX, posY))
        {
          ret.Add(Widget);
        }
      }

      return ret;
    }

    private static bool Between(int value, int minimum, int maximum)
    {
      return ((value >= minimum) && (value < maximum));
    }
    private static int GetLineSize(int extent)
    {
      if (Between(extent, 0, 100))
        return 1;
      if (Between(extent, 100, 1000))
        return 10;
      if (Between(extent, 1000, 10000))
        return 100;
      return 1000;
    }
    private static int GetPageSize(int extent)
    {
      return 10 * GetLineSize(extent);
    }

    /// <summary>
    /// Remove a widget from the selection list
    /// </summary>
    /// <param name="widget"></param>
    internal void RemoveSelectionObject(IWidget widget)
    {
      foreach (var w in _selectedWidgetList)
      {
        if (w.Key.Equals(widget))
        {
          _selectedWidgetList.Remove(w);
          break;
        }
      }
    }

    /// <summary>
    /// Clears the selection
    /// </summary>
    public void ClearSelections()
    {
      //clear selections
      _selectedWidgetList.Clear();
      _moveMultipleWidgets = false;
      _dragWidget = null;
      _selectionDragging = false;

      foreach (IWidget item in Widgets.List)
      {
        ISelectable selectedItem = item as ISelectable;
        if (selectedItem != null)
        {
          if (item.Visible)
            selectedItem.UnSelect();
        }
      }
    }

    /// <summary>
    /// Select all widgets on the canvas
    /// </summary>
    public void SelectAll()
    {
      if (!_multiSelectSupported)
        return;

      this.ClearSelections();
      _multiSelecting = true;

      foreach (IWidget item in Widgets.List)
      {
        ISelectable selectedItem = item as ISelectable;
        if (selectedItem != null)
        {
          if (item.Visible)
          {
            if (item is SGIConnection)
            {
            }
            else
            {
              selectedItem.Select();
              _selectedWidgetList.Add(new KeyValuePair<IWidget, Point>(item, new Point(item.X, item.Y)));
            }
          }
        }
      }

      _multiSelecting = false;

      if (_selectedWidgetList.Count == 1)
        _selectedWidgetList.Clear();
    }

    /// <summary>
    /// 
    /// </summary>
    public void InvalidateAll()
    {
      foreach (IWidget item in Widgets.List)
      {
        if (item.Visible)
        {
          ((Widget)item).Invalidate();
        }
      }
    }

    /// <summary>
    /// Convert and normalize the points and draw the reversible frame.
    /// </summary>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    private void DrawReversibleLine(Point p1, Point p2)
    {
      // Convert the points to screen coordinates.
      p1 = PointToScreen(p1);
      p2 = PointToScreen(p2);

      // Draw the reversible frame.
      ControlPaint.DrawReversibleLine(p1, p2, this.BackColor);
    }

    /// <summary>
    /// Convert and normalize the points and draw the reversible frame.
    /// </summary>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    /// <returns></returns>
    private Rectangle DrawReversibleRect(Point p1, Point p2)
    {
      // Convert the points to screen coordinates.
      p1 = PointToScreen(p1);
      p2 = PointToScreen(p2);

      int w = p2.X - p1.X;
      int h = p2.Y - p1.Y;

      // Draw the reversible frame.
      Rectangle rect = new Rectangle(p1, new Size(w, h));
      ControlPaint.DrawReversibleFrame(rect, this.BackColor, FrameStyle.Dashed);

      return rect;
    }

    #endregion

    #region Event Handlers

    /// <summary>
    /// 
    /// </summary>
    public virtual void OnDeleteObject()
    {
      if (OnDelete != null)
      {
        OnDelete(this, EventArgs.Empty);
        this.Invalidate();
      }
    }

    /// <summary>
    /// Processes arrow keys for the <see cref="WidgetCanvas"/>.
    /// </summary>
    /// <param name="keyData">
    /// One of the <see cref="Keys"/> values that represents
    /// the key to process.
    /// </param>
    /// <returns>
    /// <c>true</c> for arrow keys; otherwise, <c>false</c>.
    /// </returns>
    protected override bool ProcessDialogKey(Keys keyData)
    {
      Keys key = keyData & (~Keys.Modifiers);
      bool control = (Keys.Control == (keyData & Keys.Control));
      int dx = 0, dy = 0;
      Size sz = AutoScrollMinSize;
      switch (key)
      {
        case Keys.A:
          if (control)
          {
            if (_multiSelectSupported)
            {
              ClearSelections();
              SelectAll();
            }
          }
          break;
        case Keys.Left:
          dx -= control ? GetPageSize(sz.Width) : GetLineSize(sz.Width);
          break;
        case Keys.Right:
          dx += control ? GetPageSize(sz.Width) : GetLineSize(sz.Width);
          break;
        case Keys.Up:
          dy -= control ? GetPageSize(sz.Height) : GetLineSize(sz.Height);
          break;
        case Keys.Down:
          dy += control ? GetPageSize(sz.Height) : GetLineSize(sz.Height);
          break;
        case Keys.Escape:
          if (_multiSelectSupported)
          {
            ClearSelections();
          }
          break;
        case Keys.Delete:
          OnDeleteObject();
          break;
        case Keys.Enter:
          if (this.SelectedObjects.Count > 0)
          {
            ((IWidget)this.SelectedObjects[0]).OnDoubleClick();
          }
          break;
        default:
          return base.ProcessDialogKey(keyData);
      }

      Point pt = AutoScrollPosition;
      pt.X = dx - pt.X;
      pt.Y = dy - pt.Y;
      AutoScrollPosition = pt;
      return true;
    }

    /// <summary>
    /// Native window handle is (re)created.
    /// </summary>
    /// <param name="e">
    /// An <see cref="EventArgs"/> object that contains the event data.
    /// </param>
    protected override void OnHandleCreated(System.EventArgs e)
    {
      base.OnHandleCreated(e);
      UpdateScroller();
    }

    /// <summary>
    /// Perfoms the control painting.
    /// </summary>
    /// <param name="e">
    /// An <see cref="EventArgs"/> object that contains the event data.
    /// </param>
    /// <remarks>
    /// Be aware that the <see cref="WidgetCanvas"/> does not raise
    /// the <see cref="System.Windows.Forms.Control.Paint"/> event but
    /// performs all drawing on it's own.
    /// </remarks>
    protected override void OnPaint(PaintEventArgs e)
    {
      // Cache some frequently accessed properties
      Graphics g = e.Graphics;
      Pen defaultPen = new Pen(Color.Black);

      g.FillRectangle(DrawHelper.Instance.CreateSolidBrush(BackColor), e.ClipRectangle);
      g.SmoothingMode = SmoothingMode.AntiAlias;
      g.Clear(BackColor);

      #region Draw Grid Lines
      if (_showGrid)
      {
        for (float i = 0; i <= this.ClientSize.Width; i += 20 * 1)
        {
          for (float j = 0; j <= this.ClientSize.Height; j += 20 * 1)
          {
            Pen p = Pens.Gray;
            g.DrawLine(p, new Point((int)i, (int)j), new Point((int)i + 1, (int)j));
            p.Dispose();
          }
        }
      }
      #endregion

      //if (MultipleSelected)
      //{
      //  _dragOverlayRect = new Rectangle(_dragOverlayLeft, _dragOverlayTop, _dragOverlayRight, _dragOverlayBottom);
      //  g.DrawRectangle(defaultPen, _dragOverlayRect);
      //}

      #region Draw Widgets
      Rectangle clip = e.ClipRectangle;
      Rectangle clipRect = new Rectangle(clip.X - AutoScrollPosition.X, clip.Y - AutoScrollPosition.Y, clip.Width + 1, clip.Height + 1);
      Rectangle bounds = new Rectangle();
      foreach (IWidget widget in Widgets.List)
      {
        if (widget.Visible)
        {
          try
          {
            bounds.X = widget.X;
            bounds.Y = widget.Y;
            bounds.Width = widget.Width;
            bounds.Height = widget.Height;
            if (clipRect.IntersectsWith(bounds) || (widget is SGIConnection))
            {
              g.TranslateTransform(widget.X + AutoScrollPosition.X, widget.Y + AutoScrollPosition.Y);
              widget.Draw(g);
              g.TranslateTransform(-widget.X - AutoScrollPosition.X, -widget.Y - AutoScrollPosition.Y);
            }
          }
          finally { }
        }
      }
      #endregion

      defaultPen.Dispose();
    }

    /// <summary>
    /// Fires the double click event handler
    /// </summary>
    /// <param name="e"></param>
    protected override void OnDoubleClick(EventArgs e)
    {
      List<IWidget> hitResults = HitTest(_mouseLocation.X, _mouseLocation.Y);
      IWidget Widget = null;
      if (hitResults.Count > 0)
        Widget = hitResults[0];

      if (null == Widget)
        return;

      if (Widget.Enabled)
      {
        Widget.OnDoubleClick();
      }
    }

    /// <summary>
    /// Fires the click event handler
    /// </summary>
    /// <param name="e"></param>
    protected override void OnClick(EventArgs e)
    {
      //if we didn't find a Widget on the location, then we clicked on the white area
      List<IWidget> hitResults = HitTest(_mouseLocation.X, _mouseLocation.Y);
      IWidget Widget = null;
      if (hitResults.Count > 0)
        Widget = hitResults[0];

      if (Widget != null)
        return;

      if (_capturingWidget != null)
        return;

      if (MultiSelectSupported)
        ClearSelections();

      //raise if we only clicked on the white area
      if (OnClicked != null)
        OnClicked(this, EventArgs.Empty);
    }

    /// <summary>
    /// Translates the mouse down event to <see cref="IWidget.OnMouseDown"/>
    /// call.
    /// </summary>
    /// <param name="e">
    /// A <see cref="MouseEventArgs"/> object that contains the event data.
    /// </param>
    /// <remarks>
    /// Be aware that the <see cref="WidgetCanvas"/> does not raise
    /// the <see cref="System.Windows.Forms.Control.MouseDown"/> event but
    /// performs all mouse processing on it's own.
    /// </remarks>
    protected override void OnMouseDown(MouseEventArgs e)
    {
      int x = e.X - AutoScrollPosition.X;
      int y = e.Y - AutoScrollPosition.Y;

      _mouseLocation = new Point(x, y);
      List<IWidget> hitResults = HitTest(x, y);
      IWidget selectedWidget = null;

      if (hitResults.Count > 0)
      {
        selectedWidget = hitResults[0];

        #region CtrlKeyDown
        /*
                 * Check if the control key is down - if it is then add the clicked widget to the
                 * selected widget list
                 */
        if (Keyboard.CtrlKeyDown)
        {
          if (!_multiSelectSupported)
            return;

          /* select the widget */
          ISelectable selectedItem = selectedWidget as ISelectable;
          if (selectedItem != null)
          {
            if (selectedItem.Selected)
            {
              foreach (var w in _selectedWidgetList)
              {
                if (w.Key.Equals(selectedWidget))
                {
                  _selectedWidgetList.Remove(w);
                  break;
                }
              }
              selectedItem.UnSelect();
            }
            else
            {
              _selectedWidgetList.Add(new KeyValuePair<IWidget, Point>(selectedWidget, new Point(selectedWidget.X, selectedWidget.Y)));
              selectedItem.Select();
            }
          }
          return;
        }
        #endregion

        /*
                 * if there is only one item selected  - then clear the selection list
                 * because we are only working with one item - note that by default the code down below
                 * will add this item back into the selected list so that it can be retrieved via the
                 * selected widgets property
                 */
        if (_selectedWidgetList.Count == 1)
          _selectedWidgetList.Clear();

        /*
         * Check if the selected item is part of the selections - if it is then
         * set the flag '_moveMultipleWidgets' to true so that when this selected item is moved
         * the reset if the selected items will follow.
         */
        if (_selectedWidgetList.Count > 0)
        {
          /* check if the clicked widget is part of the selection */
          bool partOfSelecttion = false;
          foreach (KeyValuePair<IWidget, Point> w in _selectedWidgetList)
          {
            if (w.Key.Equals(selectedWidget))
            {
              partOfSelecttion = true;
              break;
            }
          }

          if (partOfSelecttion) // widget is part of the selection
          {
            if (selectedWidget.Enabled)
            {
              _moveMultipleWidgets = true; //prepare to move multiple widgets
              _dragWidget = selectedWidget; //main widget used for dragging
              _capturingWidget = selectedWidget; // the main widget that was clicked in the selection
              Capture = true;
              _selectionDragging = true;

              _oldX = x;
              _oldY = y;

              selectedWidget.OnMouseDown(x - selectedWidget.X, y - selectedWidget.Y, e.Button);

              if (e.Button == MouseButtons.Right)
              {
                // show context menu
                if (selectedWidget.ContextMenuStrip != null)
                {
                  Control c = new Control();
                  c.Tag = selectedWidget;

                  WindowAPI.POINT pt = new WindowAPI.POINT();
                  WindowAPI.GetCursorPos(out pt);
                  selectedWidget.ContextMenuStrip.Show(c, pt.x - 2, pt.y - 28);
                }
              }
            }

            return;
          }
        }

        /* 
         * add the selected single item to the selection list .
         * A single selected item is added to the selection list so that we can use one
         * property to read all the selected items wheather it is one or more
         */
        if (_selectedWidgetList.Count == 0)
        {
          _selectedWidgetList.Add(new KeyValuePair<IWidget, Point>(selectedWidget, new Point(selectedWidget.X, selectedWidget.Y)));
        }
      }

      if (selectedWidget != null)
      {
        if (selectedWidget.ZOrder)
          Widgets.BringToFront((Widget)selectedWidget);
      }

      #region Remove Selections

      // check if a widget was clicked
      if (hitResults.Count > 0)
      {
        foreach (IWidget item in Widgets.List)
        {
          ISelectable selectedItem = item as ISelectable;
          if (selectedItem != null)
          {
            if (item != selectedWidget)  //unselect the item if is not equal to the clicked item
            {
              if (selectedWidget is LineConnector)
              {
                if (((LineConnector)selectedWidget).ParentWidget != item)
                  selectedItem.UnSelect();
                else
                  selectedItem.Select();
              }
              else
              {
                if (item.Visible)
                  selectedItem.UnSelect();
              }
            }
          }
        }
      }
      #endregion

      Invalidate();

      if (selectedWidget == null)
      {
        if (e.Button == MouseButtons.Left)
        {
          if (_multiSelectSupported && !_moveMultipleWidgets)
          {
            _drawingReverseRect = true;
            //points used by the reverse line
            _originalRectPoint = new Point(e.X, e.Y);
            _lastRectPoint = _originalRectPoint;
          }
        }
        else
        {
          // white space was clicked
          if (OnClicked != null)
            OnClicked(this, EventArgs.Empty);
        }

        return;
      }

      if (selectedWidget.Enabled)
      {
        _capturingWidget = selectedWidget;
        Capture = true;

        //select widget
        ISelectable selectedItem = selectedWidget as ISelectable;
        if (selectedItem != null)
          selectedItem.Select();

        //fire mouse down event
        selectedWidget.OnMouseDown(x - selectedWidget.X, y - selectedWidget.Y, e.Button);

        //initialise reverse line
        if (selectedWidget is LineConnector)
        {
          _drawingReverseLine = true;

          //points used by the reverse line
          _originalPoint = new Point(e.X, e.Y);
          _lastPoint = _originalPoint;

          //set source
          _sourceWidget = ((LineConnector)selectedWidget).ParentWidget;

          return;
        }

        if (e.Button == MouseButtons.Right)
        {
          // show context menu
          if (selectedWidget.ContextMenuStrip != null)
          {
            Control c = new Control();
            c.Tag = selectedWidget;

            WindowAPI.POINT pt = new WindowAPI.POINT();
            WindowAPI.GetCursorPos(out pt);
            selectedWidget.ContextMenuStrip.Show(c, pt.x - 2, pt.y - 28);

            return;
          }
        }
      }
    }

    /// <summary>
    /// Translates the mouse move event to <see cref="IWidget.OnMouseHover"/>,
    /// <see cref="IWidget.OnMouseEnter"/> or <see cref="IWidget.OnMouseLeave"/>
    /// calls. In addition handles mouse capture.
    /// </summary>
    /// <param name="e">
    /// A <see cref="MouseEventArgs"/> object that contains the event data.
    /// </param>
    /// <remarks>
    /// Be aware that the <see cref="WidgetCanvas"/> does not raise
    /// the <see cref="System.Windows.Forms.Control.MouseMove"/> event but
    /// performs all mouse processing on it's own.
    /// </remarks>
    protected override void OnMouseMove(MouseEventArgs e)
    {
      int currentX = e.X - AutoScrollPosition.X;
      int currentY = e.Y - AutoScrollPosition.Y;

      if (_selectionDragging)
      {
        if (_dragWidget != null)
        {
          if (_moveMultipleWidgets) //move the selected objects
          {
            //update overlay position
            UpdateDragOverlayPosition();

            //get the difference of X and Y
            int wX = _dragWidget.X;
            int wY = _dragWidget.Y;

            int diffX = currentX - _oldX;
            int diffY = currentY - _oldY;

            for (int i = 0; i < _selectedWidgetList.Count; i++)
            {
              //exclude the drag object from the list
              if (_dragWidget != _selectedWidgetList[i].Key) 
              {
                //move the other objects in the list based on the difference

                if (_dragOverlayLeft + diffX > 0)
                {
                  _selectedWidgetList[i].Key.X += diffX;
                }
                if (_dragOverlayTop + diffY > 0)
                {
                  _selectedWidgetList[i].Key.Y += diffY;
                }
              }
            }

            //update the old location
            _oldX = currentX;
            _oldY = currentY;

            //update overlay position
            UpdateDragOverlayPosition();

            Invalidate();
          }
        }
      }

      List<IWidget> hitResults = HitTest(currentX, currentY);
      IWidget Widget = null;
      if (hitResults.Count > 0)
        Widget = hitResults[0];

      #region Draw Reverse Line
      if (_drawingReverseLine)
      {
        //erase old line
        DrawReversibleLine(_originalPoint, _lastPoint);

        //draw new line
        _lastPoint = new Point(e.X, e.Y);
        DrawReversibleLine(_originalPoint, _lastPoint);

        if (hitResults.Count > 0)
        {
          /*the line connector is over another widget*/
          if (hitResults[0] != _currentTrackingWidget) //make sure this is not a line connector
          {

            if (_dragLineCurrentTrackingWidget == null)
            {
              //fire a mouse enter event for the widget
              if (hitResults[0].Enabled)
              {
                hitResults[0].OnMouseEnter(currentX - hitResults[0].X, currentY - hitResults[0].Y);
                _dragLineCurrentTrackingWidget = hitResults[0];
              }
            }
            else
            {
              if (hitResults[0] != _dragLineCurrentTrackingWidget)
              {
                //fire a mouse leave event for the widget
                if (_dragLineCurrentTrackingWidget != null)
                {
                  if (_dragLineCurrentTrackingWidget.Enabled)
                  {
                    _dragLineCurrentTrackingWidget.OnMouseLeave(currentX - _dragLineCurrentTrackingWidget.X, currentY - _dragLineCurrentTrackingWidget.Y);
                    _dragLineCurrentTrackingWidget = null;

                  }
                }

                //fire a mouse enter event for the widget
                if (hitResults[0].Enabled)
                {
                  hitResults[0].OnMouseEnter(currentX - hitResults[0].X, currentY - hitResults[0].Y);
                  _dragLineCurrentTrackingWidget = hitResults[0];
                }
              }
            }
          }
        }
        else
        {
          //fire a mouse leave event for the widget
          if (_dragLineCurrentTrackingWidget != null)
          {
            if (_dragLineCurrentTrackingWidget.Enabled)
            {
              _dragLineCurrentTrackingWidget.OnMouseLeave(currentX - _dragLineCurrentTrackingWidget.X, currentY - _dragLineCurrentTrackingWidget.Y);
              _dragLineCurrentTrackingWidget = null;
            }
          }
        }

        return;
      }
      else
      {
        if (_drawingReverseRect)
        {
          //erase old line
          DrawReversibleRect(_originalRectPoint, _lastRectPoint);

          //draw new line
          _lastRectPoint = new Point(e.X, e.Y);
          DrawReversibleRect(_originalRectPoint, _lastRectPoint);

          return;
        }
      }
      #endregion

      #region Fire Mouse Events
      if (Widget == _currentTrackingWidget) //check if we are still in the same Widget
      {
        if (null != Widget) //make sure we not working with a blank Widget
        {
          Widget.OnMouseHover(currentX - Widget.X, currentY - Widget.Y);
          UpdateScroller();
          Invalidate();
        }

        return;
      }
      if (null != _currentTrackingWidget) //check if the current Widget is not null
      {
        if (_currentTrackingWidget.Dragging) //check if we are dragging a Widget
        {
          _currentTrackingWidget.OnMouseHover(currentX - _currentTrackingWidget.X, currentY - _currentTrackingWidget.Y);
          UpdateScroller();
          Invalidate();
          return;
        }
        if (_currentTrackingWidget.Enabled)
        {
          _currentTrackingWidget.OnMouseLeave(currentX - _currentTrackingWidget.X, currentY - _currentTrackingWidget.Y);

          // unselect hover
          ISelectable selectedItem = _currentTrackingWidget as ISelectable;
          if (selectedItem != null)
          {
            if (!selectedItem.Selected)
              selectedItem.UnSelect();
          }

          _currentTrackingWidget = null;
        }

      }

      if (null != Widget)
      {
        if (Widget.Enabled)
        {
          Widget.OnMouseEnter(currentX - Widget.X, currentY - Widget.Y);
          _currentTrackingWidget = Widget;
        }
      }

      #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public void UpdateDragOverlayPosition()
    {
      /*Update the location of the overlay rect for multiple selection dragging*/
      _dragOverlayLeft = _selectedWidgetList.Min(i => i.Key.X);
      _dragOverlayTop = _selectedWidgetList.Min(i => i.Key.Y);
      _dragOverlayRight = _selectedWidgetList.Max(i => i.Key.X + i.Key.Width) - _dragOverlayLeft;
      _dragOverlayBottom = _selectedWidgetList.Max(i => i.Key.Y + i.Key.Height) - _dragOverlayTop;
    }

    /// <summary>
    /// Translates the mouse up event to <see cref="IWidget.OnMouseUp"/>
    /// call. In addition handles mouse capture.
    /// </summary>
    /// <param name="e">
    /// A <see cref="MouseEventArgs"/> object that contains the event data.
    /// </param>
    /// <remarks>
    /// Be aware that the <see cref="WidgetCanvas"/> does not raise
    /// the <see cref="System.Windows.Forms.Control.MouseUp"/> event but
    /// performs all mouse processing on it's own.
    /// </remarks>
    protected override void OnMouseUp(MouseEventArgs e)
    {
      int x;
      int y;

      IWidget selectedWidget;
      bool reverseLine = _drawingReverseLine;

      _selectionDragging = false;


      #region Reverse Line
      if (_drawingReverseLine)
      {
        _drawingReverseLine = false;
        //erase old line
        DrawReversibleLine(_originalPoint, _lastPoint);
        Invalidate();
      }

      if (reverseLine) //check if we need to connect objects
      {
        x = e.X - AutoScrollPosition.X;
        y = e.Y - AutoScrollPosition.Y;

        List<IWidget> hitResults = HitTest(x, y);
        IWidget distWidget = null;
        if (hitResults.Count > 0)
          distWidget = hitResults[0];

        //set destination
        _destinationWidget = distWidget;

        //check if source is not the same as destination
        if (_destinationWidget != null)
        {
          if (_sourceWidget.Equals(_destinationWidget))
            return;
          else
          {
            //fire the connect event
            if (OnConnect != null)
              OnConnect(_sourceWidget, _destinationWidget);
          }
        }
      }
      #endregion

      #region Selection Regtangle
      if (_drawingReverseRect)
      {
        _drawingReverseRect = false;

        //erase old line
        _selectionArea = DrawReversibleRect(_originalRectPoint, _lastRectPoint);
        _selectionArea.Location = PointToClient(_selectionArea.Location);
        _selectionArea.X -= AutoScrollPosition.X;
        _selectionArea.Y -= AutoScrollPosition.Y;

        /* -W and -H */
        if (_selectionArea.Size.Width < 0 && _selectionArea.Size.Height < 0)
        {
          _selectionArea.Size = new Size(Math.Abs(_selectionArea.Size.Width), Math.Abs(_selectionArea.Size.Height));
          /* move rect on the right position */
          _selectionArea.Location = new Point(_selectionArea.X - _selectionArea.Width, _selectionArea.Y - _selectionArea.Height);
        }
        /* -W*/
        else if (_selectionArea.Size.Width < 0)
        {
          _selectionArea.Size = new Size(Math.Abs(_selectionArea.Size.Width), Math.Abs(_selectionArea.Size.Height));
          /* move rect on the right position */
          _selectionArea.X = _selectionArea.X - _selectionArea.Width;
        }
        /* -H */
        else if (_selectionArea.Size.Height < 0)
        {
          _selectionArea.Size = new Size(Math.Abs(_selectionArea.Size.Width), Math.Abs(_selectionArea.Size.Height));
          /* move rect on the right position */
          _selectionArea.Y = _selectionArea.Y - _selectionArea.Height;
        }


        /* Select Widgets that are in the selection area */
        _multiSelecting = true;
        foreach (IWidget item in Widgets.List)
        {
          ISelectable selectedItem = item as ISelectable;
          if (selectedItem != null)
          {
            if (item.Visible)
            {
              if (((Widget)item).Image != null)
              {
                // use this logic for selection if there is a centered image
                Rectangle imageRect = ((Widget)item).ClientImageRectangle;
                imageRect.X = imageRect.X + ((Widget)item).X;
                imageRect.Y = imageRect.Y + ((Widget)item).Y;

                if (_selectionArea.IntersectsWith(imageRect))
                {
                  selectedItem.Select();
                  _selectedWidgetList.Add(new KeyValuePair<IWidget, Point>(item, new Point(item.X, item.Y)));
                }
              }
              else // default logic
              {
                if (_selectionArea.IntersectsWith(((Widget)item).ClientRectangle))
                {
                  selectedItem.Select();
                  _selectedWidgetList.Add(new KeyValuePair<IWidget, Point>(item, new Point(item.X, item.Y)));
                }
              }
            }
          }
        }
        _multiSelecting = false;
        Invalidate();
      }
      #endregion

      if (null == _capturingWidget)
        return;

      selectedWidget = _capturingWidget;
      x = e.X - AutoScrollPosition.X;
      y = e.Y - AutoScrollPosition.Y;
      if (selectedWidget.Enabled)
        selectedWidget.OnMouseUp(x - selectedWidget.X, y - selectedWidget.Y, e.Button);
      _capturingWidget = null;
      Capture = false;

    }

    /// <summary>
    /// Translates the mouse move event to <see cref="IWidget.OnMouseLeave"/>
    /// call. In addition handles mouse capture.
    /// </summary>
    /// <param name="e">
    /// A <see cref="MouseEventArgs"/> object that contains the event data.
    /// </param>
    /// <remarks>
    /// Be aware that the <see cref="WidgetCanvas"/> does not raise
    /// the <see cref="System.Windows.Forms.Control.MouseLeave"/> event but
    /// performs all mouse processing on it's own.
    /// </remarks>
    protected override void OnMouseLeave(System.EventArgs e)
    {
      if (null != _currentTrackingWidget)
      {
        if (_currentTrackingWidget.Enabled)
        {
          _currentTrackingWidget.OnMouseLeave(0, 0);
        }

        _currentTrackingWidget = null;
      }
    }

    /// <summary>
    /// Should raise <see cref="System.Windows.Forms.Control.MouseEnter"/>
    /// event.
    /// </summary>
    /// <param name="e">
    /// An EventArgs that contains the event data.
    /// </param>
    /// <remarks>
    /// This class performs custom mouse processing and does not raise
    /// the propert event.
    /// </remarks>
    protected override void OnMouseEnter(System.EventArgs e)
    { }

    /// <summary>
    /// Should raise <see cref="System.Windows.Forms.Control.MouseHover"/>
    /// event.
    /// </summary>
    /// <param name="e">
    /// An EventArgs that contains the event data.
    /// </param>
    /// <remarks>
    /// This class performs custom mouse processing and does not raise
    /// the propert event.
    /// </remarks>
    protected override void OnMouseHover(System.EventArgs e)
    { }

    /// <summary>
    /// Should raise the
    /// <see cref="System.Windows.Forms.Control.MouseWheel"/> event.
    /// </summary>
    /// <param name="e">
    /// A <see cref="MouseEventArgs"/> that contains the event data.
    /// </param>
    /// <remarks>
    /// This class performs custom mouse processing and does not raise
    /// the propert event.
    /// </remarks>
    protected override void OnMouseWheel(System.Windows.Forms.MouseEventArgs e)
    {
      //int l = SystemInformation.MouseWheelScrollLines;
    }

    /// <summary>
    /// Window procedure function
    /// </summary>
    /// <param name="m"></param>
    protected override void WndProc(ref Message m)
    {
      try
      {
        int WM_CONTEXTMENU = 123;

        //supress the container's context menu if mouse click inside Widget
        if (m.Msg == WM_CONTEXTMENU)
        {
          if (HitTest(_mouseLocation.X, _mouseLocation.Y).Count > 0)
            return;
        }

        base.WndProc(ref m);
      }
      catch (OutOfMemoryException outEx)
      {
        string error = outEx.ToString();
        MessageBox.Show("A fatal error has occured!\n\nThe application will now exit.", "Error",
            MessageBoxButtons.OK, MessageBoxIcon.Error);

        Application.Exit();
      }
    }

    #endregion

    #region IControlService interface implementation
    /// <summary>
    /// Gets the <see cref="WidgetCanvas"/> that keeps the Widget.
    /// </summary>
    /// <value>
    /// The <see cref="WidgetCanvas"/> that keeps the Widget.
    /// </value>
    public virtual WidgetCanvas Control
    {
      get
      {
        return this;
      }
    }

    /// <summary>
    /// Invalidates a rectangular area area of the Widget.
    /// </summary>
    /// <param name="x">
    /// The x-coordinate of the upper-left corner of the Widget.
    /// </param>
    /// <param name="y">
    /// The y-coordinate of the upper-left corner of the Widget.
    /// </param>
    /// <param name="width">
    /// The width of the Widget.
    /// </param>
    /// <param name="height">
    /// The height of the Widget.
    /// </param>
    public void InvalidateRectangle(int x, int y, int width, int height)
    {
      if (!Created)
        return;
      Invalidate(new Rectangle(x + AutoScrollPosition.X, y + AutoScrollPosition.Y, width, height));
    }
    #endregion

  }
}
