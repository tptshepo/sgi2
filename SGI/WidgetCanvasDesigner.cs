using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace SGI
{
	/// <summary>
	/// A custom designer for the <see cref="WidgetCanvas"/> control.
	/// </summary>
	/// <seealso cref="WidgetCanvas"/>
	internal class WidgetCanvasDesigner: ControlDesigner
	{
		/// <summary>
		/// Allows the designer to adjust attributes of
		/// <see cref="ScrollableControl.AutoScroll"/> and
		/// <see cref="Panel.BorderStyle"/> properties.
		/// </summary>
		/// <param name="properties">
		/// The properties for the <see cref="WidgetCanvas"/> control.
		/// </param>
		protected override void PostFilterProperties(IDictionary properties)
		{
			PropertyDescriptor pd;
			Attribute[] attributes;
			int i;

			pd = (PropertyDescriptor)properties["AutoScroll"];
			attributes = new Attribute[pd.Attributes.Count];
			pd.Attributes.CopyTo(attributes, 0);
			for(i = 0; i < attributes.Length; i++)
			{
				if(typeof(DefaultValueAttribute) == attributes[i].GetType())
				{
					attributes[i] = new DefaultValueAttribute(true);
					break;
				}
			}
			pd = TypeDescriptor.CreateProperty(pd.ComponentType, pd, attributes);
			properties[pd.Name] = pd;

			pd = (PropertyDescriptor)properties["BorderStyle"];
			attributes = new Attribute[pd.Attributes.Count];
			pd.Attributes.CopyTo(attributes, 0);
			for(i = 0; i < attributes.Length; i++)
			{
				if(typeof(DefaultValueAttribute) == attributes[i].GetType())
				{
					attributes[i] = new DefaultValueAttribute(BorderStyle.Fixed3D);
					break;
				}
			}
			pd = TypeDescriptor.CreateProperty(pd.ComponentType, pd, attributes);
			properties[pd.Name] = pd;

			properties.Remove("Widgets");
			properties.Remove("Control");

			base.PostFilterProperties(properties);
		}

		/// <summary>
		/// Allows the designer to remove some design-time event descriptors for
		/// the <see cref="WidgetCanvas"/> control.
		/// </summary>
		/// <param name="events">
		/// The events for the <see cref="WidgetCanvas"/> control.
		/// </param>
		protected override void PostFilterEvents(IDictionary events)
		{
			events.Remove("Paint");
			events.Remove("MouseDown");
			events.Remove("MouseUp");
			events.Remove("MouseMove");
			events.Remove("MouseLeave");
			events.Remove("Click");
			events.Remove("DoubleClick");

			base.PostFilterEvents(events);
		}
	}
}
