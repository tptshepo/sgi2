﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace SGI
{
    /// <summary>
    /// Static class for drawing the text with outlining and shadow
    /// </summary>
    public class TextPainter : IDisposable
    {
        private Font defaultFont;

        /// <summary>
        /// Default font name
        /// </summary>
        private string defaultName = "Tahoma";
        /// <summary>
        /// Default font size
        /// </summary>
        private float defaultSize = 8.0f;
        /// <summary>
        /// Default font style
        /// </summary>
        private FontStyle defaultStyle = FontStyle.Bold;
        /// <summary>
        /// Default text color
        /// </summary>
        private Color defaultColor = Color.Black;

        /// <summary>
        /// Default text outline transparency
        /// </summary>
        public int DefaultOutlineTransparency = 0; //140
        /// <summary>
        /// Default shadow transparency
        /// </summary>
        public int DefaultShadowTransparency = 0; //64
        /// <summary>
        /// Default shadow horizontal offset
        /// </summary>
        public int DefaultShadowX = 0; //2
        /// <summary>
        /// Default shadow vertical offset
        /// </summary>
        public int DefaultShadowY = 0; //2
        /// <summary>
        /// Gets the default font.
        /// </summary>
        /// <value>The default font.</value>
        public Font DefaultFont
        {
            get
            {
                if (defaultFont == null)
                    defaultFont = new Font(defaultName, defaultSize, defaultStyle, GraphicsUnit.Point);
                return defaultFont;
            }
        }

        /// <summary>
        /// Gets the default color
        /// </summary>
        public Color DefaultColor
        {
            get { return defaultColor; }
        }

        /// <summary>
        /// Draw text on the screen
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="text"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void DrawString(Graphics canvas, string text, int x, int y, int width, int height)
        {
            using (Font font = new Font(defaultName, defaultSize, defaultStyle, GraphicsUnit.Point))
            {
                DrawString(canvas, text, font, x, y, width, height, DefaultColor, false);
            }
        }

        /// <summary>
        /// Draw text on the screen
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="text"></param>
        /// <param name="font"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void DrawString(Graphics canvas, string text, Font font, int x, int y, int width, int height)
        {
            DrawString(canvas, text, font, x, y, width, height, DefaultColor, false);
        }

        /// <summary>
        /// Draw text on the screen
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="text"></param>
        /// <param name="font"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="bodyColor"></param>
        public void DrawString(Graphics canvas, string text, Font font, int x, int y, int width, int height, Color bodyColor)
        {
            DrawString(canvas, text, font, x, y, width, height, bodyColor, false);
        }

        /// <summary>
        /// Draw text on the screen
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="text"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="centered"></param>
        public void DrawString(Graphics canvas, string text, int x, int y, int width, int height, bool centered)
        {
            using (Font font = new Font(defaultName, defaultSize, defaultStyle, GraphicsUnit.Point))
            {
                DrawString(canvas, text, font, x, y, width, height, DefaultColor, centered);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="text"></param>
        /// <param name="font"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="bodyColor"></param>
        /// <param name="centered"></param>
        public void DrawString(Graphics canvas, string text, Font font, int x, int y, int width, int height, Color bodyColor, bool centered)
        {
            DrawString(canvas, text, font, x, y, width, height, DefaultColor, ContentAlignment.MiddleCenter);
        }

        /// <summary>
        /// Draw text on the screen
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="text"></param>
        /// <param name="font"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="bodyColor"></param>
        /// <param name="alignment"></param>
        public void DrawString(Graphics canvas, string text, Font font, int x, int y, int width, int height, Color bodyColor, ContentAlignment alignment)
        {
            if (canvas == null)
                return;

            if (string.IsNullOrEmpty(text))
                return;

            if (font == null)
                return;

            float shadowX;
            float shadowY;
            GraphicsPath textPath;
            Pen outlinePen;
            SolidBrush bodyBrush;
            SolidBrush shadowBrush;
            StringFormat stringFormat;

            canvas.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
            canvas.SmoothingMode = SmoothingMode.AntiAlias;

            bodyBrush = new SolidBrush(bodyColor);
            outlinePen = new Pen(Color.FromArgb(DefaultOutlineTransparency, Color.Black));
            shadowBrush = new SolidBrush(Color.FromArgb(DefaultShadowTransparency, Color.Black));
            stringFormat = new StringFormat(StringFormatFlags.LineLimit);
            textPath = new GraphicsPath(FillMode.Alternate);

            try
            {
                #region Drawing

                stringFormat.Trimming = StringTrimming.EllipsisWord;
                if (alignment == ContentAlignment.MiddleCenter)
                {
                    stringFormat.Alignment = StringAlignment.Center;
                    stringFormat.LineAlignment = StringAlignment.Center;
                }
                else if (alignment == ContentAlignment.MiddleLeft)
                {
                    stringFormat.Alignment = StringAlignment.Near;
                    stringFormat.LineAlignment = StringAlignment.Center;
                }
                else if (alignment == ContentAlignment.MiddleRight)
                {
                    stringFormat.Alignment = StringAlignment.Far;
                    stringFormat.LineAlignment = StringAlignment.Center;
                }
                else if (alignment == ContentAlignment.TopRight)
                {
                    stringFormat.Alignment = StringAlignment.Far;
                    stringFormat.LineAlignment = StringAlignment.Near;
                }
                else if (alignment == ContentAlignment.TopCenter)
                {
                    stringFormat.Alignment = StringAlignment.Center;
                    stringFormat.LineAlignment = StringAlignment.Near;
                }
                else if (alignment == ContentAlignment.TopLeft)
                {
                    stringFormat.Alignment = StringAlignment.Near;
                    stringFormat.LineAlignment = StringAlignment.Near;
                }
                else if (alignment == ContentAlignment.BottomLeft)
                {
                    stringFormat.Alignment = StringAlignment.Near;
                    stringFormat.LineAlignment = StringAlignment.Far;
                }
                else if (alignment == ContentAlignment.BottomCenter)
                {
                    stringFormat.Alignment = StringAlignment.Center;
                    stringFormat.LineAlignment = StringAlignment.Far;
                }
                else if (alignment == ContentAlignment.BottomRight)
                {
                    stringFormat.Alignment = StringAlignment.Far;
                    stringFormat.LineAlignment = StringAlignment.Far;
                }
                else
                {
                    stringFormat.Alignment = StringAlignment.Near;
                    stringFormat.LineAlignment = StringAlignment.Center;
                }


                shadowX = DefaultShadowX;
                shadowY = DefaultShadowY;

                textPath.AddString(text, font.FontFamily, (int)font.Style, font.Height, new Rectangle(x, y, width, height), stringFormat);

                canvas.TranslateTransform(shadowX, shadowY);

                canvas.FillPath(shadowBrush, textPath);
                canvas.DrawPath(outlinePen, textPath);
                canvas.FillPath(bodyBrush, textPath);

                canvas.TranslateTransform(-shadowX, -shadowY);

                #endregion
            }
            finally
            {
                outlinePen.Dispose();
                outlinePen = null;

                textPath.Dispose();
                textPath = null;

                stringFormat.Dispose();
                stringFormat = null;

                bodyBrush.Dispose();
                bodyBrush = null;

                shadowBrush.Dispose();
                shadowBrush = null;
            }
        }


        #region IDisposable Members

        public void Dispose()
        {

        }

        #endregion
    }
}
