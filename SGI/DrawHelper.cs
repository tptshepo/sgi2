using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace SGI
{
    // Change with accordance with DrawHelper.BitShift constant
    using Key = System.Int32;
    using System.IO;

    /// <summary>
    /// Represents a special collection of disposable drawing resources for
    /// light Widgets.
    /// </summary>
    /// <remarks>
    /// <para>This class provides a set of factory methods for some types from
    /// <c>System.Drawing</c> and <c>System.Drawing.Drawing2D</c> namespaces
    /// that implement <see cref="IDisposable"/> interface. If you need a
    /// resource for drawing, call a proper method. The method creates a
    /// drawing object and keeps it in the internal hash tables. If another
    /// <c>IWidget</c>-based object draws itself and requests an already created
    /// object, the instance of the <c>DrawHelper</c> will reuse it thus
    /// reducing resources consumption and, in some cases, speeding the
    /// application.</para>
    /// <para>Also if you dispose the <c>DrawingHelper</c> instance you also
    /// disposing of all alloted graphical resources at once. So while using
    /// this facility you don't have to keep track of those graphical resource.
    /// </para>
    /// </remarks>
    public sealed class DrawHelper
    {
        public DrawHelper()
        {
            Application.ApplicationExit += new EventHandler(OnApplicationExit);
            Application.Idle += new EventHandler(OnIdle);
            this.disposer = new ArrayList();
            this.colorPens = new Hashtable();
            this.brushPens = new Hashtable();
            this.solidBrushes = new Hashtable();
            this.textureBrushes = new Hashtable();
            this.fonts = new Hashtable();
            this.linearGradientBrushes = new Hashtable();
            this.hatchBrushes = new Hashtable();
            this.pathGradientBrushes1 = new Hashtable();
            this.pathGradientBrushes2 = new Hashtable();
            this.pathGradientBrushes3 = new Hashtable();
        }

        #region Draw Paths
        public static GraphicsPath BuildRect(Rectangle baseRect)
        {
            //Create a new Graphics Path
            GraphicsPath grfxPath = new GraphicsPath();
            //Create a new Rectangle for Calculations
            Rectangle grfxRect = new Rectangle();
            //Set the New Rectangle bound = to the control bounds
            grfxRect = new Rectangle(0, 0, (int)(baseRect.Width - 1), (int)(baseRect.Height - 1));
            try
            {
                //Start the Figure
                grfxPath.StartFigure();
                //Add the shape to the Path
                grfxPath.AddRectangle(grfxRect);
                //Close the Path, baseRect ensures that all line will form 
                //one Continuous shape outline
                grfxPath.CloseFigure();
                //Return the Path to be drawn and filled
                return grfxPath;
            }
            catch (IOException ioe)
            {
                string msg = ioe.Message;
                msg = null;
                return null;
            }
        }
        public static GraphicsPath BuildRndRect(Rectangle baseRect)
        {
            GraphicsPath grfxPath = new GraphicsPath();
            //Create a new Rectangle for Calculations
            Rectangle grfxRect = new Rectangle();
            //Set the New Rectangle bound = to the control bounds
            grfxRect = new Rectangle(baseRect.X, baseRect.Y, (int)(baseRect.Width - 1), (int)(baseRect.Height - 1));
            //The Radius of the rounded edges
            float radius = (int)(grfxRect.Height * 0.1);
            //Width of the rectangle
            float width = (int)(grfxRect.Width);
            //Height of the rectangle
            float height = (int)(grfxRect.Height);
            //Following two lines are simply the Location of the rectangle
            float x = grfxRect.Left;
            float y = grfxRect.Top;
            //Make sure the radius is a valid value
            if (radius < 1) { radius = 1; }

            grfxPath.StartFigure();
            grfxPath.AddLine(x + radius, y, x + width - (radius * 2), y);
            grfxPath.AddArc(x + width - (radius * 2), y, radius * 2, radius * 2, 270, 90);
            grfxPath.AddLine(x + width, y + radius, x + width, y + height - (radius * 2));
            grfxPath.AddArc(x + width - (radius * 2), y + height - (radius * 2), radius * 2, radius * 2, 0, 90);
            grfxPath.AddLine(x + width - (radius * 2), y + height, x + radius, y + height);
            grfxPath.AddArc(x, y + height - (radius * 2), radius * 2, radius * 2, 90, 90);
            grfxPath.AddLine(x, y + height - (radius * 2), x, y + radius);
            grfxPath.AddArc(x, y, radius * 2, radius * 2, 180, 90);
            grfxPath.CloseFigure();
            return grfxPath;
        }

        #endregion

        public int GetNearestGridPoint(int position /*X or Y*/)
        {
            //scale to units 
            double u = position / 20.0;
            //find the closest unit and scale back - snap to grid
            return (int)Math.Round(u) * 20;
        }
        public static int MeasureDisplayStringWidth(Graphics graphics, string text, Font font)
        {
            System.Drawing.StringFormat format = new System.Drawing.StringFormat();
            System.Drawing.RectangleF rect = new System.Drawing.RectangleF(0, 0,
                                                                          1000, 1000);
            System.Drawing.CharacterRange[] ranges = 
                                       { new System.Drawing.CharacterRange(0, 
                                                               text.Length) };
            System.Drawing.Region[] regions = new System.Drawing.Region[1];

            format.SetMeasurableCharacterRanges(ranges);

            regions = graphics.MeasureCharacterRanges(text, font, rect, format);
            rect = regions[0].GetBounds(graphics);

            regions[0].Dispose();
            regions = null;
            format.Dispose();
            format = null;

            return (int)(rect.Right + 1.0f);
        }

        public static Rectangle MeasureDisplayStringRect(Graphics graphics, string text, Font font)
        {
            System.Drawing.StringFormat format = new System.Drawing.StringFormat();
            System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, 1000, 1000);
            System.Drawing.CharacterRange[] ranges = { new System.Drawing.CharacterRange(0, text.Length) };
            System.Drawing.Region[] regions = new System.Drawing.Region[1];

            format.SetMeasurableCharacterRanges(ranges);

            regions = graphics.MeasureCharacterRanges(text, font, rect, format);
            rect = new Rectangle(Convert.ToInt32(regions[0].GetBounds(graphics).X),
                Convert.ToInt32(regions[0].GetBounds(graphics).Y),
                Convert.ToInt32(regions[0].GetBounds(graphics).Width),
                Convert.ToInt32(regions[0].GetBounds(graphics).Height));

            regions[0].Dispose();
            regions = null;
            format.Dispose();
            format = null;

            return rect;
        }

        #region Singleton pattern support
        /// <summary>
        /// Gets the single instance of the <see cref="DrawHelper"/> class.
        /// </summary>
        public static DrawHelper Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }

            internal static readonly DrawHelper instance = new DrawHelper();
        }
        #endregion

        #region Resource cleanup
        public void Cleanup()
        {
            if (0 == disposer.Count)
                return;

            foreach (IDisposable disposable in disposer)
                disposable.Dispose();
            disposer.Clear();

            colorPens.Clear();
            brushPens.Clear();
            solidBrushes.Clear();
            textureBrushes.Clear();
            fonts.Clear();
            linearGradientBrushes.Clear();
            hatchBrushes.Clear();
            pathGradientBrushes1.Clear();
            pathGradientBrushes2.Clear();
            pathGradientBrushes3.Clear();
            //Trace.WriteLine("Resources cleaned up");
        }

        void OnIdle(object sender, EventArgs e)
        {
            Cleanup();
        }

        void OnApplicationExit(object sender, EventArgs e)
        {
            Cleanup();
            Application.Idle -= new EventHandler(OnIdle);
            Application.ApplicationExit -= new EventHandler(OnApplicationExit);
        }
        #endregion

        const int BitShift = 1;

        ArrayList disposer;

        #region Hashtable declarations
        // NB: Keep in sync with Dispose method and factory properties
        Hashtable colorPens;
        Hashtable brushPens;
        Hashtable solidBrushes;
        Hashtable textureBrushes;
        Hashtable fonts;
        Hashtable linearGradientBrushes;
        Hashtable hatchBrushes;
        Hashtable pathGradientBrushes1;
        Hashtable pathGradientBrushes2;
        Hashtable pathGradientBrushes3;
        #endregion

        /// <summary>
        /// Gray out an image
        /// </summary>
        /// <returns></returns>
        public static ImageAttributes GrayOutImage()
        {
            // Create a color matrix
            // The value 0.6 in row 4, column 4 specifies the alpha value
            float[][] matrixItems = {
                                new float[] {1, 0, 0, 0, 0},
                                new float[] {0, 1, 0, 0, 0},
                                new float[] {0, 0, 1, 0, 0},
                                new float[] {0, 0, 0, 0.5f, 0}, 
                                new float[] {0, 0, 0, 0, 1}};
            ColorMatrix colorMatrix = new ColorMatrix(matrixItems);

            // Create an ImageAttributes object and set its color matrix
            ImageAttributes imageAtt = new ImageAttributes();
            imageAtt.SetColorMatrix(colorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

            return imageAtt;
        }

        /// <summary>
        /// Get text alignmenet
        /// </summary>
        /// <param name="align"></param>
        /// <returns></returns>
        public static StringFormat GetTextAlignment(ContentAlignment align)
        {
            StringFormat format = DrawHelper.Instance.CloneTypographicStringFormat();
            switch (align)
            {
                case ContentAlignment.BottomCenter:
                    format.Alignment = StringAlignment.Center;
                    format.LineAlignment = StringAlignment.Far;
                    break;
                case ContentAlignment.BottomLeft:
                    format.Alignment = StringAlignment.Near;
                    format.LineAlignment = StringAlignment.Far;
                    break;
                case ContentAlignment.BottomRight:
                    format.Alignment = StringAlignment.Far;
                    format.LineAlignment = StringAlignment.Far;
                    break;
                case ContentAlignment.MiddleCenter:
                    format.Alignment = StringAlignment.Center;
                    format.LineAlignment = StringAlignment.Center;
                    break;
                case ContentAlignment.MiddleLeft:
                    format.Alignment = StringAlignment.Near;
                    format.LineAlignment = StringAlignment.Center;
                    break;
                case ContentAlignment.MiddleRight:
                    format.Alignment = StringAlignment.Far;
                    format.LineAlignment = StringAlignment.Center;
                    break;
                case ContentAlignment.TopCenter:
                    format.Alignment = StringAlignment.Near;
                    format.LineAlignment = StringAlignment.Center;
                    break;
                case ContentAlignment.TopLeft:
                    format.Alignment = StringAlignment.Near;
                    format.LineAlignment = StringAlignment.Near;
                    break;
                case ContentAlignment.TopRight:
                    format.Alignment = StringAlignment.Near;
                    format.LineAlignment = StringAlignment.Far;
                    break;
            }
            return format;
        }

        /// <summary>
        /// Creates a solid brush with specified <paramref name="color"/>.
        /// </summary>
        /// <param name="color">
        /// A <see cref="Color"/> structure that represents the color of this
        /// brush.
        /// </param>
        /// <returns>
        /// An instance of a <see cref="SolidBrush"/> object with specified
        /// <paramref name="color"/>.
        /// </returns>
        public SolidBrush CreateSolidBrush(Color color)
        {
            Key key = color.GetHashCode();
            SolidBrush result = (SolidBrush)solidBrushes[key];
            if (null == result)
            {
                result = new SolidBrush(color);
                disposer.Add(result);
                solidBrushes.Add(key, result);
            }
            return result;
        }

        /// <summary>
        /// Creates a clone of <see cref="StringFormat.GenericTypographic"/>
        /// object so it can be customized.
        /// </summary>
        /// <returns>
        /// An exact copy of <see cref="StringFormat.GenericTypographic"/> that
        /// can be adjusted by the caller.
        /// </returns>
        public StringFormat CloneTypographicStringFormat()
        {
            StringFormat result = (StringFormat)StringFormat.GenericTypographic.Clone();
            disposer.Add(result);
            return result;
        }
    }
}
