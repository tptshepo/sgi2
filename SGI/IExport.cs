﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SGI
{
    /// <summary>
    /// The IExportable interface defines the structure for exporting to file 
    /// </summary>
    public interface IExportable
    {
        /// <summary>
        /// Indicates if the object can export
        /// </summary>
        /// <returns></returns>
        bool CanExport();

        /// <summary>
        /// Gets a list attributes
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> Attributes();
    }
}
