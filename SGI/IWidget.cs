using System;
using System.Drawing;
using System.Windows.Forms;

namespace SGI
{
    /// <summary>
    /// The Widget class represents a graphics element
    /// </summary>
	public interface IWidget : IBaseCollectionItem
	{
		/// <summary>
		/// Gets or sets a parent object that provides support for
		/// <see cref="IControlService"/>.
		/// </summary>
		IControlService Parent
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the horizontal coordinate of the Widget relative to it's
		/// parent object.
		/// </summary>
		int X
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the vertical coordinate of the Widget relative to it's
		/// parent object.
		/// </summary>
		int Y
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the width of the Widget.
		/// </summary>
		int Width
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the height of the Widget.
		/// </summary>
		int Height
		{
			get;
			set;
		}

		/// <summary>
		/// Enables or disables the Widget.
		/// </summary>
		bool Enabled
		{
			get;
			set;
		}

		/// <summary>
		/// Shows or hides the Widget.
		/// </summary>
		bool Visible
		{
			get;
			set;
		}

        /// <summary>
        /// Enables or disables dragging of the Widget.
        /// </summary>
        bool Dragging
        {
            get;
            set;
        }

        /// <summary>
        /// Moves a the widget up in z-order of widgets
        /// </summary>
        bool ZOrder
        {
            get;
            set;
        }

        /// <summary>
        ///Gets or sets the System.Windows.Forms.ContextMenuStrip associated with this control
        /// </summary>
        ContextMenuStrip ContextMenuStrip
        {
            get;
            set;
        }


		/// <summary>
		/// Invalidates a rectangular area specified in the Widget's cooridinates.
		/// </summary>
		/// <param name="x">
		/// The x-coordinate of the upper-left corner of the rectangle in
		/// the Widget's coordinates.
		/// </param>
		/// <param name="y">
		/// The y-coordinate of the upper-left corner of the rectangle in
		/// the Widget's coordinates.
		/// </param>
		/// <param name="width">
		/// The width of the Widget.
		/// </param>
		/// <param name="height">
		/// The height of the Widget.
		/// </param>
		void Invalidate(int x, int y, int width, int height);

		/// <summary>
		/// Performs the Widget painting.
		/// </summary>
		/// <param name="graphics">An instance of the <see cref="Graphics"/>
		/// to paint on.</param>
		void Draw(Graphics graphics);

		/// <summary>
		/// Mouse down notification callback.
		/// </summary>
		/// <param name="posX">The x-coordinate of the mouse.</param>
		/// <param name="posY">The y-coordinate of the mouse.</param>
		/// <param name="buttons">
		/// A <see cref="MouseButtons"/> that describes the mouse buttons
		/// pressed.
		/// </param>
		void OnMouseDown(int posX, int posY, MouseButtons buttons);

        /// <summary>
        /// Add to container notification
        /// </summary>
        /// <param name="container">The container adding the control</param>
        void OnAddToContainer(WidgetCanvas container);

        /// <summary>
        /// Remove from container notification
        /// </summary>
        void OnRemoveFromContainer(WidgetCanvas container);

        /// <summary>
        /// Occurs when the Widget is double clicked 
        /// </summary>
        void OnDoubleClick();

		/// <summary>
		/// Mouse up notification callback.
		/// </summary>
		/// <param name="posX">The x-coordinate of the mouse.</param>
		/// <param name="posY">The y-coordinate of the mouse.</param>
		/// <param name="buttons">
		/// A <see cref="MouseButtons"/> that describes the mouse buttons
		/// pressed.
		/// </param>
		void OnMouseUp(int posX, int posY, MouseButtons buttons);

		/// <summary>
		/// Mouse entered into the Widget's bounds notification callback.
		/// </summary>
		/// <param name="posX">The x-coordinate of the mouse.</param>
		/// <param name="posY">The y-coordinate of the mouse.</param>
		void OnMouseEnter(int posX, int posY);

		/// <summary>
		/// Mouse left the Widget's bounds notification callback.
		/// </summary>
		/// <param name="posX">The x-coordinate of the mouse.</param>
		/// <param name="posY">The y-coordinate of the mouse.</param>
		void OnMouseLeave(int posX, int posY);

		/// <summary>
		/// Mouse is being moved in the Widget's bounds notification callback.
		/// </summary>
		/// <param name="posX">The x-coordinate of the mouse.</param>
		/// <param name="posY">The y-coordinate of the mouse.</param>
		void OnMouseHover(int posX, int posY);

        /// <summary>
        /// Mouse is being moved in the Widget's bounds notification callback.
        /// </summary>
        /// <param name="posX">The x-coordinate of the mouse.</param>
        /// <param name="posY">The y-coordinate of the mouse.</param>
        void OnMouseMove(int posX, int posY);

		/// <summary>
		/// Checks if the mouse is in the Widget's bounds.
		/// </summary>
		/// <param name="posX">The x-coordinate of the mouse.</param>
		/// <param name="posY">The y-coordinate of the mouse.</param>
		/// <remarks>
		/// This method is for Widgets that are non-rectangular.
		/// </remarks>
		bool HitTest(int posX, int posY);

        /// <summary>
        /// Free memory
        /// </summary>
        void Dispose();
       // void OnLocationChanged(int posX, int posY);
	}
}
