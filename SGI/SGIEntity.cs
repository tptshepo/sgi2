

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SGI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace SGI
{
    /// <summary>
    /// Represents the most basic item that can be added to the <see cref="SGIScene"/>
    /// </summary>
    [Serializable()]
    public class SGIEntity : InteractiveWidget, IExportable
    {
        #region Fields
        private Point _originalPoint;
        private SGIEntityConnectionCollection _connectionOut; //the connection coming from this entity
        private SGIEntityConnectionCollection _connectionIn; //the connection coming into this entity (parentConnection)
        private SGIScene _containerScene;
        private Image _stateImage;
        private int _minWidth = 130;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the minimum width
        /// </summary>
        public int MinWidth
        {
            get { return _minWidth; }
            set { _minWidth = value; }
        }

        /// <summary>
        /// Gets or sets the image that displays the state of the entity
        /// </summary>
        public Image StateImage
        {
            get

            { return _stateImage; }

            set
            {
                if (_stateImage != value)
                {
                    _stateImage = value;
                    this.Invalidate();
                }
            }
        }

        /// <summary>
        /// Gets or sets the source code 
        /// </summary>
        public string ScriptCode { get; set; }

        /// <summary>
        /// Gets or sets the entity type
        /// </summary>
        public string EntityType { get; set; }

        /// <summary>
        /// Gets or sets if the entity is active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets if auto size is enabled
        /// </summary>
        public bool AutoSize { get; set; }

        /// <summary>
        /// Gets or sets the number of times to skip the entity
        /// </summary>
        public int Skip { get; set; }

        /// <summary>
        /// Gets or sets if the entity only runs once
        /// </summary>
        public bool RunOnce { get; set; }

        /// <summary>
        /// Gets or set the storage tag
        /// </summary>
        public object Tag2 { get; set; }

        /// <summary>
        /// Gets or sets the tooltip text
        /// </summary>
        public string ToolTip { get; set; }

        /// <summary>
        /// Gets or sets if the entity can be deleted
        /// </summary>
        public bool CanDelete { get; set; }

        /// <summary>
        /// Gets or sets the container scene for this Entity
        /// </summary>
        public SGIScene ContainerScene
        {
            get { return _containerScene; }
            set { _containerScene = value; }
        }

        /// <summary>
        /// Gets or sets the connections going in to the Entity
        /// </summary>
        public SGIEntityConnectionCollection ConnectionIn
        {
            get { return _connectionIn; }
            set { _connectionIn = value; }
        }

        /// <summary>
        /// Gets or sets the connections going out of the Entity
        /// </summary>
        public SGIEntityConnectionCollection ConnectionOut
        {
            get { return _connectionOut; }
            set { _connectionOut = value; }
        }

        /// <summary>
        /// Gets or sets the original points of the Entity
        /// <remarks>
        /// The original points will be used to reset the widget to the original location
        /// before summing it with the <see cref="SGIScene"/> 's points
        /// </remarks>
        /// </summary>
        public Point OriginalPoint
        {
            get { return _originalPoint; }
            set { _originalPoint = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public SGIEntity()
            : base(1, 1, 1, 1)
        {
            this.BringToFront();
            this.Border = true;
            this.Transparent = true;
            this.DrawBackground = true;

            SetDefaultSettings();
            EntityType = "SGIEntity";

            Active = true;
            CanDelete = true;

            _connectionOut = new SGIEntityConnectionCollection();
            _connectionIn = new SGIEntityConnectionCollection();
            _originalPoint = new Point(1, 1);

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        public SGIEntity(int x, int y, int w, int h)
            : this()
        {
            this.Location = new Point(x, y);
            this.Size = new Size(w, h);
            _originalPoint = new Point(x, y);
        }
        #endregion

        #region Overrides

        /// <summary>
        /// Get the string value of the <see cref="SGIEntity"/>
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Text;
        }

        /// <summary>
        /// Fires when the visibility of the Entity changes
        /// </summary>
        /// <param name="visible"></param>
        protected override void OnVisibilityChanged(bool visible)
        {
            if (!visible)
            {
                if (this.Connector != null)
                    this.Connector.Visible = false;
            }

            Parent.Control.Invalidate();
            base.OnVisibilityChanged(visible);
        }

        /// <summary>
        /// Fires when the widget is removed from the container
        /// </summary>
        /// <param name="container"></param>
        protected override void OnRemoveFromContainer(WidgetCanvas container)
        {
            if (_connectionOut != null)
            {
                // remove the associations with the toEntity
                for (int i = 0; i < _connectionOut.Count; i++)
                {
                    _connectionOut[i].Connected = false;
                    ((SGIEntity)_connectionOut[i].ToWidget).ConnectionIn.Remove(_connectionOut[i]);
                }

                //move the connections
                int count = _connectionOut.Count;
                for (int i = 0; i < count; i++)
                {
                    container.Widgets.Remove(_connectionOut[0]);
                }
            }

            if (_connectionIn != null)
            {
                // remove the associations with the fromEntity
                for (int i = 0; i < _connectionIn.Count; i++)
                {
                    _connectionIn[i].Connected = false;
                    ((SGIEntity)_connectionIn[i].FromWidget).ConnectionOut.Remove(_connectionIn[i]);
                }

                //move the connections
                int count = _connectionIn.Count;
                for (int i = 0; i < count; i++)
                {
                    container.Widgets.Remove(_connectionIn[0]);
                }
            }

            this.ContainerScene = null;
            StateImage = null;

            container.Widgets.Remove(this.Connector);
            this.Connector = null;

            base.OnRemoveFromContainer(container);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();

            _connectionOut = null;
            _connectionIn = null;
            _containerScene = null;

            if (_stateImage != null)
                _stateImage.Dispose();
            _stateImage = null;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Auto resize the entity to the size of the text
        /// </summary>
        public void AutoResize()
        {
            using (Font font = new Font(this.FontName, this.FontSize))
            {
                this.Width = SGI.DrawHelper.MeasureDisplayStringWidth(this.Parent.Control.CreateGraphics(),
                    this.Text, font) + 10;
            }
        }

        /// <summary>
        /// Loads default settings
        /// </summary>
        private void SetDefaultSettings()
        {
            this.BorderWidth = 1;
            this.BorderColor = Color.Black;
            this.Shape = Shapes.RoundedRectangle;
            this.CornerRadius = 0.2;
            this.ForeColor = Color.White;
            this.TextAlign = ContentAlignment.MiddleCenter;
        }

        /// <summary>
        /// Set the backcolor of the <see cref="SGIEntity"/> as transparent
        /// </summary>
        /// <param name="backcolor"></param>
        public void SetAsTransparent(Color backcolor)
        {
            this.Border = false;
            this.BackColor = backcolor;
            this.AlternateColor = backcolor;
            this.ShapeShading = Shape3D.Flat;
        }

        #endregion

        #region IExportable Members

        /// <summary>
        /// Returns a list of attributes used when saving the Entity
        /// </summary>
        /// <returns></returns>
        public virtual Dictionary<string, string> Attributes()
        {
            Dictionary<string, string> att = new Dictionary<string, string>();

            att.Add("Name", this.Name);
            att.Add("Text", this.Text);
            att.Add("Type", this.GetType().ToString());
            att.Add("EntityType", this.EntityType);

            att.Add("Active", this.Active.ToString());
            att.Add("Skip", this.Skip.ToString());
            att.Add("RunOnce", this.RunOnce.ToString());

            att.Add("X", this.X.ToString());
            att.Add("Y", this.Y.ToString());
            att.Add("W", this.Width.ToString());
            att.Add("H", this.Height.ToString());

            return att;
        }

        Dictionary<string, string> IExportable.Attributes()
        {
            return this.Attributes();
        }

        /// <summary>
        /// Gets if object can export
        /// </summary>
        /// <returns></returns>
        public bool CanExport()
        {
            return true;
        }

        #endregion
    }
}
