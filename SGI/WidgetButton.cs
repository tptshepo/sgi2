﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace SGI
{
    /// <summary>
    /// Represents a widget button
    /// </summary>
    [Serializable]
    public class WidgetButton : InteractiveWidget
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        public WidgetButton(int x, int y, int w, int h)
            : base(x, y, w, h)
        {
            this.Border = true;
            this.Transparent = true;
            this.Shape = Shapes.Rectangle;
        }
    }

    /// <summary>
    /// Represents a widget button
    /// </summary>
    [Serializable]
    public class WidgetLabel : InteractiveWidget
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        public WidgetLabel(int x, int y, int w, int h)
            : base(x, y, w, h)
        {
            this.Border = true;
            this.Transparent = true;
            this.Shape = Shapes.Rectangle;
        }
    }
}
