using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;
using System.Security.Cryptography;
using System.Diagnostics;
using System.Resources;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

namespace SGI
{
    /// <summary>
    /// Provides statis helper utilities
    /// </summary>
    public class Utilities
    {
        /// <summary>
        /// Generates a unique id from GUID
        /// </summary>
        /// <returns></returns>
        public static string GenId()
        {
            return Guid.NewGuid().ToString().Substring(1, 4);
        }

        /// <summary>
        /// Convert string to stream
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static Stream StringToXmlStream(string text)
        {
            System.Text.UnicodeEncoding myEncoder = new System.Text.UnicodeEncoding();
            byte[] bytes = myEncoder.GetBytes(text);
            myEncoder = null;
            return (Stream)new MemoryStream(bytes);
        }
        /// <summary>
        /// Convert file contents to stream
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static Stream FileToXmlStream(string filename)
        {
            string text = System.IO.File.ReadAllText(filename);
            System.Text.UnicodeEncoding myEncoder = new System.Text.UnicodeEncoding();
            byte[] bytes = myEncoder.GetBytes(text);
            return (Stream)new MemoryStream(bytes);
        }

        /// <summary>
        /// Converts a byte array to stream
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static Stream ByteToStream(byte[] bytes)
        {
            if (bytes == null)
                return null;
            else
                return (Stream)new MemoryStream(bytes);
        }
        /// <summary>
        /// Converts string to byte
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static byte[] StringToByte(string text)
        {
            System.Text.UnicodeEncoding encoding = new System.Text.UnicodeEncoding();
            Byte[] bytes = encoding.GetBytes(text);
            return bytes;
        }
        /// <summary>
        /// Converts byto string
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string ByteToSting(byte[] bytes)
        {
            System.Text.UnicodeEncoding encoding = new System.Text.UnicodeEncoding();
            string text = encoding.GetString(bytes);
            return text;
        }

    }

    internal class AdvancedCursors
    {
        [DllImport("User32.dll")]
        private static extern IntPtr LoadCursorFromFile(String str);

        public static IntPtr ArrowCursor = IntPtr.Zero;

        public static System.Windows.Forms.Cursor CreateArrowCursor()
        {
            if (ArrowCursor == IntPtr.Zero)
            {
                string cursorFolder = System.Environment.GetFolderPath(Environment.SpecialFolder.System).Replace("system32", "Cursors");

                ArrowCursor = LoadCursorFromFile(cursorFolder + "\\lnwse.cur");
                if (!IntPtr.Zero.Equals(ArrowCursor))
                {
                    return new System.Windows.Forms.Cursor(ArrowCursor);
                }
                else
                    return System.Windows.Forms.Cursors.Default;
            }
            else
                return new System.Windows.Forms.Cursor(ArrowCursor);
        }
    }
}
