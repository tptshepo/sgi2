﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.CompilerServices;

namespace SGI
{
    /// <summary>
    /// Provides object references accross domains
    /// </summary>
    [Serializable] 
    public class ServiceProvider
    {
        #region Fields
        private Dictionary<Type, object> _serviceTable = new Dictionary<Type, object>();
        private Dictionary<string, object> _serviceStaticTable = new Dictionary<string, object>();
        #endregion

        #region Methods

        /// <summary>
        /// Clear all service objects
        /// </summary>
        public void Clear()
        {
            _serviceTable.Clear();
            _serviceStaticTable.Clear();
        }

        /// <summary>
        /// Checks if the service object exisits
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public bool ContainService(Type serviceType)
        {
            if (_serviceTable.ContainsKey(serviceType))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Checks if the service object exisits
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public bool ContainService(string serviceType)
        {
            if (_serviceStaticTable.ContainsKey(serviceType))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Adds a new service object
        /// </summary>
        /// <param name="serviceType"></param>
        /// <param name="serviceInstance"></param>
        public void AddService(Type serviceType, object serviceInstance)
        {
            if (!_serviceTable.ContainsKey(serviceType))
            {
                //throw new ArgumentException("Service already exists.");
                _serviceTable.Add(serviceType, RuntimeHelpers.GetObjectValue(serviceInstance));
            }
            else
            {
                _serviceTable[serviceType] = RuntimeHelpers.GetObjectValue(serviceInstance);
            }
        }

        /// <summary>
        ///  Adds a new service object
        /// </summary>
        /// <param name="serviceType"></param>
        /// <param name="serviceInstance"></param>
        public void AddService(string serviceType, object serviceInstance)
        {
            if (!_serviceStaticTable.ContainsKey(serviceType))
            {
                //throw new ArgumentException("Service already exists.");
                _serviceStaticTable.Add(serviceType, RuntimeHelpers.GetObjectValue(serviceInstance));
            }
            else
            {
                _serviceStaticTable[serviceType] = RuntimeHelpers.GetObjectValue(serviceInstance);
            }
        }

        /// <summary>
        /// Returns a service object
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public object GetService(Type serviceType)
        {
            if (_serviceTable.ContainsKey(serviceType))
            {
                return _serviceTable[serviceType];
            }
            return null;
        }
        /// <summary>
        /// Returns a service object
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public object GetService(string serviceType)
        {
            if (_serviceStaticTable.ContainsKey(serviceType))
            {
                return _serviceStaticTable[serviceType];
            }
            return null;
        }
        /// <summary>
        /// Removes a service object
        /// </summary>
        /// <param name="serviceType"></param>
        public void RemoveService(Type serviceType)
        {
            if (_serviceTable.ContainsKey(serviceType))
            {
                _serviceTable.Remove(serviceType);
            }
        }
        /// <summary>
        /// Removes a service object
        /// </summary>
        /// <param name="serviceType"></param>
        public void RemoveService(string serviceType)
        {
            if (_serviceStaticTable.ContainsKey(serviceType))
            {
                _serviceStaticTable.Remove(serviceType);
            }
        }

        #endregion


    }
}
