﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SGI
{
    /// <summary>
    /// Items must implement this interface if they want to have
    /// their own collection class generated on their behalf.
    /// </summary>
    public interface IBaseCollectionItem
    {
    }
}
