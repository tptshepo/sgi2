using System;
using System.Drawing;
using System.Windows.Forms;

namespace SGI
{
	/// <summary>
    /// Provides access to the parent control (<see cref="WidgetCanvas"/>) for
	/// Widgets that inserted to other Widgets.
	/// </summary>
	public interface IControlService
	{
		/// <summary>
		/// Gets the <see cref="WidgetCanvas"/> that keeps the Widget.
		/// </summary>
		/// <value>
		/// The <see cref="WidgetCanvas"/> that keeps the Widget.
		/// </value>
		WidgetCanvas Control
		{
			get;
		}

		/// <summary>
		/// Invalidates a rectangular area area of the control.
		/// </summary>
		/// <param name="x">
		/// The x-coordinate of the upper-left corner of the rectangle.
		/// </param>
		/// <param name="y">
		/// The y-coordinate of the upper-left corner of the rectangle.
		/// </param>
		/// <param name="width">
		/// The width of the Widget.
		/// </param>
		/// <param name="height">
		/// The height of the Widget.
		/// </param>
		void InvalidateRectangle(int x, int y, int width, int height);
	}
}
