﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SGI
{
    [ToolboxItem(false)]
    public partial class PathBar : UserControl
    {
        public PathBar()
        {
            InitializeComponent();
        }

        public void AddPath(PathBarItem item)
        {
            this.pnlPaths.SuspendLayout();
            item.Dock = DockStyle.Left;
            this.pnlPaths.Controls.Add(item);
            item.BringToFront();
            this.pnlPaths.ResumeLayout();
        }

        public void RemovePath(PathBarItem item)
        {
            this.pnlPaths.Controls.Remove(item);
        }

        public IEnumerable<PathBarItem> Items
        {
            get
            {

                var list = from Control ent in this.pnlPaths.Controls
                           where (ent is PathBarItem)
                           select ent;

                var ents = from PathBarItem ls in list select ls;
                return ents;
            }
        }

        public void ClearPaths()
        {
            this.pnlPaths.Controls.Clear();
        }
    }
}
