﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace SGI
{
    /// <summary>
    /// Defines the selection interface
    /// </summary>
    public interface ISelectable
    {
        /// <summary>
        /// Gets or sets if the widget is selectable
        /// </summary>
        bool Selected { get; set; }

        /// <summary>
        /// Fires when the Widget is selected
        /// </summary>
        event EventHandler OnSelected;

        /// <summary>
        /// Fire when the widget losses the selection
        /// </summary>
        event EventHandler OnUnSelected;

        /// <summary>
        /// Marks the widget as selected
        /// </summary>
        void Select();

        /// <summary>
        /// Removes the selection
        /// </summary>
        void UnSelect();
    }
}
