using System;
using System.Collections.Generic;
using System.Text;

namespace SGI
{
    /// <summary>
    /// Add Caption support to an object
    /// </summary>
    public interface ICaptionSupport
    {
        /// <summary>
        /// Gets or set the caption
        /// </summary>
        string Caption { get; set; }
    }
}
