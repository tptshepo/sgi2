﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SGI
{
    /// <summary>
    /// Collection for <see cref="SGIEntityConnection"/>
    /// </summary>
    [Serializable()]
    public class SGIEntityConnectionCollection : SGI.BaseCollection<SGIEntityConnection>
    {
    }
}
