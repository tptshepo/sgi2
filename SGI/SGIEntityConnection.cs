﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SGI;
using System.ComponentModel;

namespace SGI
{
    /// <summary>
    /// Connects two or more <see cref="SGIEntity"/> together
    /// </summary>
    [Serializable()]
    public class SGIEntityConnection : SGIConnection, IExportable
    {
        /// <summary>
        /// Gets or sets if the connection has connected to entities
        /// </summary>
        public bool Connected { get; set; }

        /// <summary>
        /// Gets or sets if this is a new connection 
        /// connecting from and to an entity
        /// </summary>
        public bool IsNewConnection { get; set; }

        /// <summary>
        /// Gets or sets the container scene for this Entity
        /// </summary>
        public SGIScene ContainerScene { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public SGIEntityConnection()
            : base()
        {
            IsNewConnection = true;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public SGIEntityConnection(Widget from, Widget to)
            : base(from, to)
        {
            IsNewConnection = true;
        }

        /// <summary>
        /// Fires when the widget is removed from the container
        /// </summary>
        /// <param name="container"></param>
        protected override void OnRemoveFromContainer(WidgetCanvas container)
        {
            if (this.Connected)
            {
                if (((SGIEntity)this.FromWidget).ConnectionOut != null)
                    ((SGIEntity)this.FromWidget).ConnectionOut.Remove(this);

                if (((SGIEntity)this.ToWidget).ConnectionIn != null)
                    ((SGIEntity)this.ToWidget).ConnectionIn.Remove(this);
            }

            this.ContainerScene = null;
            this.Connected = false;
            base.OnRemoveFromContainer(container);
        }

        #region IExportable Members

        /// <summary>
        /// Returns a list of attributes used when saving the Entity
        /// </summary>
        /// <returns></returns>
        public virtual Dictionary<string, string> Attributes()
        {
            Dictionary<string, string> att = new Dictionary<string, string>();

            att.Add("Name", this.Name);
            att.Add("Text", this.Text);
            att.Add("Type", this.GetType().ToString());

            att.Add("Connected", this.Connected.ToString());
            att.Add("FromEntity", this.Connected == false ? "" : this.FromWidget.Name);
            att.Add("ToEntity", this.Connected == false ? "" : this.ToWidget.Name);

            return att;
        }

        /// <summary>
        /// Get the attributes to export
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> IExportable.Attributes()
        {
            return this.Attributes();
        }

        /// <summary>
        /// Indicates if the <see cref="SGIEntityConnection"/> can be export
        /// </summary>
        /// <returns></returns>
        public bool CanExport()
        {
            return true;
        }

        #endregion
    }
}
