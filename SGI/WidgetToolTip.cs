﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace SGI
{
    public class WidgetToolTip: Widget
    {
         private Widget _parentWidget;

        /// <summary>
        /// Gets or sets the parent control of the Connector
        /// </summary>
        public Widget ParentWidget
        {
            get { return _parentWidget; }
            set { _parentWidget = value; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        public WidgetToolTip(int x, int y, int w, int h)
            : base(x, y, w, h)
        {
            this.BringToFront();
            this.Border = true;
            this.Transparent = true;
            this.Shape = Shapes.RoundedRectangle;

            SetDefaultSettings();
        }

        /// <summary>
        /// Loads default settings
        /// </summary>
        public void SetDefaultSettings()
        {
            this.BorderWidth = 1;
            this.BorderColor = Color.AliceBlue;

            this.ShapeGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            //this.Gradients = new Color[] { Color.AliceBlue, Color.Blue };
            //this.ShapeShading = Shape3D.Shaded3D;
            this.DrawBackground = true;
        }
    }
}
