using System;
using System.Drawing;

namespace SGI
{
	/// <summary>
	/// This class implements an empty <see cref="IControlService"/>.
	/// </summary>
	/// <remarks>
	/// This class implements a NULL OBJECT design pattern.
	/// </remarks>
	internal sealed class NullControlService: IControlService
	{
		internal static readonly NullControlService Instance  = new NullControlService();

		private NullControlService()
		{}

		#region IControlService interface implementation
		public WidgetCanvas Control
		{
			get
			{
				return null;
			}
		}

		public void InvalidateRectangle(int x, int y, int width, int height)
		{}
		#endregion
	}
}
