﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SGI
{
    /// <summary>
    /// Event arguments for entity
    /// </summary>
    public class EntityEventArgs : EventArgs
    {
        /// <summary>
        /// The source <see cref="SGIEntity"/> that fired the event
        /// </summary>
        public SGIEntity Source { get; set; }

        /// <summary>
        /// indicates if the event is cancelled
        /// </summary>
        public bool Cancel { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="entity"></param>
        public EntityEventArgs(SGIEntity entity)
        {
            Source = entity;
        }
    }

    /// <summary>
    /// Event arguments for entity connection
    /// </summary>
    public class EntityConnectionEventArgs : EventArgs
    {
        /// <summary>
        /// The <see cref="SGIEntityConnection"/> that fired the event
        /// </summary>
        public SGIEntityConnection Source { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public SGIEntityConnection Destination { get; set; }

        /// <summary>
        /// indicates if the event is cancelled
        /// </summary>
        public bool Cancel { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="source"></param>
        public EntityConnectionEventArgs(SGIEntityConnection source)
        {
            Source = source;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        public EntityConnectionEventArgs(SGIEntityConnection source,
            SGIEntityConnection destination)
            : this(source)
        {
            Destination = destination;
        }
    }
}
