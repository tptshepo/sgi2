using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace SGI
{
    /// <summary>
    /// Represents a widget button
    /// </summary>
    [Serializable]
    public class LineConnector : Widget
    {
        private Widget _parentWidget;

        /// <summary>
        /// Gets or sets the parent control of the Connector
        /// </summary>
        public Widget ParentWidget
        {
            get { return _parentWidget; }
            set { _parentWidget = value; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        public LineConnector(int x, int y, int w, int h)
            : base(x, y, w, h)
        {
            this.BringToFront();
            this.Border = true;
            this.Transparent = true;
            this.Shape = Shapes.Dice;
            SetDefaultSettings();

            this.MouseMove += new MouseEventHandler(LineConnector_MouseMove);
            this.MouseLeave += new EventHandler(LineConnector_MouseLeave);
        }

        protected void LineConnector_MouseMove(object sender, MouseEventArgs e)
        {
            System.Windows.Forms.Cursor.Current = AdvancedCursors.CreateArrowCursor();
            //   this.Parent.Control.Cursor = System.Windows.Forms.Cursor.Current;
        }

        protected void LineConnector_MouseLeave(object sender, EventArgs e)
        {
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
        }

        protected override void OnRemoveFromContainer(WidgetCanvas container)
        {
            this.MouseMove -= new MouseEventHandler(LineConnector_MouseMove);
            this.MouseLeave -= new EventHandler(LineConnector_MouseLeave);

            base.OnRemoveFromContainer(container);
        }

        /// <summary>
        /// Loads default settings
        /// </summary>
        public void SetDefaultSettings()
        {
            this.BorderWidth = 1;
            this.BorderColor = Color.Black;

            this.ShapeGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.AddGradientColor((float)0.0, Color.AliceBlue);
            this.AddGradientColor((float)1.0, Color.Blue);
            this.ShapeShading = Shape3D.Shaded3D;
            this.DrawBackground = true;
        }

        /// <summary>
        /// Free memory
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();

            _parentWidget = null;
        }
    }
}
