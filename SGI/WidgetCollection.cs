using System;
using System.Collections;

namespace SGI
{
    /// <summary>
    /// Represents a typed collection of <see cref="Widget"/> objects.
    /// </summary>
    [Serializable]
    public class WidgetCollection : BaseCollection<Widget>
    {
    }
}
