using System.Collections.Generic;
using System.Drawing;
using SGI;

namespace SGI
{
    /// <summary>
    /// Contains widget items
    /// </summary>
    public class SelectionArea : InteractiveWidget
    {
        private List<SGIEntity> _widgets;

        private int _columns = 1;
        private int _width;
        private int _height;

        //used to keep track of the old positions
        int _oldX = 0;
        int _oldY = 0;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="text"></param>
        public SelectionArea(string text)
            : base(50, 50, 100, 100)
        {
            _widgets = new List<SGIEntity>();
            this.Shape = Shapes.Rectangle;
            this.BackColor = Color.White;
            this.Controller.Resizable = false;

            _oldX = this.X;
            _oldY = this.Y;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="text"></param>
        /// <param name="columns"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        public SelectionArea(string text, int columns, int x, int y, int w, int h)
            : this(text)
        {
            _columns = columns;
            _width = w;
            _height = h;
            this.X = x;
            this.Y = y;

            _oldX = this.X;
            _oldY = this.Y;
        }

        /// <summary>
        /// Adds an <see cref="SGIEntity"/> to the collection
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public SGIEntity AddEntityToSectionArea(SGIEntity item)
        {
            _widgets.Add(item); //add to group collection
            return item;
        }

        /// <summary>
        /// Update the positions of the selected entities
        /// </summary>
        private void MoveSelectedEntities()
        {
            //get the difference of X and Y
            int diffX = this.X - _oldX;
            int diffY = this.Y - _oldY;

            for (int i = 0; i < _widgets.Count; i++)
            {
                _widgets[i].X += diffX;
                _widgets[i].Y += diffY;
            }
        }

        /// <summary>
        /// Fires when the location and size of this object changes
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        protected override void OnLocationChanged(int x, int y, int width, int height)
        {
            MoveSelectedEntities();
            base.OnLocationChanged(x, y, width, height);
        }

        /// <summary>
        /// Fires when the view is added to the container
        /// </summary>
        /// <param name="container"></param>
        protected override void OnAddToContainer(WidgetCanvas container)
        {
            base.OnAddToContainer(container);
        }

        /// <summary>
        /// Fires when the widget is removed from the container
        /// </summary>
        /// <param name="container"></param>
        protected override void OnRemoveFromContainer(WidgetCanvas container)
        {
            _widgets.Clear();
            base.OnRemoveFromContainer(container);
        }
    }
}
